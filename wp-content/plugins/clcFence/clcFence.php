<?php
/*
Plugin Name: Калькулятор заборов
Plugin URI: http://www.free-lance.ru/users/J7777
Description: Плагин калькулятора заборов.
Version: 1.0
Author: Женя
Author URI: http://www.free-lance.ru/users/J7777
*/

error_reporting(E_ERROR | E_WARNING | E_PARSE);
@define(PATCH_CLCFENCE, WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)));

include 'inc/options.php';
include 'inc/clcFence_policarbonat.php';
include 'inc/clcFence_svarnoi.php';
include 'inc/clcFence_kovaniy.php';
include 'inc/clcFence_metall.php';
include 'inc/clcFence_profnastil.php';
include 'inc/clcFence_files.php';
include 'inc/output.php';

//добавление вывода в header
add_action( 'admin_init', 'clcFence_admin_init' );
wp_enqueue_script('google_captcha', 'https://www.google.com/recaptcha/api.js');

function clcFence_admin_init() {
	wp_register_script('Bootstrap', plugins_url('js/bootstrap.js', __FILE__));
	wp_register_style('Bootstrap', plugins_url('css/bootstrap.css', __FILE__));
	wp_register_script('clcFence', plugins_url('js/js.js', __FILE__));
	wp_register_style('clcFence', plugins_url('css/css.css', __FILE__));
}

function clcFence_admin_scripts($hook) {
	wp_enqueue_script('Bootstrap', PATCH_CLCFENCE.'js/bootstrap.js', array('jquery'), '1.0');
    wp_enqueue_script('clcFence', PATCH_CLCFENCE.'js/js.js', array('jquery'), '1.0');
}

function clcFence_admin_styles($hook) {
	wp_enqueue_style('Bootstrap', PATCH_CLCFENCE.'css/bootstrap.css', '', '1.0', 'screen, projection');
	wp_enqueue_style('clcFence', PATCH_CLCFENCE.'css/css.css', '', '1.0', 'screen, projection');
}

//действия при регистрации плагина
register_activation_hook(__FILE__,'clcFence_create_options');
//действия при удалении плагина
register_deactivation_hook(__FILE__,'clcFence_delete_options');

//создание настроек
function clcFence_create_options($clcFence_options){
    global $clcFence_options;
	add_option('clcFence_options', $clcFence_options);
}

//удаление настроек
function clcFence_delete_options($clcFence_options){
    global $clcFence_options;
	delete_option('clcFence_options', $clcFence_options);
}

//ссылка на страницу настроек на странице плагинов
add_filter('plugin_action_links', 'clcFence_adminMenu', 10, 2);
//ссылка на страницу настроек в блоке настройки
add_action('admin_menu', 'clcFence_menu');

//ссылка на страницу настроек на странице плагинов
function clcFence_adminMenu($links, $file){
    static $this_plugin;
    if(!$this_plugin){
        $this_plugin = 'clcFence/clcFence.php';
    }
    if($file == $this_plugin) {
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=clcFence.php">Настройки</a>';
        array_unshift($links, $settings_link);
    }
    return $links;
}

//добавление страницы настроек плагина
function clcFence_menu(){
    $page1 = add_menu_page('Заборы из профнастила', 'Заборы из профнастила', 'manage_options', basename(__FILE__), 'clcFence_form', PATCH_CLCFENCE.'img/fence.png', '75.11');
	$page2 = add_submenu_page('clcFence.php', 'Забор из поликарбоната', 'Забор из поликарбоната', 'manage_options', 'clcFence_policarbonat.php', 'clcFence_policarbonat_form');
	$page3 = add_submenu_page('clcFence.php', 'Сварной забор', 'Сварной забор', 'manage_options', 'clcFence_svarnoi.php', 'clcFence_svarnoi_form');
	$page4 = add_submenu_page('clcFence.php', 'Кованые заборы', 'Кованые заборы', 'manage_options', 'clcFence_kovaniy.php', 'clcFence_kovaniy_form');
	$page5 = add_submenu_page('clcFence.php', 'Заборы из металлического штакетника', 'Заборы из металлического штакетника', 'manage_options', 'clcFence_metall.php', 'clcFence_metall_form');
	$page6 = add_submenu_page('clcFence.php', 'Загруженные файлы', 'Загруженные файлы', 'manage_options', 'clcFence_files.php', 'clcFence_files_form');
    
    
    add_action('admin_print_scripts-' . $page1, 'clcFence_admin_scripts');
    add_action('admin_print_styles-' . $page1, 'clcFence_admin_styles');
    add_action('admin_print_scripts-' . $page2, 'clcFence_admin_scripts');
    add_action('admin_print_styles-' . $page2, 'clcFence_admin_styles');
    add_action('admin_print_scripts-' . $page3, 'clcFence_admin_scripts');
    add_action('admin_print_styles-' . $page3, 'clcFence_admin_styles');
    add_action('admin_print_scripts-' . $page4, 'clcFence_admin_scripts');
    add_action('admin_print_styles-' . $page4, 'clcFence_admin_styles');
    add_action('admin_print_scripts-' . $page5, 'clcFence_admin_scripts');
    add_action('admin_print_styles-' . $page5, 'clcFence_admin_styles');
    add_action('admin_print_scripts-' . $page6, 'clcFence_admin_scripts');
    add_action('admin_print_styles-' . $page6, 'clcFence_admin_styles');
}

function transliterate($input){ 
	$gost = array("Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"-","є"=>"ye","ѓ"=>"g","А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D","Е"=>"E","Ё"=>"YO","Ж"=>"ZH","З"=>"Z","И"=>"I","Й"=>"J",
	"К"=>"K","Л"=>"L","М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X","Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'","Ы"=>"Y","Ь"=>"","Э"=>"E",
	"Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh","з"=>"z","и"=>"i","й"=>"i","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x","ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"","ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"," "=>"_","—"=>"_",","=>"_","!"=>"_","@"=>"_",
	"#"=>"-","$"=>"","%"=>"","^"=>"","&"=>"","*"=>"","("=>"",")"=>"","+"=>"","="=>"",";"=>"",":"=>"","'"=>"","\""=>"","~"=>"","`"=>"","?"=>"","/"=>"","\\"=>"","["=>"","]"=>"","{"=>"","}"=>"","|"=>"","«"=>"","»"=>"");  
	return strtr($input, $gost); 
}

//Заборы из профнастила
add_shortcode('clcFence_profnastil', 'clcFence_profnastil_func');
function clcFence_profnastil_func(){
	return clcFence_profnastil_func_output();
}

//Металлический штакетник
add_shortcode('clcFence_metall', 'clcFence_metall_func');
function clcFence_metall_func(){
	return clcFence_metall_func_output();
}

//Сварной забор
add_shortcode('clcFence_svarnoi', 'clcFence_svarnoi_func');
function clcFence_svarnoi_func(){
	return clcFence_svarnoi_func_output();
}

//Кованый забор
add_shortcode('clcFence_kovaniy', 'clcFence_kovaniy_func');
function clcFence_kovaniy_func(){
	return clcFence_kovaniy_func_output();
}

/**
 * Request to Google reCaptcha Api
 *
 * @param $value
 *
 * @return mixed
 */
function checkRecaptchaResponse($value) {

    $response = wp_remote_post('https://www.google.com/recaptcha/api/siteverify', [
        'body' => [
        'secret' => get_option('clcFence_options')['profnastil']['profnastil_secret_key'],
        'response' => $value
    ]
        ]
    );

     return json_decode($response['body'])->success;
}