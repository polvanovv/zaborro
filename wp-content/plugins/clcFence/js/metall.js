function correctCaptcha() {
	$("form").each(function() {
		$(this).find(':button[type="submit"]').prop('disabled', false);
	});
}
(function($){
    $(document).ready(function(){
        $('#clcFence-send-form').trigger('reset');
		
		$('#clc_zazor_shtak1').prop('disabled', false);
		$('#clc_zazor_shtak2').prop('disabled', true);

		$('#order_modal').on('shown.bs.modal', () => {
			$("form").each(function() {
				$(this).find(':button[type="submit"]').prop('disabled', true);
			});

		});
		
		$('#clcFence-send-form :text').keypress(function(e){
			if(e.keyCode == 13){
				e.preventDefault();
			}
		});
		
		$('#order_modal').modal({
			backdrop: false,
			show: false
		});
		
		clc();
		
		$('#clcFence').delegate(':text', 'keyup change', function(){
			if($(this).attr('id') == 'clc_length'){
				var val = $(this).val();
				var res = '';
				
				for(var i = 0, mx = val.length; i < mx; i++){
					if(Number(val[i]) || val[i] == '.' || val[i] == '0'){
						res += val[i];
					}
				}
				
				$(this).val(res);
			}
			
			clc();
		});
		
		$('#clcFence').delegate(':checkbox, select', 'change', function(){
			clc();
		});
		
		$('#clcFence-send-form').validationEngine('attach', {
			promptPosition : "topLeft", 
			scroll: false 
		});
		
		$('#clc_evroshtaketnik').change(function(){
			var img = $(this).find('option:selected').attr('img');
			$('#clc-img').attr('src', '../../../wp-content/plugins/clcFence/img/uploads/'+img);
			
			var colors = $(this).find('option:selected').attr('color').split(/,/);
			$('#clc_color').html('');
			for(var i = 0, max = colors.length; i < max; i++){
				$('#clc_color').append('<option value="'+colors[i]+'">'+colors[i]+'</option>');
			}
		});
		
		$('#clc_type_install').change(function(){
			var inx = $(this).find('option:selected').index();
			inx++;
			$('select[name="clc_zazor_shtak"]').prop('disabled', true).closest('.form-group').hide();
			$('#clc_zazor_shtak'+inx).prop('disabled', false).closest('.form-group').show();
		});
		
		$('#print_order').click(function(){
			if(!Number($('#clc_total').val())){
				return false;
			}
			
			var params = "resizable=yes,scrollbars=yes,width=950,height=800";
			var newWin = window.open('', '', params);
			var cost_m = Math.round(Number($('#clc_total').val()) / Number($('#clc_length').val()));
			var phones = $('#fence_phones').val() ? '<tr><td align="center"><h3>'+$('#fence_phones').val()+'</h3></td></tr>' : '';
			var address = $('#fence_address').val() ? '<tr><td align="center"><h3>'+$('#fence_address').val()+'</h3></td></tr>' : '';
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="0">'+
														address+
														phones+
														'<tr>'+
															'<td><p>Стоимость погонного метра '+cost_m+' руб.</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td><p>Длина забора: '+$('#clc_length').val()+'м</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td><p>Высота: '+$('#clc_height').find('option:selected').text()+'</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td align="center"><h3>Расчет забора из металлического штакетника</h3></td>'+
														'</tr>'+
													'</table>');
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="1">'+$('#table-list-data').html()+'</table>');
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="0">'+
														'<tr>'+
															'<td><a onclick="print()" href="">Печать</a></td>'+
														'</tr>'+
													'</table>');
		});
		
		function clc(){
			var mat = 0;
			var job = 0;
			var dop = 0;
			var total = 0;
			var out = '<tr>'+$('#table-list-data').find('tr:eq(0)').html()+'</tr>';
			var length = Number($('#clc_length').val());
			
			if(!length){
				$('#clc_itog_mat').val(mat);
				$('#clc_itog_job').val(job);
				$('#clc_total').val(total);
				return false;
			}
			
			//Металлические столбы 
			var count_stlb = Math.round(length / Number($('#clc_rasst_stolb').val())) + 1;
			var name_count_stlb = $('#clc_rasst_stolb option:selected').text();
			var cost_stlb = Number($('#metall_stolbi').val());
			var sum_stlb = count_stlb * cost_stlb;
			mat += sum_stlb;
			total += sum_stlb;
			out += '<tr>'+
						'<td><p>Металлические столбы, расстояние '+name_count_stlb+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+count_stlb+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+cost_stlb+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(sum_stlb)+'</p></td>'+
					'</tr>';
					
			//Лаги
			var lagi_count = Number($('#clc_lagi').val()) * length;
			var name_lagi_count = $('#clc_lagi option:selected').text();
			var lagi_cost = Number($('#clc_lagi option:selected').attr('cost'));
			var lagi_sum = lagi_count * lagi_cost;
			total += lagi_sum;
			mat += lagi_sum;
			out += '<tr>'+
						'<td><p>Лаги, кол-во '+name_lagi_count+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+lagi_count+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+lagi_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(lagi_sum)+'</p></td>'+
					'</tr>';
					
			//Тип евроштакетника 
			var height_name = $('#clc_height option:selected').text();
			var name_evroshtaketnik = $('#clc_evroshtaketnik').val();
			var color_evroshtaketnik = $('#clc_color').val();
			var zazor_shtak = Number($('select[name="clc_zazor_shtak"]').not(':disabled').val());
			var name_zazor_shtak = $('select[name="clc_zazor_shtak"]').not(':disabled').find('option:selected').text();
			var count_evroshtaketnik = Math.round(length / ((zazor_shtak + 11.8) / 100));
			var arr_cost_evroshtaketnik = $('#clc_evroshtaketnik option:selected').attr('cost').split(/,/);
			var inx_height = $('#clc_height option:selected').index();
			var sum_evroshtaketnik = count_evroshtaketnik * arr_cost_evroshtaketnik[inx_height];
			var inx_type_install = $('#clc_type_install').find('option:selected').index();
			var name_type_install = $('#clc_type_install').find('option:selected').text();
			
			if(inx_type_install){
				count_evroshtaketnik *= 2;
				sum_evroshtaketnik *= 2;
			}
			
			total += sum_evroshtaketnik;
			mat += sum_evroshtaketnik;
			out += '<tr>'+
						'<td><p>'+name_evroshtaketnik+', h='+height_name+', '+name_type_install+', Зазор между штакетником '+name_zazor_shtak+', Цвет '+color_evroshtaketnik+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+count_evroshtaketnik+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+arr_cost_evroshtaketnik[inx_height]+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(sum_evroshtaketnik)+'</p></td>'+
					'</tr>';
					
			//Покрытие каркаса забора
			var height = Number($('#clc_height').val());
			var area = length * height;
			var coloration_name = $('#clc_coloration').val();
			var coloration_cost = Number($('#clc_coloration option:selected').attr('cost'));
			var coloration_sum = area * coloration_cost;
			total += coloration_sum;
			mat += coloration_sum;
			out += '<tr>'+
						'<td><p>Покрытие каркаса забора - '+coloration_name+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(area)+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>кв.м.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+coloration_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(coloration_sum)+'</p></td>'+
					'</tr>';
					
			//Саморезы
			var samorezi_name = $('#clc_samorezi').val();
			var samorezi_cost = Number($('#clc_samorezi option:selected').attr('cost'));
			var samorezi_count = count_evroshtaketnik * 4;
			var samorezi_sum = samorezi_cost * samorezi_count;
			total += samorezi_sum;
			mat += samorezi_sum;
			out += '<tr>'+
						'<td><p>Саморезы - '+samorezi_name+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+samorezi_count+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+samorezi_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(samorezi_sum)+'</p></td>'+
					'</tr>';
			
			//Пластиковые заглушки
			var zaglushki_cost = Number($('#clc_zaglushki option:selected').attr('cost'));
			var zaglushki_sum = count_stlb * zaglushki_cost;
			if(zaglushki_sum){
				total += zaglushki_sum;
				mat += zaglushki_sum;
				out += '<tr>'+
						'<td><p>Пластиковые заглушки</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+count_stlb+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+zaglushki_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(zaglushki_sum)+'</p></td>'+
					'</tr>';
			}
			
			//Ворота
			var gate = $('#clc_vorota').val();
			var gate_count = Number($('#clc_count_vorota').val()) ? Number($('#clc_count_vorota').val()) : 0;
			var gate_cost = Number($('#clc_vorota option:selected').attr('cost'));
			var gate_sum = gate_count * gate_cost;
			if(gate_count){
				total += gate_sum;
				mat += gate_sum;
				out += '<tr>'+
							'<td><p>Ворота шириной - '+gate+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(gate_sum)+'</p></td>'+
						'</tr>';
			}
			
			//Калитка
			var wicket_cost = Number($('#clc_count_kalitka option:selected').attr('cost'));
			var wicket_count = Number($('#clc_count_kalitka').val());
			var wicket_sum = wicket_cost * wicket_count;
			if(wicket_count && gate_count){
				total += wicket_sum;
				mat += wicket_sum;
				out += '<tr>'+
							'<td><p>Калитка</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(wicket_sum)+'</p></td>'+
						'</tr>';
			}
			
			out += '<tr>'+
						'<td align="right" style="text-align:right;" colspan="4"><p>Стоимость материалов:</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(mat)+'</p></td>'+
					'</tr>';
					
			//Установка забора
			var ustanovka_zabora_cost = Number($('#ustanovka_zabora').val());
			var ustanovka_zabora_sum = length * ustanovka_zabora_cost;
			if($('#clc_install_zabor').prop('checked')){
				total += ustanovka_zabora_sum;
				job += ustanovka_zabora_sum;
				out += '<tr>'+
							'<td><p>Установка забора</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>м.п.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+ustanovka_zabora_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(ustanovka_zabora_sum)+'</p></td>'+
						'</tr>';
			}
					
			//Установка столбов
			var install_stolbi_name = $('#clc_install_stolbi').val();
			var install_stolbi_cost = Number($('#clc_install_stolbi option:selected').attr('cost'));
			var install_stolbi_sum = count_stlb * install_stolbi_cost;
			if($('#clc_install_zabor').prop('checked')){
				total += install_stolbi_sum;
				job += install_stolbi_sum;
				out += '<tr>'+
							'<td><p>Установка столбов - '+install_stolbi_name+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+count_stlb+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+install_stolbi_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(install_stolbi_sum)+'</p></td>'+
						'</tr>';
			}
					
			//Монтаж ворот
			var montaz_vorot_cost = Number($('#montaz_vorot').val());
			var montaz_vorot_sum = montaz_vorot_cost * gate_count;
			if(gate_count && $('#clc_install_zabor').prop('checked')){
				total += montaz_vorot_sum;
				job += montaz_vorot_sum;
				out += '<tr>'+
							'<td><p>Монтаж ворот</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+montaz_vorot_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(montaz_vorot_sum)+'</p></td>'+
						'</tr>';
			}
					
			//Монтаж калитки
			var montaz_kalitki_cost = Number($('#montaz_kalitki').val());
			var montaz_kalitki_sum = montaz_kalitki_cost * wicket_count;
			if(wicket_count && gate_count && $('#clc_install_zabor').prop('checked')){
				total += montaz_kalitki_sum;
				job += montaz_kalitki_sum;
				out += '<tr>'+
							'<td><p>Монтаж калитки</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+montaz_kalitki_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(montaz_kalitki_sum)+'</p></td>'+
						'</tr>';
			}
			
			if(job){
				out += '<tr>'+
							'<td align="right" style="text-align:right;" colspan="4"><p>Стоимость работ:</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(job)+'</p></td>'+
						'</tr>';
			}
					
			//Амортизация оборудования и расходники
			var amortizacia_cost = Number($('#amortizacia').val());
			if($('#clc_install_zabor').prop('checked')){
				total += amortizacia_cost;
				dop += amortizacia_cost;
				out += '<tr>'+
							'<td><p>Амортизация оборудования и расходники</p></td>'+
							'<td align="center" style="text-align:center;"><p>1</p></td>'+
							'<td align="center" style="text-align:center;"><p>к-кт</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+amortizacia_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(amortizacia_cost)+'</p></td>'+
						'</tr>';
			}
					
			//Мкад
			var mkad_cost = Number($('#clc_mkad').attr('cost'));
			var mkad_count = Number($('#clc_mkad').val());
			var mkad_sum = mkad_count * mkad_cost;
			
			if(mkad_count){
				total += mkad_sum;
				dop += mkad_sum;
				out += '<tr>'+
							'<td><p>Расстояние от МКАД</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+mkad_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>км.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+mkad_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(mkad_sum)+'</p></td>'+
						'</tr>';
			}
					
			//Вывоз генератора
			var generator_cost = $('#clc_generator').prop('checked') ? Number($('#clc_generator').val()) : 0;
			var generator_length = Number($('#clc_generator_length').val());
			var day_generator = Math.ceil(length / generator_length);
			var generator_sum = day_generator * generator_cost;
			
			if($('#clc_generator').prop('checked') && $('#clc_install_zabor').prop('checked')){
				total += generator_sum;
				dop += generator_sum;
				out += '<tr>'+
							'<td><p>Вывоз генератора</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+day_generator+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>дн.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+generator_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(generator_sum)+'</p></td>'+
						'</tr>';
			}
			
			if(dop){
				out += '<tr>'+
							'<td align="right" style="text-align:right;" colspan="4"><p>Дополнительно:</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(dop)+'</p></td>'+
						'</tr>';
			}
			
			out += '<tr>'+
						'<td align="right" style="text-align:right;" colspan="4"><p>Итого:</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(total)+'</p></td>'+
					'</tr>';
			
			$('#clc_itog_mat').val(Math.round(mat));
			$('#clc_itog_job').val(Math.round(job));
			$('#clc_total').val(Math.round(total));
			$('#table-list-data').html(out);
			$('#list-data').val(out);
		}

    });

})(jQuery);