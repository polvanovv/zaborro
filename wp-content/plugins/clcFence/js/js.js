(function($){
    $(document).ready(function(){
        $('.add-row').delegate('', 'click', function(){
			var htm = $(this).closest('.panel-body').find('.form-group:eq(0)').html();
			$(this).closest('.panel-body').find('.form-group').last().after('<div class="form-group" style="padding: 0 0 35px 0; clear: both;">'+htm+'</div>');
			$(this).closest('.panel-body').find('.form-group').last().find(':input').val('');
		});
		
		$('#accordion').delegate('.dell-row', 'click', function(){
			if(confirm('Удалить?')){
				if($(this).closest('.panel-body').find('.form-group').size() < 2){
					alert('Нельзя удалить последнее поле!');
					return false;
				}
				
				$(this).closest('.form-group').fadeOut(150, function(){
					$(this).remove();
				});
			}
		});
		
		$('#clcFence').delegate('.name-upload-file', 'focus', function(){
			$(this).select();
		});
		
		$('#clcFence').delegate('.dell-file', 'click', function(){
			if(!confirm('Удалить?')){
				return false;
			}
		});
		
		$('#clcFence').delegate(':submit[name="reset"]', 'click', function(){
			if(!confirm('Сбросить настройки?')){
				return false;
			}
		});
    });
})(jQuery);