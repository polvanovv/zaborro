function correctCaptcha() {
	$("form").each(function() {
		$(this).find(':button[type="submit"]').prop('disabled', false);
	});
}

(function($){
    $(document).ready(function(){

    	$('#order_modal').on('shown.bs.modal', () => {
			$("form").each(function() {
				$(this).find(':button[type="submit"]').prop('disabled', true);
			});

		})

        $('#clcFence-send-form').trigger('reset');
		$('#order_modal').modal({
			backdrop: false,
			show: false
		});
		
		$('#clcFence-send-form :text').keypress(function(e){
			if(e.keyCode == 13){
				e.preventDefault();
			}
		});
		
		clc();
		
		$('#clcFence').delegate(':text', 'keyup change', function(){
			if($(this).attr('id') == 'clc_length'){
				var val = $(this).val();
				var res = '';
				
				for(var i = 0, mx = val.length; i < mx; i++){
					if(Number(val[i]) || val[i] == '.' || val[i] == '0'){
						res += val[i];
					}
				}
				
				$(this).val(res);
			}
			
			clc();
		});
		
		$('#clcFence').delegate(':checkbox, select', 'change', function(){
			clc();
		});
		
		$('#clcFence-send-form').validationEngine('attach', {
			promptPosition : "topLeft", 
			scroll: false 
		});
		
		$('#print_order').click(function(){
			if(!Number($('#clc_total').val())){
				return false;
			}
			
			var params = "resizable=yes,scrollbars=yes,width=950,height=800";
			var newWin = window.open('', '', params);
			var cost_m = Math.round(Number($('#clc_total').val()) / Number($('#clc_length').val()));
			var phones = $('#fence_phones').val() ? '<tr><td align="center"><h3>'+$('#fence_phones').val()+'</h3></td></tr>' : '';
			var address = $('#fence_address').val() ? '<tr><td align="center"><h3>'+$('#fence_address').val()+'</h3></td></tr>' : '';
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="0">'+
														address+
														phones+
														'<tr>'+
															'<td><p>Стоимость погонного метра '+cost_m+' руб.</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td><p>Длина забора: '+$('#clc_length').val()+'м</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td><p>Высота: '+$('#clc_height').find('option:selected').text()+'</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td align="center"><h3>Расчет забора из профнастила</h3></td>'+
														'</tr>'+
													'</table>');
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="1">'+$('#table-list-data').html()+
														'<tr>'+
															'<td colspan="4" align="right"><p>Итого за материалы и работу:</p></td>'+
															'<td align="center">'+$('#clc_total').val()+'</td>'+
														'</tr>'+
													'</table>');
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="0">'+
														'<tr>'+
															'<td><a onclick="print()" href="">Печать</a></td>'+
														'</tr>'+
													'</table>');
		});
		
		function clc(){
			var mat = 0;
			var job = 0;
			var total = 0;
			var out = '<tr>'+$('#table-list-data').find('tr:eq(0)').html()+'</tr>';
			var length = Number($('#clc_length').val());
			
			if(!length){
				$('#clc_itog_mat').val(mat);
				$('#clc_itog_job').val(job);
				$('#clc_total').val(total);
				return false;
			}
			
			//Высота забора
			var height_cost = Number($('#clc_height option:selected').attr('cost'));
			var height_data = $('#clc_height option:selected').text();
			var height = $('#clc_height').val();
			total += length * height_cost;
			mat += length * height_cost;
			out += '<tr>'+
						'<td><p>Столб проф.труба 60x60мм, h=2700 мм. Профнастил С8, h='+height_data+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>мп.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+height_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(length * height_cost)+'</p></td>'+
					'</tr>';
			
			//Лаги
			var lagi = $('#clc_lagi').val();
			var lagi_cost = Number($('#clc_lagi option:selected').attr('cost'));
			total += length * lagi_cost;
			mat += length * lagi_cost;
			out += '<tr>'+
						'<td><p>Лаги проф. труба 40x20мм '+lagi+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>мп.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+lagi_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(length * lagi_cost)+'</p></td>'+
					'</tr>';
					
			//Вид покрытия
			var cover = $('#clc_cover').val();
			var cover_cost = Number($('#clc_cover option:selected').attr('cost'));
			var cover_per = Number($('#clc_cover option:selected').attr('per')) ? Number($('#clc_cover option:selected').attr('per')) : 0;
			var cover_summ = 0;
			
			if(!cover_per){
				cover_summ = length * cover_cost;
				out += '<tr>'+
							'<td><p>Вид покрытия '+cover+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>мп.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+cover_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(cover_summ)+'</p></td>'+
						'</tr>';
			}
			else{
				var cover_cost_per = height_cost - (height_cost - height_cost * cover_cost / 100);
				cover_summ = cover_cost_per * length;
				out += '<tr>'+
							'<td><p>Вид покрытия '+cover+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>мп.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+cover_cost_per+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(cover_summ)+'</p></td>'+
						'</tr>';
			}
			total += cover_summ;
			mat += cover_summ;
			
			//Ворота
			var gate = $('#clc_width_gate').val();
			var gate_count = Number($('#clc_count_gate').val()) ? Number($('#clc_count_gate').val()) : 0;
			var gate_cost = Number($('#clc_width_gate option:selected').attr('cost'));
			if(gate_count){
				total += gate_count * gate_cost;
				mat += gate_count * gate_cost;
				out += '<tr>'+
							'<td><p>Ворота распашные шириной '+gate+'м</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(gate_count * gate_cost)+'</p></td>'+
						'</tr>';
			}
					
			//Калитка
			var wicket_cost = Number($('#clc_wicket_cost').val());
			var wicket_count = Number($('#clc_wicket option:selected').val());
			if(wicket_count){
				total += wicket_count * wicket_cost;
				mat += wicket_count * wicket_cost;
				out += '<tr>'+
							'<td><p>Калитка шириной 1м.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(wicket_count * wicket_cost)+'</p></td>'+
						'</tr>';
			}
					
			out += '<tr>'+
						'<td align="right" style="text-align:right;" colspan="4"><p>Стоимость материалов:</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(mat)+'</p></td>'+
					'</tr>';
					
			//Изготовление забора
			var fence_install = Number($('#fence_install').val());
			total += length * fence_install;
			job += length * fence_install;
			out += '<tr>'+
						'<td><p>Изготовление забора</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>мп.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+fence_install+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(length * fence_install)+'</p></td>'+
					'</tr>';
					
			//Окрашивание каркаса забора
			var coloration = $('#clc_coloration').val();
			var coloration_cost = Number($('#clc_coloration option:selected').attr('cost'));
			var coloration_area = Math.ceil(height * length);
			total += coloration_area * coloration_cost;
			job += coloration_area * coloration_cost;
			out += '<tr>'+
						'<td><p>Окрашивание каркаса забора '+coloration+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+coloration_area+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>м2.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+coloration_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(coloration_area * coloration_cost)+'</p></td>'+
					'</tr>';
					
			//Установка ворот
			var gate_install = Number($('#gate_install').val());
			var gate_count = Number($('#clc_count_gate').val());
			if(gate_count){
				total += gate_count * gate_install;
				job += gate_count * gate_install;
				out += '<tr>'+
							'<td><p>Установка ворот</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_install+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(gate_count * gate_install)+'</p></td>'+
						'</tr>';
			}
					
			//Установка калитки
			var wicket_install = Number($('#wicket_install').val());
			var wicket_count = Number($('#clc_wicket').val());
			if(wicket_count){
				total += wicket_count * wicket_install;
				job += wicket_count * wicket_install;
				out += '<tr>'+
							'<td><p>Установка калитки</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_install+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(wicket_count * wicket_install)+'</p></td>'+
						'</tr>';
			}
					
			out += '<tr>'+
						'<td align="right" style="text-align:right;" colspan="4"><p>Стоимость работ:</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(job)+'</p></td>'+
					'</tr>';
					
			//Мкад
			var mkad_cost = Number($('#clc_mkad').attr('cost'));
			var mkad_count = Number($('#clc_mkad').val());
			
			if(mkad_count){
				total += mkad_count * mkad_cost;
				out += '<tr>'+
							'<td><p>Расстояние от МКАД</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+mkad_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>км.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+mkad_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(mkad_count * mkad_cost)+'</p></td>'+
						'</tr>';
			}
					
			//Вывоз генератора
			var generator = $('#clc_generator').prop('checked') ? Number($('#clc_generator').val()) : 0;
			var generator_length = Number($('#clc_generator_length').val());
			var day_generator = Math.ceil(length / generator_length);
			
			if(generator){
				total += day_generator * generator;
				out += '<tr>'+
							'<td><p>Вывоз генератора</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+day_generator+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>дн.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+generator+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(day_generator * generator)+'</p></td>'+
						'</tr>';
			}
			
			$('#clc_itog_mat').val(Math.round(mat));
			$('#clc_itog_job').val(Math.round(job));
			$('#clc_total').val(Math.round(total));
			$('#table-list-data').html(out);
			$('#list-data').val(out);
		}
    });
})(jQuery);