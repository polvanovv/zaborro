function correctCaptcha() {
    $("form").each(function() {
        $(this).find(':button[type="submit"]').prop('disabled', false);
    });
}

(function($){
    $(document).ready(function(){

        $('#order_modal').on('shown.bs.modal', () => {
            console.log('ololo');
            $("form").each(function() {
                $(this).find(':button[type="submit"]').prop('disabled', true);
            });

        });

        $('#clcFence-send-form').trigger('reset');
		$('#order_modal').modal({
			backdrop: false,
			show: false
		});
        
        $('#slider_length').slider({
            range: 'min',
            min: 1,
            max: 200,
            value: 30,
            step: 1,
            change: function(event, ui){
                $('#clc_length').val(ui.value);
                clc();
            },
            slide: function(event, ui){
                $('#clc_length').val(ui.value);
            }
        });
        
        $('#clc_length').val(30);
        
        $('#clc_count_gate').on('change', function(){
            if($(this).val() > 0)
                $('#clc_width_gate, #clc_type_gate').closest('.form-group').show();
            else
                $('#clc_width_gate, #clc_type_gate').closest('.form-group').hide();
        });
        
        $('#clc_length').on('keyup change', function(){
            var val = $(this).val();
            $('#slider_length').slider({value: val});
        });
        
        $('#clc_coloration').on('change', function(){
            $('#clc_color option').prop('disabled', false);
            
            var coloration = $(this).val();
            var flag = false;
            $('#clc_color option').each(function(){
                if($(this).attr('coloration') != coloration){
                    $(this).prop('disabled', true);
                }
                else if(!flag){
                    $(this).prop('selected', true);
                    flag = true;
                }
            });
        });
		
		$('#clcFence-send-form :text').keypress(function(e){
			if(e.keyCode == 13){
				e.preventDefault();
			}
		});
		
		clc();
		
		$('#clcFence').delegate(':text', 'keyup change', function(){
			if($(this).attr('id') == 'clc_length'){
				var val = $(this).val();
				var res = '';
				
				for(var i = 0, mx = val.length; i < mx; i++){
					if(Number(val[i]) || val[i] == '.' || val[i] == '0'){
						res += val[i];
					}
				}
				
				$(this).val(res);
			}
			
			clc();
		});
		
		$('#clcFence').delegate(':checkbox, select', 'change', function(){
			clc();
		});
		
		$('#clcFence-send-form').validationEngine('attach', {
			promptPosition : "topLeft", 
			scroll: false 
		});
        
        $('#clcFence .wrap-sketch').delegate('.btn', 'click', function(e){
            e.preventDefault();
            
            if($(this).closest('.thumbnail').hasClass('active'))
                return false;
            
            var path = $('#clc-img').attr('path');
            var img = $(this).closest('.thumbnail').find('img').attr('alt');
            
            $('.wrap-sketch .thumbnail').removeClass('active');
            $('.wrap-sketch .btn').text('Выбрать');
            
            $(this).text('Выбрано');
            $(this).closest('.thumbnail').addClass('active');
            var path = $('#clc-img').attr('src', path+img);
            
            clc();
        });
        
        $('#clcFence .wrap-sketch').delegate('img', 'click', function(e){
            e.preventDefault();
            
            if($(this).closest('.thumbnail').hasClass('active'))
                return false;
            
            var path = $('#clc-img').attr('path');
            var img = $(this).attr('alt');
            
            $('.wrap-sketch .thumbnail').removeClass('active');
            $('.wrap-sketch .btn').text('Выбрать');
            
            $(this).closest('.thumbnail').find('.btn').text('Выбрано');
            $(this).closest('.thumbnail').addClass('active');
            var path = $('#clc-img').attr('src', path+img);
            
            clc();
        });
        
        $('#clc-img').click(function(){
            if($(':button[data-target="#collapseExample"]').attr('aria-expanded') == 'true')
                return false;
            
            $(':button[data-target="#collapseExample"]').trigger('click');
        });
        
        $('#clcFence .wrap-zagl-met').delegate('.thumbnail', 'click', function(e){
            e.preventDefault();
            
            if($(this).hasClass('active'))
                return false;
            
            $('.wrap-zagl-met .thumbnail').removeClass('active');
            $('.wrap-zagl-met .thumbnail').find('.badge').hide();
            
            $(this).addClass('active');
            $(this).find('.badge').show();
            
            clc();
        });
        
        $('#clcFence .wrap-zagl-kir').delegate('.thumbnail', 'click', function(e){
            e.preventDefault();
            
            if($(this).hasClass('active'))
                return false;
            
            $('.wrap-zagl-kir .thumbnail').removeClass('active');
            $('.wrap-zagl-kir .thumbnail').find('.badge').hide();
            
            $(this).addClass('active');
            $(this).find('.badge').show();
            
            clc();
        });
        
        $('#clc_type_stolb').change(function(){
            var val = $(this).val();
            
            if(val == 'Металлические'){
                $('#clc_metall_stolbi_type').closest('.form-group').show();
                $('#clc_kirpich_stolbi_type').closest('.form-group').hide();
                $('#clc_install_met_stolb').closest('.form-group').show();
                $('#clc_install_kir_stolb').closest('.form-group').hide();
                $('.wrap-zagl-met').show();
                $('.wrap-zagl-kir').hide();
            }
            else{
                $('#clc_metall_stolbi_type').closest('.form-group').hide();
                $('#clc_kirpich_stolbi_type').closest('.form-group').show();
                $('#clc_install_met_stolb').closest('.form-group').hide();
                $('#clc_install_kir_stolb').closest('.form-group').show();
                $('.wrap-zagl-met').hide();
                $('.wrap-zagl-kir').show();
            }
            
            clc();
        });
        
		$('#print_order').click(function(){
			if(!Number($('#clc_total').val())){
				return false;
			}
			
			var params = "resizable=yes,scrollbars=yes,width=950,height=800";
			var newWin = window.open('', '', params);
			var cost_m = Math.round(Number($('#clc_total').val()) / Number($('#clc_length').val()));
			var phones = $('#fence_phones').val() ? '<tr><td align="center"><h3>'+$('#fence_phones').val()+'</h3></td></tr>' : '';
			var address = $('#fence_address').val() ? '<tr><td align="center"><h3>'+$('#fence_address').val()+'</h3></td></tr>' : '';
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="0">'+
														address+
														phones+
														'<tr>'+
															'<td><p>Стоимость погонного метра '+cost_m+' руб.</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td><p>Длина забора: '+$('#clc_length').val()+'м</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td><p>Высота: '+$('#clc_height').find('option:selected').text()+'</p></td>'+
														'</tr>'+
														'<tr>'+
															'<td align="center"><h3>Расчет кованого секционного забора</h3></td>'+
														'</tr>'+
													'</table>');
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="1">'+$('#table-list-data').html()+
														'<tr>'+
															'<td colspan="4" align="right"><p>Итого за материалы и работу:</p></td>'+
															'<td align="center">'+$('#clc_total').val()+'</td>'+
														'</tr>'+
													'</table>');
			
			$(newWin.document).find('body').append('<table align="center" width="900" cellpadding="5" cellspacing="0" border="0">'+
														'<tr>'+
															'<td><a onclick="print()" href="">Печать</a></td>'+
														'</tr>'+
													'</table>');
		});
		
		function clc(){
			var length = Number($('#clc_length').val()) ? Number($('#clc_length').val()) : 0;
            var height = Number($('#clc_height').val());
            var area = length * height;
            var mat = 0;
			var job = 0;
            var dop = 0;
			var total = 0;
			var out = '<tr>'+$('#table-list-data').find('tr:eq(0)').html()+'</tr>';
            
			if(!length){
				$('#clc_itog_mat').val(mat);
				$('#clc_itog_job').val(job);
				$('#clc_total').val(total);
				return false;
			}
			
			//Тип забора
			var price_section_m = Number($('.wrap-sketch').find('.thumbnail.active').find('.btn').attr('href'));
            var name_section = $('.wrap-sketch').find('.thumbnail.active').find('.sketch-art').text();
            var width_section = Number($('#clc_rasst_stolb').val());
            var thick_prof = Number($('#clc_thick_prof option:selected').attr('per')) ? Number($('#clc_thick_prof option:selected').attr('per')) : 0;
            var thick_prof_name = $('#clc_thick_prof').val();
            
            price_section_m = price_section_m + price_section_m * thick_prof / 100;
            
            var area_section = width_section * height;
            var price_section = area_section * price_section_m;
            
            var wicket_count = Number($('#clc_wicket').val());
            var stolbi_name = $('#clc_type_stolb').val();
            
            if(stolbi_name == 'Металлические'){
                var stolbi_width = Number($('#clc_metall_stolbi_type option:selected').attr('width')) ? Number($('#clc_metall_stolbi_type option:selected').attr('width')) : 0;
            }
            else{
                var stolbi_width = Number($('#clc_kirpich_stolbi_type option:selected').attr('width')) ? Number($('#clc_kirpich_stolbi_type option:selected').attr('width')) : 0;
            }
            
            var gate_width = Number($('#clc_width_gate').val()) ? Number($('#clc_width_gate').val()) : 0;
			var gate_count = Number($('#clc_count_gate').val()) ? Number($('#clc_count_gate').val()) : 0;
            
            var old_width_section = width_section;
            width_section += stolbi_width * 2;
            
            var old_length = length;
            length -= gate_count * gate_width;
            length -= wicket_count;
            var count_section = Math.ceil(length / width_section);
            total += Math.round(count_section * price_section);
			mat += Math.round(count_section * price_section);
            
            out += '<tr>'+
						'<td><p>Металлическая '+name_section+', высотой '+height+'м, шириной '+old_width_section+'м, с толщиной профиля '+thick_prof_name+', общая длина '+old_length+'м.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+count_section+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+price_section+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(count_section * price_section)+'</p></td>'+
					'</tr>';
            
            //Столбы
            if(stolbi_name == 'Металлические'){
                var stolbi_type_name = 'Металлические столбы '+$('#clc_metall_stolbi_type').val();
                var stolbi_type_price = Number($('#clc_metall_stolbi_type option:selected').attr('cost')) ? Number($('#clc_metall_stolbi_type option:selected').attr('cost')) : 0;
            }
            else{
                var stolbi_type_name = 'Кирпичные столбы '+$('#clc_kirpich_stolbi_type').val();
                var stolbi_type_price = Number($('#clc_kirpich_stolbi_type option:selected').attr('cost')) ? Number($('#clc_kirpich_stolbi_type option:selected').attr('cost')) : 0;
            }
            
            var count_stolbi = count_section + 1;
            total += Math.round(count_stolbi * stolbi_type_price);
			mat += Math.round(count_stolbi * stolbi_type_price);
			out += '<tr>'+
						'<td><p>'+stolbi_type_name+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+count_stolbi+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+stolbi_type_price+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(count_stolbi * stolbi_type_price)+'</p></td>'+
					'</tr>';
            
            //Заглушки столбов
            if(stolbi_name == 'Металлические'){
                var zagl_name = $('.wrap-zagl-met').find('.thumbnail.active').attr('naim');
                var zagl_price = $('.wrap-zagl-met').find('.thumbnail.active').attr('cost');
            }
            else{
                var zagl_name = $('.wrap-zagl-kir').find('.thumbnail.active').attr('naim');
                var zagl_price = $('.wrap-zagl-kir').find('.thumbnail.active').attr('cost');
            }
            
            total += Math.round(count_stolbi * zagl_price);
			mat += Math.round(count_stolbi * zagl_price);
			out += '<tr>'+
						'<td><p>Заглушка '+zagl_name+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+count_stolbi+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+zagl_price+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(count_stolbi * zagl_price)+'</p></td>'+
					'</tr>';
            
            //Ворота
			var gate_cost = price_section_m;
            var gate_type_name = $('#clc_type_gate').val();
            var gate_type_per = Number($('#clc_type_gate option:selected').attr('per')) ? Number($('#clc_type_gate option:selected').attr('per')) : 0;
            var gate_type_cost = Number($('#clc_type_gate option:selected').attr('cost')) ? Number($('#clc_type_gate option:selected').attr('cost')) : 0;
            var gate_area = gate_width * height;
            var gate_price = gate_area * gate_cost;
            
            gate_price = gate_price + gate_price * gate_type_per / 100;
            gate_price += gate_type_cost;
            
			if(gate_count){
				var autom = '';
                
                if(gate_type_cost)
                    autom = ' (автоматика CAME, Италия)';
                
                total += Math.round(gate_count * gate_price);
				mat += Math.round(gate_count * gate_price);
				out += '<tr>'+
							'<td><p>'+gate_type_name+autom+' шириной '+gate_width+'м</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_price+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(gate_count * gate_price)+'</p></td>'+
						'</tr>';
			}
            
            //Калитка
			var wicked_area = 1 * height;
            var wicket_cost = price_section_m * wicked_area;
			if(wicket_count){
				total += Math.round(wicket_count * wicket_cost);
				mat += Math.round(wicket_count * wicket_cost);
				out += '<tr>'+
							'<td><p>Калитка шириной 1м.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(wicket_count * wicket_cost)+'</p></td>'+
						'</tr>';
			}
            
            out += '<tr>'+
						'<td align="right" style="text-align:right;" colspan="4"><p>Стоимость материалов:</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+mat+'</p></td>'+
					'</tr>';
            
            //Окрашивание каркаса забора
            var area_stolb = count_stolbi * (stolbi_width * 4);
            var pokr_gate = gate_area * gate_count;
            var pokr_wicked = wicked_area * wicket_count;
            var pokr_section = area_section * count_section;
            
			var coloration = $('#clc_coloration').val();
			var coloration_cost = Number($('#clc_coloration option:selected').attr('cost'));
            var color = $('#clc_color').val();
			total += Math.ceil(pokr_section * coloration_cost);
			mat += Math.ceil(pokr_section * coloration_cost);
			out += '<tr>'+
						'<td><p>Покрытие каркаса забора '+coloration+' ('+color+')</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+pokr_section+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>м2.</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+coloration_cost+'</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+Math.round(pokr_section * coloration_cost)+'</p></td>'+
					'</tr>';
            
            if(stolbi_name == 'Металлические'){
                total += Math.ceil(area_stolb * coloration_cost);
                mat += Math.ceil(area_stolb * coloration_cost);
                out += '<tr>'+
                            '<td><p>Покрытие столбов '+coloration+' ('+color+')</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+area_stolb+'</p></td>'+
                            '<td align="center" style="text-align:center;"><p>м2.</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+coloration_cost+'</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+Math.round(area_stolb * coloration_cost)+'</p></td>'+
                        '</tr>';
            }
            
            if(gate_count){
                total += Math.ceil(pokr_gate * coloration_cost);
                mat += Math.ceil(pokr_gate * coloration_cost);
                out += '<tr>'+
                            '<td><p>Покрытие ворот '+coloration+' ('+color+')</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+pokr_gate+'</p></td>'+
                            '<td align="center" style="text-align:center;"><p>м2.</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+coloration_cost+'</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+Math.round(pokr_gate * coloration_cost)+'</p></td>'+
                        '</tr>';
            }
            
            if(wicket_count){
                total += Math.ceil(pokr_wicked * coloration_cost);
                mat += Math.ceil(pokr_wicked * coloration_cost);
                out += '<tr>'+
                            '<td><p>Покрытие калитки '+coloration+' ('+color+')</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+pokr_wicked+'</p></td>'+
                            '<td align="center" style="text-align:center;"><p>м2.</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+coloration_cost+'</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+Math.round(pokr_wicked * coloration_cost)+'</p></td>'+
                        '</tr>';
            }
            
            //Установка секций и столбов
			var section_install = 0;
            var stolb_install = 0;
            var fund_install = 0;
            var inx_install = 0;
            
            if(stolbi_name == 'Металлические'){
                section_install = Number($('#section_met_install').val()) ? Number($('#section_met_install').val()) : 0;
                inx_install = Number($('#clc_install_met_stolb option:selected').index()) ? Number($('#clc_install_met_stolb option:selected').index()) : 0;
                
                if(inx_install == 1){
                    stolb_install = Number($('#clc_install_met_stolb option:selected').attr('cost')) ? Number($('#clc_install_met_stolb option:selected').attr('cost')) : 0;
                }
                else if(inx_install == 2){
                    fund_install = Number($('#clc_install_met_stolb option:selected').attr('cost')) ? Number($('#clc_install_met_stolb option:selected').attr('cost')) : 0;
                    stolb_install = Number($('#clc_install_met_stolb option:eq(1)').attr('cost')) ? Number($('#clc_install_met_stolb option:eq(1)').attr('cost')) : 0;
                }
            }
            else{
                section_install = Number($('#section_kir_install').val()) ? Number($('#section_kir_install').val()) : 0;
                stolb_install = Number($('#clc_install_kir_stolb option:selected').attr('cost2')) ? Number($('#clc_install_kir_stolb option:selected').attr('cost2')) : 0;
                fund_install = Number($('#clc_install_kir_stolb option:selected').attr('cost')) ? Number($('#clc_install_kir_stolb option:selected').attr('cost')) : 0;
            }
            
            if(stolb_install || fund_install){
                total += Math.round(count_section * section_install);
				job += Math.round(count_section * section_install);
                
                out += '<tr>'+
							'<td><p>Установка секций</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+count_section+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+section_install+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(count_section * section_install)+'</p></td>'+
						'</tr>';
                
                if(stolb_install){
                    total += Math.round(stolb_install * count_stolbi);
                    job += Math.round(stolb_install * count_stolbi);

                    out += '<tr>'+
                                '<td><p>Установка столбов</p></td>'+
                                '<td align="center" style="text-align:center;"><p>'+count_stolbi+'</p></td>'+
                                '<td align="center" style="text-align:center;"><p>шт.</p></td>'+
                                '<td align="center" style="text-align:center;"><p>'+stolb_install+'</p></td>'+
                                '<td align="center" style="text-align:center;"><p>'+Math.round(stolb_install * count_stolbi)+'</p></td>'+
                            '</tr>';
                }
                
                if(fund_install){
                    total += Math.round(fund_install * length);
                    job += Math.round(fund_install * length);

                    out += '<tr>'+
                                '<td><p>Ленточный фундамент</p></td>'+
                                '<td align="center" style="text-align:center;"><p>'+length+'</p></td>'+
                                '<td align="center" style="text-align:center;"><p>мп.</p></td>'+
                                '<td align="center" style="text-align:center;"><p>'+fund_install+'</p></td>'+
                                '<td align="center" style="text-align:center;"><p>'+Math.round(fund_install * length)+'</p></td>'+
                            '</tr>';
                }
            }
            
			//Установка ворот
			var gate_install = Number($('#clc_type_gate option:selected').attr('install')) ? Number($('#clc_type_gate option:selected').attr('install')) : 0;
            var gate_note = ' '+$('#clc_type_gate option:selected').attr('note');
			if(gate_count && (stolb_install || fund_install)){
				total += Math.round(gate_count * gate_install);
				job += Math.round(gate_count * gate_install);
				out += '<tr>'+
							'<td><p>Установка ворот'+gate_note+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+gate_install+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(gate_count * gate_install)+'</p></td>'+
						'</tr>';
			}
					
			//Установка калитки
			var wicket_install = Number($('#wicket_install').val());
			if(wicket_count && (stolb_install || fund_install)){
				total += wicket_count * wicket_install;
				job += wicket_count * wicket_install;
				out += '<tr>'+
							'<td><p>Установка калитки</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>шт.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+wicket_install+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(wicket_count * wicket_install)+'</p></td>'+
						'</tr>';
			}
			
            if(job){
                out += '<tr>'+
                            '<td align="right" style="text-align:right;" colspan="4"><p>Стоимость работ:</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+Math.round(job)+'</p></td>'+
                        '</tr>';
            }
					
			//Амортизация оборудования и расходники
			var amortizacia_cost = Number($('#amortizacia').val());
            total += Math.round(amortizacia_cost);
            dop += Math.round(amortizacia_cost);
            out += '<tr>'+
                        '<td><p>Амортизация оборудования и расходники</p></td>'+
                        '<td align="center" style="text-align:center;"><p>1</p></td>'+
                        '<td align="center" style="text-align:center;"><p>к-кт</p></td>'+
                        '<td align="center" style="text-align:center;"><p>'+amortizacia_cost+'</p></td>'+
                        '<td align="center" style="text-align:center;"><p>'+Math.round(amortizacia_cost)+'</p></td>'+
                    '</tr>';
            
            //Мкад
			var mkad_cost = Number($('#clc_mkad').attr('cost'));
			var mkad_count = Number($('#clc_mkad').val());
			
			if(mkad_count){
				total += mkad_count * mkad_cost;
                dop += mkad_count * mkad_cost;
				out += '<tr>'+
							'<td><p>Расстояние от МКАД</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+mkad_count+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>км.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+mkad_cost+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(mkad_count * mkad_cost)+'</p></td>'+
						'</tr>';
			}
					
			//Вывоз генератора
			var generator = $('#clc_generator').prop('checked') ? Number($('#clc_generator').val()) : 0;
			var generator_length = Number($('#clc_generator_length').val());
			var day_generator = Math.ceil(length / generator_length);
			
			if(generator){
				total += day_generator * generator;
                dop += day_generator * generator;
				out += '<tr>'+
							'<td><p>Вывоз генератора</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+day_generator+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>дн.</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+generator+'</p></td>'+
							'<td align="center" style="text-align:center;"><p>'+Math.round(day_generator * generator)+'</p></td>'+
						'</tr>';
			}
            
            if(dop){
                out += '<tr>'+
                            '<td align="right" style="text-align:right;" colspan="4"><p>Дополнительно:</p></td>'+
                            '<td align="center" style="text-align:center;"><p>'+dop+'</p></td>'+
                        '</tr>';
            }
            
            out += '<tr>'+
						'<td align="right" style="text-align:right;" colspan="4"><p>Итого:</p></td>'+
						'<td align="center" style="text-align:center;"><p>'+total+'</p></td>'+
					'</tr>';
			
			$('#clc_itog_mat').val(Math.round(mat));
			$('#clc_itog_job').val(Math.round(job));
			$('#clc_total').val(Math.round(total));
			$('#table-list-data').html(out);
			$('#list-data').val(out);
		}
    });
})(jQuery);