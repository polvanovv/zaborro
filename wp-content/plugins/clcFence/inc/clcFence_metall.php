<?php
/*
* Металлический штакетник
*/

function clcFence_metall_form(){
    ?>
        <div id="clcFence">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<?php 
							if(!empty($_POST['update'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_metall_form');
								
								$update_options = get_option('clcFence_options');
								
								$update_options['metall']['contacts']['emails'] = trim($_POST['metall_email']);
								$update_options['metall']['contacts']['address'] = trim($_POST['metall_address']);
								$update_options['metall']['contacts']['phones'] = trim($_POST['metall_phone']);
								
								$update_options['metall']['height'] = array();
								for($i = 0, $max = count($_POST['metall_height_name']); $i < $max; $i++){
									$update_options['metall']['height'][] = array('name' => trim($_POST['metall_height_name'][$i]), 'num' => trim($_POST['metall_height_num'][$i]));
								}
								
								$update_options['metall']['evroshtaketnik'] = array();
								for($i = 0, $max = count($_POST['metall_evroshtaketnik_name']); $i < $max; $i++){
									$update_options['metall']['evroshtaketnik'][] = array('name' => trim($_POST['metall_evroshtaketnik_name'][$i]), 'img' => trim($_POST['metall_evroshtaketnik_img'][$i]), 'price' => trim($_POST['metall_evroshtaketnik_price'][$i]), 'color' => trim($_POST['metall_evroshtaketnik_color'][$i]));
								}
								
								$update_options['metall']['rasst_stolb'] = array();
								for($i = 0, $max = count($_POST['metall_rasst_stolb_name']); $i < $max; $i++){
									$update_options['metall']['rasst_stolb'][] = array('name' => trim($_POST['metall_rasst_stolb_name'][$i]), 'num' => trim($_POST['metall_rasst_stolb_num'][$i]));
								}
								
								$update_options['metall']['zazor_shtak'][0] = array();
								for($i = 0, $max = count($_POST['metall_zazor_shtak_odna_storona_name']); $i < $max; $i++){
									$update_options['metall']['zazor_shtak'][0][] = array('name' => trim($_POST['metall_zazor_shtak_odna_storona_name'][$i]), 'num' => trim($_POST['metall_zazor_shtak_odna_storona_num'][$i]));
								}
								
								$update_options['metall']['zazor_shtak'][1] = array();
								for($i = 0, $max = count($_POST['metall_zazor_shtak_shach_poradok_name']); $i < $max; $i++){
									$update_options['metall']['zazor_shtak'][1][] = array('name' => trim($_POST['metall_zazor_shtak_shach_poradok_name'][$i]), 'num' => trim($_POST['metall_zazor_shtak_shach_poradok_num'][$i]));
								}
								
								$update_options['metall']['metall_stolbi'] = trim($_POST['metall_stolbi']);
								
								$update_options['metall']['lagi'] = trim($_POST['metall_lagi']);
								
								$update_options['metall']['pokritie_zabora'] = array();
								for($i = 0, $max = count($_POST['metall_pokritie_zabora_name']); $i < $max; $i++){
									$update_options['metall']['pokritie_zabora'][] = array('name' => trim($_POST['metall_pokritie_zabora_name'][$i]), 'price' => trim($_POST['metall_pokritie_zabora_price'][$i]));
								}
								
								$update_options['metall']['samorezi'] = array();
								for($i = 0, $max = count($_POST['metall_samorezi_name']); $i < $max; $i++){
									$update_options['metall']['samorezi'][] = array('name' => trim($_POST['metall_samorezi_name'][$i]), 'price' => trim($_POST['metall_samorezi_price'][$i]));
								}
								
								$update_options['metall']['zaglushki'] = trim($_POST['metall_zaglushki']);
								
								$update_options['metall']['vorota'] = array();
								for($i = 0, $max = count($_POST['metall_vorota_name']); $i < $max; $i++){
									$update_options['metall']['vorota'][] = array('name' => trim($_POST['metall_vorota_name'][$i]), 'price' => trim($_POST['metall_vorota_price'][$i]));
								}
								
								$update_options['metall']['kalitka'] = trim($_POST['metall_kalitka']);
								
								$update_options['metall']['ustanovka_zabora'] = trim($_POST['metall_ustanovka_zabora']);
								
								$update_options['metall']['ustanovka_stolbov'] = array();
								for($i = 0, $max = count($_POST['metall_ustanovka_stolbov_name']); $i < $max; $i++){
									$update_options['metall']['ustanovka_stolbov'][] = array('name' => trim($_POST['metall_ustanovka_stolbov_name'][$i]), 'price' => trim($_POST['metall_ustanovka_stolbov_price'][$i]));
								}
								
								$update_options['metall']['montaz_vorot'] = trim($_POST['metall_montaz_vorot']);
								$update_options['metall']['montaz_kalitki'] = trim($_POST['metall_montaz_kalitki']);
								
								$update_options['metall']['pokraska_karkasa_zabora'] = array();
								for($i = 0, $max = count($_POST['metall_vorota_name']); $i < $max; $i++){
									$update_options['metall']['pokraska_karkasa_zabora'][] = array('name' => trim($_POST['metall_pokraska_karkasa_zabora_name'][$i]), 'price' => trim($_POST['metall_pokraska_karkasa_zabora_price'][$i]));
								}
								
								$update_options['metall']['amortizacia'] = trim($_POST['metall_amortizacia']);
								
								$update_options['metall']['generator'] = trim($_POST['metall_generator']);
								$update_options['metall']['generator_length'] = trim($_POST['metall_generator_length']);
								
								$update_options['metall']['dostavka'] = trim($_POST['metall_dostavka']);
								
								update_option('clcFence_options', $update_options);
								
								?>
								<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Данные успешно обновлены.
								</div>
								<?php
							}
							if(!empty($_POST['reset'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_metall_form');
								
								global $clcFence_options;
								$update_options = get_option('clcFence_options');
								$update_options['metall'] = $clcFence_options['metall'];
                                update_option('clcFence_options', $update_options);
								?>
								<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Данные успешно восстановлены.
								</div>
								<?php
							}
						?>
						<form name="clcFence_metall_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=clcFence_metall.php" >
							<?php 
							wp_nonce_field('clcFence_metall_form');
							$list_options = get_option('clcFence_options');
							?>
							<h2>Заборы из металлического штакетника</h2><hr/>
                            
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  	<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading1">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
									  			Контактные данные
											</a>
								  		</h4>
									</div>
									<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
								  		<div class="panel-body">
											<div class="form-group" style="padding: 0 0 35px 0;">
												<div class="col-md-4" style="padding: 0 5px 0 0;">
													<input type="text" class="form-control" name="metall_email" placeholder="Email через запятую" value="<?php echo $list_options['metall']['contacts']['emails']; ?>" />
                                                    <p class="help-block">Контактные данные (email)</p>
												</div>
												<div class="col-md-4" style="padding: 0 5px 0 0;">
													<input type="text" class="form-control" name="metall_address" placeholder="Адрес" value="<?php echo $list_options['metall']['contacts']['address']; ?>" />
                                                    <p class="help-block">Контактные данные (адрес)</p>
												</div>
												<div class="col-md-4" style="padding: 0 5px 0 5px;">
													<input type="text" class="form-control" name="metall_phone" placeholder="Телефоны через запятую" value="<?php echo $list_options['metall']['contacts']['phones']; ?>" />
                                                    <p class="help-block">Контактные данные (телефоны через запятую)</p>
												</div>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading2">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
									  			Высота забора
											</a>
								  		</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['height']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_height_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['height'][$i]['name']; ?>" />
                                                        <p class="help-block">Высота забора (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_height_num[]" placeholder="Значение" value="<?php echo $list_options['metall']['height'][$i]['num']; ?>" />
                                                        <p class="help-block">Высота забора (значение)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading3">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
									  			Евроштакетник
											</a>
								  		</h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['evroshtaketnik']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0; clear: both;">
													<div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_evroshtaketnik_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['evroshtaketnik'][$i]['name']; ?>" />
                                                        <p class="help-block">Евроштакетник (наименование)</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_evroshtaketnik_img[]" placeholder="Изображение" value="<?php echo $list_options['metall']['evroshtaketnik'][$i]['img']; ?>" />
                                                        <p class="help-block">Изображение (загрузка на странице файлов)</p>
													</div>
                                                    <div class="col-md-2" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_evroshtaketnik_price[]" placeholder="Цены" value="<?php echo $list_options['metall']['evroshtaketnik'][$i]['price']; ?>" />
                                                        <p class="help-block">Цены по кол-ву высот забора, через запятую</p>
													</div>
                                                    <div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_evroshtaketnik_color[]" placeholder="Цены" value="<?php echo $list_options['metall']['evroshtaketnik'][$i]['color']; ?>" />
                                                        <p class="help-block">Доступные цвета, через запятую</p>
													</div>
													<div class="col-md-1" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading3_1">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse3_1" aria-expanded="true" aria-controls="collapse3_1">
									  			Расстояние между столбами
											</a>
								  		</h4>
									</div>
									<div id="collapse3_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3_1">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['rasst_stolb']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_rasst_stolb_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['rasst_stolb'][$i]['name']; ?>" />
                                                        <p class="help-block">Расстояние между столбами (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_rasst_stolb_num[]" placeholder="Значение" value="<?php echo $list_options['metall']['rasst_stolb'][$i]['num']; ?>" />
                                                        <p class="help-block">Расстояние между столбами (значение)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading3_2">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse3_2" aria-expanded="true" aria-controls="collapse3_2">
									  			Зазор между штакетником
											</a>
								  		</h4>
									</div>
									<div id="collapse3_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3_2">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['zazor_shtak'][0]); $i < $max; $i++): ?>
                                                <div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_zazor_shtak_odna_storona_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['zazor_shtak'][0][$i]['name']; ?>" />
                                                        <p class="help-block">Для одной стороны (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_zazor_shtak_odna_storona_num[]" placeholder="Значение" value="<?php echo $list_options['metall']['zazor_shtak'][0][$i]['num']; ?>" />
                                                        <p class="help-block">Для одной стороны (значение)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
                                        
                                        <div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['zazor_shtak'][1]); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_zazor_shtak_shach_poradok_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['zazor_shtak'][1][$i]['name']; ?>" />
                                                        <p class="help-block">В шахматном порядке (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_zazor_shtak_shach_poradok_num[]" placeholder="Значение" value="<?php echo $list_options['metall']['zazor_shtak'][1][$i]['num']; ?>" />
                                                        <p class="help-block">В шахматном порядке (значение)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading4">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
									  			Металлические столбы
											</a>
								  		</h4>
									</div>
									<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_stolbi" placeholder="Цена" value="<?php echo $list_options['metall']['metall_stolbi']; ?>" />
                                                <p class="help-block">Металлические столбы (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading5">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
									  			Лаги
											</a>
								  		</h4>
									</div>
									<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_lagi" placeholder="Цена" value="<?php echo $list_options['metall']['lagi']; ?>" />
                                                <p class="help-block">Лаги (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading6">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
									  			Покрытие каркаса забора
											</a>
								  		</h4>
									</div>
									<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['pokritie_zabora']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_pokritie_zabora_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['pokritie_zabora'][$i]['name']; ?>" />
                                                        <p class="help-block">Покрытие каркаса забора (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_pokritie_zabora_price[]" placeholder="Цена" value="<?php echo $list_options['metall']['pokritie_zabora'][$i]['price']; ?>" />
                                                        <p class="help-block">Покрытие каркаса забора (цена)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading7">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
									  			Саморезы
											</a>
								  		</h4>
									</div>
									<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['samorezi']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_samorezi_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['samorezi'][$i]['name']; ?>" />
                                                        <p class="help-block">Саморезы (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_samorezi_price[]" placeholder="Цена" value="<?php echo $list_options['metall']['samorezi'][$i]['price']; ?>" />
                                                        <p class="help-block">Саморезы (цена)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading8">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">
									  			Пластиковые заглушки
											</a>
								  		</h4>
									</div>
									<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_zaglushki" placeholder="Цена" value="<?php echo $list_options['metall']['zaglushki']; ?>" />
                                                <p class="help-block">Пластиковые заглушки (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading9">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="true" aria-controls="collapse9">
									  			Ворота
											</a>
								  		</h4>
									</div>
									<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['vorota']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_vorota_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['vorota'][$i]['name']; ?>" />
                                                        <p class="help-block">Ворота (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_vorota_price[]" placeholder="Цена" value="<?php echo $list_options['metall']['vorota'][$i]['price']; ?>" />
                                                        <p class="help-block">Ворота (цена)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading10">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="true" aria-controls="collapse10">
									  			Калитка
											</a>
								  		</h4>
									</div>
									<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_kalitka" placeholder="Цена" value="<?php echo $list_options['metall']['kalitka']; ?>" />
                                                <p class="help-block">Калитка (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading11">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="true" aria-controls="collapse11">
									  			Установка забора
											</a>
								  		</h4>
									</div>
									<div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_ustanovka_zabora" placeholder="Цена" value="<?php echo $list_options['metall']['ustanovka_zabora']; ?>" />
                                                <p class="help-block">Установка забора (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading12">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="true" aria-controls="collapse12">
									  			Установка столбов
											</a>
								  		</h4>
									</div>
									<div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['ustanovka_stolbov']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_ustanovka_stolbov_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['ustanovka_stolbov'][$i]['name']; ?>" />
                                                        <p class="help-block">Установка столбов (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_ustanovka_stolbov_price[]" placeholder="Цена" value="<?php echo $list_options['metall']['ustanovka_stolbov'][$i]['price']; ?>" />
                                                        <p class="help-block">Установка столбов (цена)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading14">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="true" aria-controls="collapse14">
									  			Монтаж
											</a>
								  		</h4>
									</div>
									<div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_montaz_vorot" placeholder="Цена" value="<?php echo $list_options['metall']['montaz_vorot']; ?>" />
                                                <p class="help-block">Монтаж ворот (цена)</p>
											</div>
                                            <div class="form-group">
												<input type="text" class="form-control" name="metall_montaz_kalitki" placeholder="Цена" value="<?php echo $list_options['metall']['montaz_kalitki']; ?>" />
                                                <p class="help-block">Монтаж калитки (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading15">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse15" aria-expanded="true" aria-controls="collapse15">
									  			Покраска каркаса забора
											</a>
								  		</h4>
									</div>
									<div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['metall']['pokraska_karkasa_zabora']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_pokraska_karkasa_zabora_name[]" placeholder="Наименование" value="<?php echo $list_options['metall']['pokraska_karkasa_zabora'][$i]['name']; ?>" />
                                                        <p class="help-block">Покраска каркаса забора (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="metall_pokraska_karkasa_zabora_price[]" placeholder="Цена" value="<?php echo $list_options['metall']['pokraska_karkasa_zabora'][$i]['price']; ?>" />
                                                        <p class="help-block">Покраска каркаса забора (цена)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading16">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse16" aria-expanded="true" aria-controls="collapse16">
									  			Амортизация
											</a>
								  		</h4>
									</div>
									<div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_amortizacia" placeholder="Цена" value="<?php echo $list_options['metall']['amortizacia']; ?>" />
                                                <p class="help-block">Амортизация оборудования и расходники (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading17">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse17" aria-expanded="true" aria-controls="collapse17">
									  			Генератор
											</a>
								  		</h4>
									</div>
									<div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_generator" placeholder="Цена" value="<?php echo $list_options['metall']['generator']; ?>" />
                                                <p class="help-block">Вывоз генератора (цена)</p>
											</div>
                                            <div class="form-group">
												<input type="text" class="form-control" name="metall_generator_length" placeholder="Цена" value="<?php echo $list_options['metall']['generator_length']; ?>" />
                                                <p class="help-block">Вывоз генератора (двойной тариф мп.)</p>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading18">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse18" aria-expanded="true" aria-controls="collapse18">
									  			Доставка
											</a>
								  		</h4>
									</div>
									<div id="collapse18" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="metall_dostavka" placeholder="Цена" value="<?php echo $list_options['metall']['dostavka']; ?>" />
                                                <p class="help-block">Доставка (цена 1км)</p>
											</div>
								  		</div>
									</div>
                                </div>
							</div>
							<div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                	<button name="reset" type="submit" class="btn btn-default btn-lg btn-block" value="1">Сброс настроек</button>
                                </div>
                                <div class="btn-group" role="group">
                                	<button name="update" type="submit" class="btn btn-primary btn-lg btn-block" value="1">Сохранить</button>
                                </div>
                            </div>
							<br/>
							<pre>Вывод калькулятора на странице: [clcFence_metall]</pre>
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="page_options" value="" />
						</form>
					</div>
				</div>
			</div>
		</div>
    <? 
}
?>