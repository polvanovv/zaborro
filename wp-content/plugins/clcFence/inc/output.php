<?php
//Заборы из профнастила
function clcFence_profnastil_func_output(){
	$list_options = get_option('clcFence_options');
	@$send_order = $_POST['send-order'] ? $_POST['send-order'] : '';
	?>
	<!--<link href="<?php echo PATCH_CLCFENCE; ?>css/bootstrap.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo PATCH_CLCFENCE; ?>css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

	<!--<script src="<?php echo PATCH_CLCFENCE; ?>js/bootstrap.js" type="text/javascript"></script>-->
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine-ru.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/profnastil.js" type="text/javascript"></script>

	<style type="text/css">
	#clcFence{
		padding-top: 40px;
	}
	#clcFence label{
		text-align: left;
		font-size: 14px;
	}
	#clcFence table tr td:first-child {
		background: white;
	}
	#clcFence .btn {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fdb700 0%, #fb7501 100%) repeat scroll 0 0;
		border: none !important;
	}
	#clcFence .btn:hover,
	#clcFence .btn:focus {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fb7501 0%, #fdb700 100%) repeat scroll 0 0;
		color: #fff;
	}
	</style>
	<?php
	$send_mess = '';
	if($send_order){
        $gRecaptchaResponse = checkRecaptchaResponse($_POST['g-recaptcha-response']);

        if (!$gRecaptchaResponse) {
            $send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Вы не прошли верификацию reCaptcha.
							</div>';
        } else {
            $send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
						</div>';

            add_filter('wp_mail_content_type','set_contenttype');
            function set_contenttype($content_type){
                return 'text/html';
            }

            remove_all_filters('wp_mail_from');
            remove_all_filters('wp_mail_from_name');

            //$headers = 'From: '.$_POST['clc_name'].' <'.$_POST['clc_email'].'>' . "\r\n";
            $headers = 'From: '.$_POST['clc_name'].' <support@zaborro.ru>' . "\r\n";

            $text = '';
            $text .= !empty($_POST['clc_name']) ? 'Имя: '.trim($_POST['clc_name']).'<br/>' : '';
            $text .= !empty($_POST['clc_phone']) ? 'Телефон: '.trim($_POST['clc_phone']).'<br/>' : '';
            $text .= !empty($_POST['clc_email']) ? 'Email: '.trim($_POST['clc_email']).'<br/>' : '';
            $text .= !empty($_POST['clc_comment']) ? 'Комментарий: '.trim($_POST['clc_comment']).'<br/>' : '';
            $text .= '<br/><hr/>';
            $text .= '<table cellpadding="5" cellspacing="5" border="1">';
            $text .= $_POST['list-data'];
            $text .= '</table>';

            if($list_options['profnastil']['contacts']['emails']){
                $admin_email = $list_options['profnastil']['contacts']['emails'];
            }
            else{
                $admin_email = get_bloginfo('admin_email');
            }

            if(wp_mail($admin_email, 'Заказ забора из профнастила', $text, $headers)){
                $send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
							</div>';
            }
            else{
                $send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> При отправке заказа возникла ошибка.
							</div>';
            }
        }
	}
	?>
	<div id="clcFence">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<?php echo $send_mess; ?>
					<form id="clcFence-send-form" class="form-horizontal" method="post">
						<div class="col-md-8" style="padding-right: 30px; border-right: 1px solid #CCC;">
							<div class="form-group">
                                <label for="clc_length" class="col-sm-4 control-label">Общая длина:</label>
                                <div class="col-sm-8">
                                    <input name="clc_length" type="text" class="form-control" id="clc_length" placeholder="Общая длина (число)" />
                                </div>
                            </div>
							<div class="form-group">
                                <label for="clc_height" class="col-sm-4 control-label">Высота забора:</label>
                                <div class="col-sm-8">
                                    <select id="clc_height" name="clc_height" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['profnastil']['height']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['profnastil']['height'][$i]['cost']; ?>" value="<?php echo $list_options['profnastil']['height'][$i]['num']; ?>"><?php echo $list_options['profnastil']['height'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
                                <label for="clc_lagi" class="col-sm-4 control-label">Кол-во лаг (горизонталей):</label>
                                <div class="col-sm-8">
                                    <select id="clc_lagi" name="clc_lagi" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['profnastil']['lagi']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['profnastil']['lagi'][$i]['cost']; ?>" value="<?php echo $list_options['profnastil']['lagi'][$i]['name']; ?>"><?php echo $list_options['profnastil']['lagi'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_cover" class="col-sm-4 control-label">Вид покрытия:</label>
                                <div class="col-sm-8">
                                    <select id="clc_cover" name="clc_cover" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['profnastil']['cover']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> per="<?php echo $list_options['profnastil']['cover'][$i]['per']; ?>" cost="<?php echo $list_options['profnastil']['cover'][$i]['cost']; ?>" value="<?php echo $list_options['profnastil']['cover'][$i]['name']; ?>"><?php echo $list_options['profnastil']['cover'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_coloration" class="col-sm-4 control-label">Окрашивание каркаса забора:</label>
                                <div class="col-sm-8">
                                    <select id="clc_coloration" name="clc_coloration" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['profnastil']['coloration']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['profnastil']['coloration'][$i]['cost']; ?>" value="<?php echo $list_options['profnastil']['coloration'][$i]['name']; ?>"><?php echo $list_options['profnastil']['coloration'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_count_gate" class="col-sm-4 control-label">Кол-во ворот:</label>
                                <div class="col-sm-8">
                                    <select id="clc_count_gate" name="clc_count_gate" class="form-control">
                                        <option selected value="0">Нет</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_width_gate" class="col-sm-4 control-label">Ширина ворот (м):</label>
                                <div class="col-sm-8">
                                    <select id="clc_width_gate" name="clc_width_gate" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['profnastil']['gate']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['profnastil']['gate'][$i]['cost']; ?>" value="<?php echo $list_options['profnastil']['gate'][$i]['name']; ?>"><?php echo $list_options['profnastil']['gate'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_wicket" class="col-sm-4 control-label">Кол-во калиток:</label>
                                <div class="col-sm-8">
                                    <select id="clc_wicket" name="clc_wicket" class="form-control">
                                        <option selected value="0">Нет</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                    <input id="clc_wicket_cost" name="clc_wicket_cost" type="hidden" value="<?php echo $list_options['profnastil']['wicket']; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_mkad" class="col-sm-4 control-label">Расстояние от МКАД:</label>
                                <div class="col-sm-8">
                                    <input cost="<?php echo $list_options['profnastil']['mkad']; ?>" name="clc_mkad" type="text" class="form-control" id="clc_mkad" placeholder="Расстояние от МКАД" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_generator" class="col-sm-4 control-label">Вывоз генератора (если нет электричества):</label>
                                <div class="col-sm-8 checkbox">
                                    <label>
                                         <input name="clc_generator" type="checkbox" id="clc_generator" value="<?php echo $list_options['profnastil']['generator']; ?>" />
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input id="fence_install" name="fence_install" type="hidden" value="<?php echo $list_options['profnastil']['fence_install']; ?>" />
                        <input id="gate_install" name="gate_install" type="hidden" value="<?php echo $list_options['profnastil']['gate_install']; ?>" />
                        <input id="wicket_install" name="wicket_install" type="hidden" value="<?php echo $list_options['profnastil']['wicket_install']; ?>" />
                        <input id="clc_generator_length" name="clc_generator_length" type="hidden" value="<?php echo $list_options['profnastil']['generator_length']; ?>" />
                        <input id="fence_job" name="fence_job" type="hidden" value="<?php echo $list_options['profnastil']['fence_job']; ?>" />
                        <input id="fence_address" name="fence_address" type="hidden" value="<?php echo $list_options['profnastil']['contacts']['address']; ?>" />
                        <input id="fence_phones" name="fence_phones" type="hidden" value="<?php echo $list_options['profnastil']['contacts']['phones']; ?>" />
                        <div class="col-md-4" style="padding-left: 30px;">
                            <div class="thumbnail"><img src="<?php echo PATCH_CLCFENCE; ?>img/profnastil.png" /></div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_mat" class="control-label">Итого за материалы:</label>
                                <input name="clc_itog_mat" type="text" class="form-control" id="clc_itog_mat" placeholder="Итого за материалы" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_job" class="control-label">Стоимость работ:</label>
                                <input name="clc_itog_job" type="text" class="form-control" id="clc_itog_job" placeholder="Стоимость работ" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_total" class="control-label">Итого за материалы и работу:</label>
                                <input name="clc_total" type="text" class="form-control validate[required,min[1]]" id="clc_total" placeholder="Итого за материалы и работу" readonly value="0" />
                            </div>
                            <br/>
                            <button name="update" type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#order_modal">Оставить заявку</button><br/>
                            <button id="print_order" name="print_order" type="button" class="btn btn-default btn-lg btn-block" value="1">Распечатать смету</button>
                        </div>
                        <div class="col-md-12">
                            <br/>
                            <table id="table-list-data" class="table table-condensed table-hover table-bordered table-striped">
                                <tr>
                                    <th style="text-align:center;"><h4>Наименование</h4></th>
                                    <th style="text-align:center;"><h4>Количество</h4></th>
                                    <th style="text-align:center;"><h4>Ед. изм.</h4></th>
                                    <th style="text-align:center;"><h4>Цена</h4></th>
                                    <th style="text-align:center;"><h4>Сумма</h4></th>
                                </tr>
                            </table>
                        </div>
                        <input id="list-data" name="list-data" type="hidden" value="" />
                        <div id="order_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Отправить заказ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Ваше имя *</label>
                                            <input name="clc_name" type="text" class="form-control validate[required]" id="clc_name" placeholder="Ваше имя" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_phone" class="control-label">Ваш телефон *</label>
                                            <input name="clc_phone" type="text" class="form-control validate[required,custom[phone]]" id="clc_phone" placeholder="Ваш телефон" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_email" class="control-label">Ваш email *</label>
                                            <input name="clc_email" type="text" class="form-control validate[required,custom[email]]" id="clc_email" placeholder="Ваш email" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Комментарий</label>
                                            <textarea name="clc_comment" id="clc_comment" placeholder="Комментарий" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="g-recaptcha" data-callback="correctCaptcha"  data-sitekey="<?php echo get_option('clcFence_options')['profnastil']['profnastil_captcha_key']; ?>"></div>
                                        <div>
                                            <input type="checkbox" class="validate[required,alertTextCheckboxe]" id="agreement" name="agreement">
                                            <label for="agreement">Cогласен с <a href="/privacy">политикой конфиденцильности</a> и <a href="/terms">пользовательским соглашением</a>.</label>
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                        <button type="submit" class="btn btn-primary" value="1" name="send-order">Отправить</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </form>
				</div>
			</div>
		</div>
	</div>
    <?php
}

//Металлический штакетник
function clcFence_metall_func_output(){
	$list_options = get_option('clcFence_options');
	@$send_order = $_POST['send-order'] ? $_POST['send-order'] : '';
	?>
	<!--<link href="<?php echo PATCH_CLCFENCE; ?>css/bootstrap.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo PATCH_CLCFENCE; ?>css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	
	<!--<script src="<?php echo PATCH_CLCFENCE; ?>js/bootstrap.js" type="text/javascript"></script>-->
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine-ru.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/metall.js" type="text/javascript"></script>
	
	<style type="text/css">
	#clcFence{
		padding-top: 40px;
	}
	#clcFence label{
		text-align: left;
		font-size: 14px;
	}
	#clcFence table tr td:first-child {
		background: white;
	}
	#clcFence .btn {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fdb700 0%, #fb7501 100%) repeat scroll 0 0;
		border: none !important;
	}
	#clcFence .btn:hover,
	#clcFence .btn:focus {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fb7501 0%, #fdb700 100%) repeat scroll 0 0;
		color: #fff;
	}
	</style>
	<?php
	$send_mess = '';
	if($send_order) {
        $gRecaptchaResponse = checkRecaptchaResponse($_POST['g-recaptcha-response']);

        if (!$gRecaptchaResponse) {
            $send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Вы не прошли верификацию reCaptcha.
							</div>';
        } else {
            $send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
						</div>';

            add_filter('wp_mail_content_type', 'set_contenttype');
            function set_contenttype($content_type)
            {
                return 'text/html';
            }

            remove_all_filters('wp_mail_from');
            remove_all_filters('wp_mail_from_name');

            //$headers = 'From: '.$_POST['clc_name'].' <'.$_POST['clc_email'].'>' . "\r\n";
            $headers = 'From: '.$_POST['clc_name'].' <support@zaborro.ru>'."\r\n";

            $text = '';
            $text .= !empty($_POST['clc_name']) ? 'Имя: '.trim(
                    $_POST['clc_name']
                ).'<br/>' : '';
            $text .= !empty($_POST['clc_phone']) ? 'Телефон: '.trim(
                    $_POST['clc_phone']
                ).'<br/>' : '';
            $text .= !empty($_POST['clc_email']) ? 'Email: '.trim(
                    $_POST['clc_email']
                ).'<br/>' : '';
            $text .= !empty($_POST['clc_comment']) ? 'Комментарий: '.trim(
                    $_POST['clc_comment']
                ).'<br/>' : '';
            $text .= '<br/><hr/>';
            $text .= '<table cellpadding="5" cellspacing="5" border="1">';
            $text .= $_POST['list-data'];
            $text .= '</table>';

            if ($list_options['metall']['contacts']['emails']) {
                $admin_email = $list_options['metall']['contacts']['emails'];
            } else {
                $admin_email = get_bloginfo('admin_email');
            }

            if (wp_mail(
                $admin_email,
                'Заказ забора из металлического штакетника',
                $text,
                $headers
            )) {
                $send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
							</div>';
            } else {
                $send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> При отправке заказа возникла ошибка.
							</div>';
            }
        }
    }
	?>
	<div id="clcFence">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<?php echo $send_mess; ?>
					<form id="clcFence-send-form" class="form-horizontal" method="post">
						<div class="col-md-8" style="padding-right: 30px; border-right: 1px solid #CCC;">
							<div class="form-group">
                                <label for="clc_length" class="col-sm-4 control-label">Длина забора:</label>
                                <div class="col-sm-8">
                                    <input name="clc_length" type="text" class="form-control" id="clc_length" placeholder="Общая длина (число)" />
                                </div>
                            </div>
							<div class="form-group">
                                <label for="clc_height" class="col-sm-4 control-label">Высота забора:</label>
                                <div class="col-sm-8">
                                    <select id="clc_height" name="clc_height" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['height']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['metall']['height'][$i]['num']; ?>"><?php echo $list_options['metall']['height'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
                                <label for="clc_evroshtaketnik" class="col-sm-4 control-label">Тип евроштакетника:</label>
                                <div class="col-sm-8">
                                    <select id="clc_evroshtaketnik" name="clc_evroshtaketnik" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['evroshtaketnik']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> color="<?php echo $list_options['metall']['evroshtaketnik'][$i]['color']; ?>" img="<?php echo $list_options['metall']['evroshtaketnik'][$i]['img']; ?>" cost="<?php echo $list_options['metall']['evroshtaketnik'][$i]['price']; ?>" value="<?php echo $list_options['metall']['evroshtaketnik'][$i]['name']; ?>"><?php echo $list_options['metall']['evroshtaketnik'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_color" class="col-sm-4 control-label">Цвет евроштакетника:</label>
                                <div class="col-sm-8">
                                    <?php $arr_colors = explode(',', $list_options['metall']['evroshtaketnik'][0]['color']); ?>
                                    <select id="clc_color" name="clc_color" class="form-control">
                                    <?php for($i = 0, $max = count($arr_colors); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $arr_colors[$i]; ?>"><?php echo $arr_colors[$i]; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_rasst_stolb" class="col-sm-4 control-label">Расстояние между столбами:</label>
                                <div class="col-sm-8">
                                    <select id="clc_rasst_stolb" name="clc_rasst_stolb" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['rasst_stolb']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['metall']['rasst_stolb'][$i]['num']; ?>"><?php echo $list_options['metall']['rasst_stolb'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
                            <div class="form-group">
                                <label for="clc_lagi" class="col-sm-4 control-label">Количество лаг между столбами:</label>
                                <div class="col-sm-8">
                                    <select id="clc_lagi" name="clc_lagi" class="form-control">
                                        <option selected cost="<?php echo $list_options['metall']['lagi']; ?>" value="2">2шт.</option>
                                        <option cost="<?php echo $list_options['metall']['lagi']; ?>" value="3">3шт.</option>
                                        <option cost="<?php echo $list_options['metall']['lagi']; ?>" value="4">4шт.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_coloration" class="col-sm-4 control-label">Антикоррозийная краска:</label>
                                <div class="col-sm-8">
                                    <select id="clc_coloration" name="clc_coloration" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['pokritie_zabora']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['metall']['pokritie_zabora'][$i]['price']; ?>" value="<?php echo $list_options['metall']['pokritie_zabora'][$i]['name']; ?>"><?php echo $list_options['metall']['pokritie_zabora'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_type_install" class="col-sm-4 control-label">Тип установки штакетин:</label>
                                <div class="col-sm-8">
                                    <select id="clc_type_install" name="clc_type_install" class="form-control">
                                        <option selected value="Штакетник с одной стороны">Штакетник с одной стороны</option>
                                        <option value="В шахматном порядке">В шахматном порядке</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_zazor_shtak1" class="col-sm-4 control-label">Зазор между штакетником:</label>
                                <div class="col-sm-8">
                                    <select id="clc_zazor_shtak1" name="clc_zazor_shtak" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['zazor_shtak'][0]); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['metall']['zazor_shtak'][0][$i]['num']; ?>"><?php echo $list_options['metall']['zazor_shtak'][0][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label for="clc_zazor_shtak2" class="col-sm-4 control-label">Зазор между штакетником:</label>
                                <div class="col-sm-8">
                                    <select id="clc_zazor_shtak2" name="clc_zazor_shtak" class="form-control" disabled>
                                    <?php for($i = 0, $max = count($list_options['metall']['zazor_shtak'][1]); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['metall']['zazor_shtak'][1][$i]['num']; ?>"><?php echo $list_options['metall']['zazor_shtak'][1][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_vorota" class="col-sm-4 control-label">Ворота размер:</label>
                                <div class="col-sm-8">
                                    <select id="clc_vorota" name="clc_vorota" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['vorota']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['metall']['vorota'][$i]['price']; ?>" value="<?php echo $list_options['metall']['vorota'][$i]['name']; ?>"><?php echo $list_options['metall']['vorota'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_count_vorota" class="col-sm-4 control-label">Кол-во ворот:</label>
                                <div class="col-sm-8">
                                    <select id="clc_count_vorota" name="clc_count_vorota" class="form-control">
                                        <option selected value="0">Нет</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_count_kalitka" class="col-sm-4 control-label">Калитка кол-во:</label>
                                <div class="col-sm-8">
                                    <select id="clc_count_kalitka" name="clc_count_kalitka" class="form-control">
                                        <option cost="0" selected value="0">Нет</option>
                                        <option cost="<?php echo $list_options['metall']['kalitka']; ?>" value="1">1</option>
                                        <option cost="<?php echo $list_options['metall']['kalitka']; ?>" value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_samorezi" class="col-sm-4 control-label">Саморезы:</label>
                                <div class="col-sm-8">
                                    <select id="clc_samorezi" name="clc_samorezi" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['samorezi']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['metall']['samorezi'][$i]['price']; ?>" value="<?php echo $list_options['metall']['samorezi'][$i]['name']; ?>"><?php echo $list_options['metall']['samorezi'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_zaglushki" class="col-sm-4 control-label">Пластиковые заглушки:</label>
                                <div class="col-sm-8">
                                    <select id="clc_zaglushki" name="clc_zaglushki" class="form-control">
                                        <option cost="0" selected value="Нет">Нет</option>
                                        <option cost="<?php echo $list_options['metall']['zaglushki']; ?>" value="Да">Да</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_install_stolbi" class="col-sm-4 control-label">Установка столбов:</label>
                                <div class="col-sm-8">
                                    <select id="clc_install_stolbi" name="clc_install_stolbi" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['metall']['ustanovka_stolbov']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['metall']['ustanovka_stolbov'][$i]['price']; ?>" value="<?php echo $list_options['metall']['ustanovka_stolbov'][$i]['name']; ?>"><?php echo $list_options['metall']['ustanovka_stolbov'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_mkad" class="col-sm-4 control-label">Расстояние от МКАД:</label>
                                <div class="col-sm-8">
                                    <input cost="<?php echo $list_options['metall']['dostavka']; ?>" name="clc_mkad" type="text" class="form-control" id="clc_mkad" placeholder="Расстояние от МКАД" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_install_zabor" class="col-sm-4 control-label">Установка забора:</label>
                                <div class="col-sm-8 checkbox">
                                    <label>
                                         <input name="clc_install_zabor" type="checkbox" id="clc_install_zabor" value="Установка забора" />
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="clc_generator" class="col-sm-4 control-label">Вывоз генератора (если нет электричества):</label>
                                <div class="col-sm-8 checkbox">
                                    <label>
                                         <input name="clc_generator" type="checkbox" id="clc_generator" value="<?php echo $list_options['metall']['generator']; ?>" />
                                    </label>
                                </div>
                            </div>
						</div>
                        
                        <input id="generator_length" name="generator_length" type="hidden" value="<?php echo $list_options['metall']['generator_length']; ?>" />
                        <input id="amortizacia" name="amortizacia" type="hidden" value="<?php echo $list_options['metall']['amortizacia']; ?>" />
                        <input id="montaz_vorot" name="montaz_vorot" type="hidden" value="<?php echo $list_options['metall']['montaz_vorot']; ?>" />
                        <input id="montaz_kalitki" name="montaz_kalitki" type="hidden" value="<?php echo $list_options['metall']['montaz_kalitki']; ?>" />
                        <input id="ustanovka_zabora" name="ustanovka_zabora" type="hidden" value="<?php echo $list_options['metall']['ustanovka_zabora']; ?>" />
                        <input id="metall_stolbi" name="metall_stolbi" type="hidden" value="<?php echo $list_options['metall']['metall_stolbi']; ?>" />
                        <input id="clc_generator_length" name="clc_generator_length" type="hidden" value="<?php echo $list_options['metall']['generator_length']; ?>" />
                        <input id="fence_address" name="fence_address" type="hidden" value="<?php echo $list_options['metall']['contacts']['address']; ?>" />
                        <input id="fence_phones" name="fence_phones" type="hidden" value="<?php echo $list_options['metall']['contacts']['phones']; ?>" />
                        <div class="col-md-4" style="padding-left: 30px;">
                            <div class="thumbnail"><img id="clc-img" src="<?php echo PATCH_CLCFENCE; ?>img/uploads/YUniks_Premium_strukturnyi__matovyi_2_cveta.jpg" /></div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_mat" class="control-label">Итого за материалы:</label>
                                <input name="clc_itog_mat" type="text" class="form-control" id="clc_itog_mat" placeholder="Итого за материалы" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_job" class="control-label">Стоимость работ:</label>
                                <input name="clc_itog_job" type="text" class="form-control" id="clc_itog_job" placeholder="Стоимость работ" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_total" class="control-label">Итого:</label>
                                <input name="clc_total" type="text" class="form-control validate[required,min[1]]" id="clc_total" placeholder="Итого за материалы и работу" readonly value="0" />
                            </div>
                            <br/>
                            <button name="update" type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#order_modal">Оставить заявку</button><br/>
                            <button id="print_order" name="print_order" type="button" class="btn btn-default btn-lg btn-block" value="1">Распечатать смету</button>
                        </div>
                        <div class="col-md-12">
                            <br/>
                            <table id="table-list-data" class="table table-condensed table-hover table-bordered table-striped">
                                <tr>
                                    <th style="text-align:center;"><h4>Наименование</h4></th>
                                    <th style="text-align:center;"><h4>Количество</h4></th>
                                    <th style="text-align:center;"><h4>Ед. изм.</h4></th>
                                    <th style="text-align:center;"><h4>Цена</h4></th>
                                    <th style="text-align:center;"><h4>Сумма</h4></th>
                                </tr>
                            </table>
                        </div>
                        <input id="list-data" name="list-data" type="hidden" value="" />
                        <div id="order_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Отправить заказ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Ваше имя *</label>
                                            <input name="clc_name" type="text" class="form-control validate[required]" id="clc_name" placeholder="Ваше имя" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_phone" class="control-label">Ваш телефон *</label>
                                            <input name="clc_phone" type="text" class="form-control validate[required,custom[phone]]" id="clc_phone" placeholder="Ваш телефон" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_email" class="control-label">Ваш email *</label>
                                            <input name="clc_email" type="text" class="form-control validate[required,custom[email]]" id="clc_email" placeholder="Ваш email" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Комментарий</label>
                                            <textarea name="clc_comment" id="clc_comment" placeholder="Комментарий" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer" >
                                        <div class="g-recaptcha" data-callback="correctCaptcha"    data-sitekey="<?php echo get_option('clcFence_options')['profnastil']['profnastil_captcha_key']; ?>"></div>
                                        <div>
                                            <input type="checkbox" class="validate[required,alertTextCheckboxe]"
                                                   id="agreement" name="agreement">
                                            <label for="agreement">Cогласен с <a href="/privacy">политикой конфиденцильности</a> и <a href="/terms">пользовательским соглашением</a>.</label>
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                        <button type="submit" class="btn btn-primary" value="1" name="send-order">Отправить</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </form>
				</div>
			</div>
		</div>
	</div>
    <?php
}

//Сварной забор
function clcFence_svarnoi_func_output(){
	$list_options = get_option('clcFence_options');
	@$send_order = $_POST['send-order'] ? $_POST['send-order'] : '';
	?>
	<!--<link href="<?php echo PATCH_CLCFENCE; ?>css/bootstrap.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo PATCH_CLCFENCE; ?>css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	
	<!--<script src="<?php echo PATCH_CLCFENCE; ?>js/bootstrap.js" type="text/javascript"></script>-->
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine-ru.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/svarnoi.js" type="text/javascript"></script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	
	<style type="text/css">
	#clcFence{
		padding-top: 40px;
	}
	#clcFence label{
		text-align: left;
		font-size: 14px;
	}
	#clcFence table tr td:first-child {
		background: white;
	}
	#clcFence .btn {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fdb700 0%, #fb7501 100%) repeat scroll 0 0;
		border: none !important;
	}
	#clcFence .btn:hover,
	#clcFence .btn:focus {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fb7501 0%, #fdb700 100%) repeat scroll 0 0;
		color: #fff;
	}
    #clcFence .sketch-art{
        color: #23527c;
    }
    #clcFence .sketch-art{
        color: #23527c;
        font-size: 13px;
        margin: 0;
        text-align: center;
        font-weight: bold;
    }
    #clcFence .sketch-price{
        color: #fb7401;
        font-size: 12px;
        margin: 0;
        text-align: center;
        font-weight: bold;
    }
    #clcFence .sketch-unit{
        color: #636464;
        font-size: 11px;
        text-align: center;
    }
    #clcFence .wrap-sketch .thumbnail{
        margin-right: 5px;
        position: relative;
        float: left;
    }
    #clcFence .wrap-sketch .thumbnail img{
        height: 95px;
        width: 145px;
    }
    #clcFence .wrap-sketch .thumbnail.active{
        box-shadow: 0 0 3px #FB7901;
    }
    #clcFence .wrap-sketch .thumbnail .btn{
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #23527c 0%, #266faf 100%) repeat scroll 0 0;
        color: #FFFFFF;
    }
    #clcFence .wrap-sketch .thumbnail.active .btn{
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fdb700 0%, #fb7501 100%) repeat scroll 0 0;
    }
    #clcFence .wrap-sketch .thumbnail .label-success{
        position: absolute;
        right: 1px;
        top: 1px;
    }
    #clcFence .wrap-zagl-met .thumbnail,
    #clcFence .wrap-zagl-kir .thumbnail{
        margin-right: 5px;
        position: relative;
        float: left;
        cursor: pointer;
    }  
    #clcFence .wrap-zagl-met .thumbnail .badge,
    #clcFence .wrap-zagl-kir .thumbnail .badge{
        background-color: #FB7901;
        position: absolute;
        right: 3px;
        top: 3px;
    }
    #clcFence .wrap-zagl-met .thumbnail img,
    #clcFence .wrap-zagl-kir .thumbnail img{
        max-height: 100px;
    }
    #clcFence .wrap-zagl-met .thumbnail.active,
    #clcFence .wrap-zagl-kir .thumbnail.active{
        box-shadow: 0 0 3px #FB7901;
    }
    #clcFence #clc_color option:disabled{
        color: #9A1013;
        text-decoration: line-through;
    }
	</style>
	<?php
	$send_mess = '';
	if($send_order){
        $gRecaptchaResponse = checkRecaptchaResponse($_POST['g-recaptcha-response']);

        if (!$gRecaptchaResponse) {
            $send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Вы не прошли верификацию reCaptcha.
							</div>';
        } else {
		$send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
						</div>';

		add_filter('wp_mail_content_type','set_contenttype');
		function set_contenttype($content_type){
			return 'text/html';
		}
		
		remove_all_filters('wp_mail_from');
		remove_all_filters('wp_mail_from_name');
		
		//$headers = 'From: '.$_POST['clc_name'].' <'.$_POST['clc_email'].'>' . "\r\n";
        $headers = 'From: '.$_POST['clc_name'].' <support@zaborro.ru>' . "\r\n";
		
		$text = '';
		$text .= !empty($_POST['clc_name']) ? 'Имя: '.trim($_POST['clc_name']).'<br/>' : '';
		$text .= !empty($_POST['clc_phone']) ? 'Телефон: '.trim($_POST['clc_phone']).'<br/>' : '';
		$text .= !empty($_POST['clc_email']) ? 'Email: '.trim($_POST['clc_email']).'<br/>' : '';
		$text .= !empty($_POST['clc_comment']) ? 'Комментарий: '.trim($_POST['clc_comment']).'<br/>' : '';
		$text .= '<br/><hr/>';
		$text .= '<table cellpadding="5" cellspacing="5" border="1">';
		$text .= $_POST['list-data'];
		$text .= '</table>';
		
		if($list_options['svarnoi']['contacts']['emails']){
			$admin_email = $list_options['svarnoi']['contacts']['emails'];
		}
		else{
			$admin_email = get_bloginfo('admin_email');
		}
		
		if(wp_mail($admin_email, 'Заказ сварного забора', $text, $headers)){
			$send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
							</div>';
		}
		else{
			$send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> При отправке заказа возникла ошибка.
							</div>';
		}
	}
    }
	?>
	<div id="clcFence">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<?php echo $send_mess; ?>
					<form id="clcFence-send-form" class="form-horizontal" method="post">
						<div class="col-md-8" style="padding-right: 30px; border-right: 1px solid #CCC;">
							<div class="form-group">
                                <label for="clc_length" class="col-sm-4 control-label">Общая длина м:</label>
                                <div class="col-sm-8">
                                    <input name="clc_length" type="text" class="form-control" id="clc_length" placeholder="Общая длина (число)" /><br/>
                                    <div id="slider_length"></div>
                                </div>
                            </div>
                            
							<div class="form-group">
                                <label for="clc_height" class="col-sm-4 control-label">Высота забора:</label>
                                <div class="col-sm-8">
                                    <select id="clc_height" name="clc_height" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['height']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['svarnoi']['height'][$i]['cost']; ?>" value="<?php echo $list_options['svarnoi']['height'][$i]['num']; ?>"><?php echo $list_options['svarnoi']['height'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Выбрать эскиз секции</button><br/><br/>
                                    <div class="collapse" id="collapseExample">
                                        <div class="panel panel-default wrap-sketch">
                                            <div class="panel-body">
                                                <?php for($i = 0, $max = count($list_options['svarnoi']['models']); $i < $max; $i++): ?>
                                                    <?php
                                                    $active = '';
                                                    $sel = 'Выбрать';
                                                    if($i == 0){
                                                        $active = 'active';
                                                        $sel = 'Выбрано';
                                                    }
    
                                                    $nal = '';
                                                    if($list_options['svarnoi']['models'][$i]['nal'])
                                                        $nal = '<span class="label label-success">Есть на складе</span>';
                                                    ?>
                                                
                                                    <div class="thumbnail <?php echo $active; ?>">
                                                        <?php echo $nal; ?>
                                                        <img src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['svarnoi']['models'][$i]['img']; ?>" alt="<?php echo $list_options['svarnoi']['models'][$i]['img']; ?>">
                                                        <div class="caption">
                                                            <p class="sketch-art"><?php echo $list_options['svarnoi']['models'][$i]['name']; ?></p>
                                                            <p class="sketch-price"><?php echo number_format($list_options['svarnoi']['models'][$i]['price'], 0, '.', ' '); ?> руб.</p>
                                                            <p class="sketch-unit">за м&sup2;</p>
                                                            <div class="clearfix"></div>
                                                            <a href="<?php echo $list_options['svarnoi']['models'][$i]['price']; ?>" class="btn btn-default btn-sm btn-block" role="button"><?php echo $sel; ?></a>
                                                        </div>
                                                    </div>
                                                    
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
                            
                            <div class="form-group">
                                <label for="clc_thick_prof" class="col-sm-4 control-label">Толщина профиля секции:</label>
                                <div class="col-sm-8">
                                    <select id="clc_thick_prof" name="clc_thick_prof" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['thick_prof']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> per="<?php echo $list_options['svarnoi']['thick_prof'][$i]['per']; ?>" value="<?php echo $list_options['svarnoi']['thick_prof'][$i]['name']; ?>"><?php echo $list_options['svarnoi']['thick_prof'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
                            
                            <div class="form-group">
                                <label for="clc_rasst_stolb" class="col-sm-4 control-label">Ширина секции:</label>
                                <div class="col-sm-8">
                                    <select id="clc_rasst_stolb" name="clc_rasst_stolb" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['rasst_stolb']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['svarnoi']['rasst_stolb'][$i]['num']; ?>"><?php echo $list_options['svarnoi']['rasst_stolb'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
                            
							<div class="form-group">
                                <label for="clc_coloration" class="col-sm-4 control-label">Окрашивание каркаса забора:</label>
                                <div class="col-sm-8">
                                    <select id="clc_coloration" name="clc_coloration" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['coloration']); $i < $max; $i++): ?>
                                        <?php if($i == 0) $coloration = $list_options['svarnoi']['coloration'][$i]['name']; ?>
                                        
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['svarnoi']['coloration'][$i]['cost']; ?>" value="<?php echo $list_options['svarnoi']['coloration'][$i]['name']; ?>"><?php echo $list_options['svarnoi']['coloration'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_color" class="col-sm-4 control-label">Цвет:</label>
                                <div class="col-sm-8">
                                    <select id="clc_color" name="clc_color" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['color']); $i < $max; $i++): ?>
                                        <option <?php if($coloration != $list_options['svarnoi']['color'][$i]['coloration']) echo 'disabled'; ?> coloration="<?php echo $list_options['svarnoi']['color'][$i]['coloration']; ?>" value="<?php echo $list_options['svarnoi']['color'][$i]['name']; ?>"><?php echo $list_options['svarnoi']['color'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_count_gate" class="col-sm-4 control-label">Кол-во ворот:</label>
                                <div class="col-sm-8">
                                    <select id="clc_count_gate" name="clc_count_gate" class="form-control">
                                        <option selected value="0">Нет</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group" style="display: none;">
                                <label for="clc_width_gate" class="col-sm-4 control-label">Ширина ворот (м):</label>
                                <div class="col-sm-8">
                                    <select id="clc_width_gate" name="clc_width_gate" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['gate']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['svarnoi']['gate'][$i]['num']; ?>"><?php echo $list_options['svarnoi']['gate'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group" style="display: none;">
                                <label for="clc_type_gate" class="col-sm-4 control-label">Тип ворот (м):</label>
                                <div class="col-sm-8">
                                    <select id="clc_type_gate" name="clc_type_gate" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['type_gate']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> install="<?php echo $list_options['svarnoi']['type_gate'][$i]['install']; ?>" note="<?php echo $list_options['svarnoi']['type_gate'][$i]['note']; ?>" per="<?php echo $list_options['svarnoi']['type_gate'][$i]['per']; ?>" cost="<?php echo $list_options['svarnoi']['type_gate'][$i]['price']; ?>" value="<?php echo $list_options['svarnoi']['type_gate'][$i]['name']; ?>"><?php echo $list_options['svarnoi']['type_gate'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_wicket" class="col-sm-4 control-label">Кол-во калиток:</label>
                                <div class="col-sm-8">
                                    <select id="clc_wicket" name="clc_wicket" class="form-control">
                                        <option selected value="0">Нет</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_type_stolb" class="col-sm-4 control-label">Столбы:</label>
                                <div class="col-sm-8">
                                    <select id="clc_type_stolb" name="clc_type_stolb" class="form-control">
                                        <option value="Металлические">Металлические</option>
                                        <option value="Кирпичные">Кирпичные</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_metall_stolbi_type" class="col-sm-4 control-label">Параметры столба:</label>
                                <div class="col-sm-8">
                                    <select id="clc_metall_stolbi_type" name="clc_metall_stolbi_type" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['metall_stolbi']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> width="<?php echo $list_options['svarnoi']['metall_stolbi'][$i]['width']; ?>" cost="<?php echo $list_options['svarnoi']['metall_stolbi'][$i]['price']; ?>" value="<?php echo $list_options['svarnoi']['metall_stolbi'][$i]['name']; ?>"><?php echo $list_options['svarnoi']['metall_stolbi'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group kirpich_stolbi_type_block" style="display: none;">
                                <label for="clc_kirpich_stolbi_type" class="col-sm-4 control-label">Параметры столба:</label>
                                <div class="col-sm-8">
                                    <select id="clc_kirpich_stolbi_type" name="clc_kirpich_stolbi_type" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['svarnoi']['kirpich_stolbi']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> width="<?php echo $list_options['svarnoi']['kirpich_stolbi'][$i]['width']; ?>" cost="<?php echo $list_options['svarnoi']['kirpich_stolbi'][$i]['price']; ?>" value="<?php echo $list_options['svarnoi']['kirpich_stolbi'][$i]['name']; ?>"><?php echo $list_options['svarnoi']['kirpich_stolbi'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">Выбрать заглушки</button><br/><br/>
                                    <div class="collapse" id="collapseExample2">
                                        <div class="panel panel-default wrap-zagl-met">
                                            <div class="panel-body">
                                                <?php for($i = 0, $max = count($list_options['svarnoi']['metall_stolbi_zagl']); $i < $max; $i++): ?>
                                                    <?php
                                                    $active = '';
                                                    $sel = 'style="display: none;"';
                                                    if($i == 0){
                                                        $active = 'active';
                                                        $sel = '';
                                                    }
                                                    ?>
                                                    <div class="thumbnail <?php echo $active; ?>" cost="<?php echo $list_options['svarnoi']['metall_stolbi_zagl'][$i]['price']; ?>" naim="<?php echo $list_options['svarnoi']['metall_stolbi_zagl'][$i]['name']; ?>">
                                                        <span <?php echo $sel; ?> class="badge"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>
                                                        <img src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['svarnoi']['metall_stolbi_zagl'][$i]['img']; ?>" alt="<?php echo $list_options['svarnoi']['metall_stolbi_zagl'][$i]['img']; ?>">
                                                    </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default wrap-zagl-kir" style="display: none;">
                                            <div class="panel-body">
                                                <?php for($i = 0, $max = count($list_options['svarnoi']['kirpich_stolbi_zagl']); $i < $max; $i++): ?>
                                                    <?php
                                                    $active = '';
                                                    $sel = 'style="display: none;"';
                                                    if($i == 0){
                                                        $active = 'active';
                                                        $sel = '';
                                                    }
                                                    ?>
                                                    <div class="thumbnail <?php echo $active; ?>" cost="<?php echo $list_options['svarnoi']['kirpich_stolbi_zagl'][$i]['price']; ?>" naim="<?php echo $list_options['svarnoi']['kirpich_stolbi_zagl'][$i]['name']; ?>">
                                                        <span <?php echo $sel; ?> class="badge"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>
                                                        <img src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['svarnoi']['kirpich_stolbi_zagl'][$i]['img']; ?>" alt="<?php echo $list_options['svarnoi']['kirpich_stolbi_zagl'][$i]['img']; ?>">
                                                    </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
                            
                            <div class="form-group">
                                <label for="clc_install_met_stolb" class="col-sm-4 control-label">Установка:</label>
                                <div class="col-sm-8">
                                    <select id="clc_install_met_stolb" name="clc_install_met_stolb" class="form-control">
                                        <option cost="0" value="Без установки">Без установки</option>
                                        <option cost="<?php echo $list_options['svarnoi']['install']['met'][0]; ?>" value="Бетонирование столбов">Бетонирование столбов</option>
                                        <option cost="<?php echo $list_options['svarnoi']['install']['met'][1]; ?>" value="Ленточный фундамент 25х40">Ленточный фундамент 25х40</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group" style="display: none;">
                                <label for="clc_install_kir_stolb" class="col-sm-4 control-label">Установка:</label>
                                <div class="col-sm-8">
                                    <select id="clc_install_kir_stolb" name="clc_install_kir_stolb" class="form-control">
                                        <option cost2="<?php echo $list_options['svarnoi']['install']['kir'][0]; ?>" cost="<?php echo $list_options['svarnoi']['install']['kir'][1]; ?>" value="Ленточный фундамент 40х60">Ленточный фундамент 40х60</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_mkad" class="col-sm-4 control-label">Расстояние от МКАД:</label>
                                <div class="col-sm-8">
                                    <input cost="<?php echo $list_options['svarnoi']['dostavka']; ?>" name="clc_mkad" type="text" class="form-control" id="clc_mkad" placeholder="Расстояние от МКАД" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_generator" class="col-sm-4 control-label">Вывоз генератора (если нет электричества):</label>
                                <div class="col-sm-8 checkbox">
                                    <label>
                                         <input name="clc_generator" type="checkbox" id="clc_generator" value="<?php echo $list_options['svarnoi']['generator']; ?>" />
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <input id="section_met_install" name="section_met_install" type="hidden" value="<?php echo $list_options['svarnoi']['install']['met'][2]; ?>" />
                        <input id="section_kir_install" name="section_kir_install" type="hidden" value="<?php echo $list_options['svarnoi']['install']['kir'][2]; ?>" />
                        
                        <input id="gate_install" name="gate_install" type="hidden" value="<?php echo $list_options['svarnoi']['gate_install']; ?>" />
                        <input id="wicket_install" name="wicket_install" type="hidden" value="<?php echo $list_options['svarnoi']['kalitka']['install']; ?>" />
                        <input id="clc_generator_length" name="clc_generator_length" type="hidden" value="<?php echo $list_options['svarnoi']['generator_length']; ?>" />
                        <input id="fence_job" name="fence_job" type="hidden" value="<?php echo $list_options['svarnoi']['fence_job']; ?>" />
                        <input id="fence_address" name="fence_address" type="hidden" value="<?php echo $list_options['svarnoi']['contacts']['address']; ?>" />
                        <input id="fence_phones" name="fence_phones" type="hidden" value="<?php echo $list_options['svarnoi']['contacts']['phones']; ?>" />
                        <input id="amortizacia" name="amortizacia" type="hidden" value="<?php echo $list_options['svarnoi']['amortizacia']; ?>" />
                        
                        <div class="col-md-4" style="padding-left: 30px;">
                            <div class="thumbnail"><img id="clc-img" src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['svarnoi']['models'][0]['img']; ?>" path="<?php echo PATCH_CLCFENCE; ?>img/uploads/" /></div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_mat" class="control-label">Итого за материалы:</label>
                                <input name="clc_itog_mat" type="text" class="form-control" id="clc_itog_mat" placeholder="Итого за материалы" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_job" class="control-label">Стоимость работ:</label>
                                <input name="clc_itog_job" type="text" class="form-control" id="clc_itog_job" placeholder="Стоимость работ" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_total" class="control-label">Итого за материалы и работу:</label>
                                <input name="clc_total" type="text" class="form-control validate[required,min[1]]" id="clc_total" placeholder="Итого за материалы и работу" readonly value="0" />
                            </div>
                            <br/>
                            <button name="update" type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#order_modal">Оставить заявку</button><br/>
                            <button id="print_order" name="print_order" type="button" class="btn btn-default btn-lg btn-block" value="1">Распечатать смету</button>
                        </div>
                        
                        <div class="col-md-12">
                            <br/>
                            <table id="table-list-data" class="table table-condensed table-hover table-bordered table-striped">
                                <tr>
                                    <th style="text-align:center;"><h4>Наименование</h4></th>
                                    <th style="text-align:center;"><h4>Количество</h4></th>
                                    <th style="text-align:center;"><h4>Ед. изм.</h4></th>
                                    <th style="text-align:center;"><h4>Цена</h4></th>
                                    <th style="text-align:center;"><h4>Сумма</h4></th>
                                </tr>
                            </table>
                        </div>
                        
                        <input id="list-data" name="list-data" type="hidden" value="" />
                        
                        <div id="order_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Отправить заказ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Ваше имя *</label>
                                            <input name="clc_name" type="text" class="form-control validate[required]" id="clc_name" placeholder="Ваше имя" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_phone" class="control-label">Ваш телефон *</label>
                                            <input name="clc_phone" type="text" class="form-control validate[required,custom[phone]]" id="clc_phone" placeholder="Ваш телефон" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_email" class="control-label">Ваш email *</label>
                                            <input name="clc_email" type="text" class="form-control validate[required,custom[email]]" id="clc_email" placeholder="Ваш email" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Комментарий</label>
                                            <textarea name="clc_comment" id="clc_comment" placeholder="Комментарий" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer" >
                                        <div class="g-recaptcha" data-callback="correctCaptcha"
                                             data-sitekey="<?php echo get_option('clcFence_options')['profnastil']['profnastil_captcha_key']; ?>"></div>
                                        <div>
                                            <input type="checkbox" class="validate[required,alertTextCheckboxe]"
                                                   id="agreement" name="agreement">
                                            <label for="agreement">Cогласен с <a href="/privacy">политикой конфиденцильности</a> и <a href="/terms">пользовательским соглашением</a>.</label>
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                        <button type="submit" class="btn btn-primary" value="1" name="send-order">Отправить</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </form>
				</div>
			</div>
		</div>
	</div>
    <?php
}

//Кованый забор
function clcFence_kovaniy_func_output(){
	$list_options = get_option('clcFence_options');
	@$send_order = $_POST['send-order'] ? $_POST['send-order'] : '';
	?>
	<!--<link href="<?php echo PATCH_CLCFENCE; ?>css/bootstrap.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo PATCH_CLCFENCE; ?>css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	
	<!--<script src="<?php echo PATCH_CLCFENCE; ?>js/bootstrap.js" type="text/javascript"></script>-->
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/jquery.validationEngine-ru.js" type="text/javascript"></script>
	<script src="<?php echo PATCH_CLCFENCE; ?>js/kovaniy.js" type="text/javascript"></script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	
	<style type="text/css">
	#clcFence{
		padding-top: 40px;
	}
	#clcFence label{
		text-align: left;
		font-size: 14px;
	}
	#clcFence table tr td:first-child {
		background: white;
	}
	#clcFence .btn {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fdb700 0%, #fb7501 100%) repeat scroll 0 0;
		border: none !important;
	}
	#clcFence .btn:hover,
	#clcFence .btn:focus {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fb7501 0%, #fdb700 100%) repeat scroll 0 0;
		color: #fff;
	}
    #clcFence .sketch-art{
        color: #23527c;
    }
    #clcFence .sketch-art{
        color: #23527c;
        font-size: 13px;
        margin: 0;
        text-align: center;
        font-weight: bold;
    }
    #clcFence .sketch-price{
        color: #fb7401;
        font-size: 12px;
        margin: 0;
        text-align: center;
        font-weight: bold;
    }
    #clcFence .sketch-unit{
        color: #636464;
        font-size: 11px;
        text-align: center;
    }
    #clcFence .wrap-sketch .thumbnail{
        margin-right: 5px;
        position: relative;
        float: left;
    }
    #clcFence .wrap-sketch .thumbnail img{
        height: 95px;
        width: 145px;
    }
    #clcFence .wrap-sketch .thumbnail.active{
        box-shadow: 0 0 3px #FB7901;
    }
    #clcFence .wrap-sketch .thumbnail .btn{
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #23527c 0%, #266faf 100%) repeat scroll 0 0;
        color: #FFFFFF;
    }
    #clcFence .wrap-sketch .thumbnail.active .btn{
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #fdb700 0%, #fb7501 100%) repeat scroll 0 0;
    }
    #clcFence .wrap-sketch .thumbnail .label-success{
        position: absolute;
        right: 1px;
        top: 1px;
    }
    #clcFence .wrap-zagl-met .thumbnail,
    #clcFence .wrap-zagl-kir .thumbnail{
        margin-right: 5px;
        position: relative;
        float: left;
        cursor: pointer;
    }  
    #clcFence .wrap-zagl-met .thumbnail .badge,
    #clcFence .wrap-zagl-kir .thumbnail .badge{
        background-color: #FB7901;
        position: absolute;
        right: 3px;
        top: 3px;
    }
    #clcFence .wrap-zagl-met .thumbnail img,
    #clcFence .wrap-zagl-kir .thumbnail img{
        max-height: 100px;
    }
    #clcFence .wrap-zagl-met .thumbnail.active,
    #clcFence .wrap-zagl-kir .thumbnail.active{
        box-shadow: 0 0 3px #FB7901;
    }
    #clcFence #clc_color option:disabled{
        color: #9A1013;
        text-decoration: line-through;
    }
	</style>
	<?php
	$send_mess = '';

	if($send_order) {
        $gRecaptchaResponse = checkRecaptchaResponse($_POST['g-recaptcha-response']);

        if (!$gRecaptchaResponse) {
            $send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Вы не прошли верификацию reCaptcha.
							</div>';
        } else {

		$send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
						</div>';

		add_filter('wp_mail_content_type','set_contenttype');
		function set_contenttype($content_type){
			return 'text/html';
		}
		
		remove_all_filters('wp_mail_from');
		remove_all_filters('wp_mail_from_name');
		
		//$headers = 'From: '.$_POST['clc_name'].' <'.$_POST['clc_email'].'>' . "\r\n";
        $headers = 'From: '.$_POST['clc_name'].' <support@zaborro.ru>' . "\r\n";
		
		$text = '';
		$text .= !empty($_POST['clc_name']) ? 'Имя: '.trim($_POST['clc_name']).'<br/>' : '';
		$text .= !empty($_POST['clc_phone']) ? 'Телефон: '.trim($_POST['clc_phone']).'<br/>' : '';
		$text .= !empty($_POST['clc_email']) ? 'Email: '.trim($_POST['clc_email']).'<br/>' : '';
		$text .= !empty($_POST['clc_comment']) ? 'Комментарий: '.trim($_POST['clc_comment']).'<br/>' : '';
		$text .= '<br/><hr/>';
		$text .= '<table cellpadding="5" cellspacing="5" border="1">';
		$text .= $_POST['list-data'];
		$text .= '</table>';
		
		if($list_options['kovaniy']['contacts']['emails']){
			$admin_email = $list_options['kovaniy']['contacts']['emails'];
		}
		else{
			$admin_email = get_bloginfo('admin_email');
		}
		
		if(wp_mail($admin_email, 'Заказ кованого забора', $text, $headers)){
			$send_mess = '<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Ваш заказ отправлен.
							</div>';
		}
		else{
			$send_mess = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> При отправке заказа возникла ошибка.
							</div>';
		}
	}
    }
	?>
	<div id="clcFence">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<?php echo $send_mess; ?>
					<form id="clcFence-send-form" class="form-horizontal" method="post">
						<div class="col-md-8" style="padding-right: 30px; border-right: 1px solid #CCC;">
							<div class="form-group">
                                <label for="clc_length" class="col-sm-4 control-label">Общая длина м:</label>
                                <div class="col-sm-8">
                                    <input name="clc_length" type="text" class="form-control" id="clc_length" placeholder="Общая длина (число)" /><br/>
                                    <div id="slider_length"></div>
                                </div>
                            </div>
                            
							<div class="form-group">
                                <label for="clc_height" class="col-sm-4 control-label">Высота забора:</label>
                                <div class="col-sm-8">
                                    <select id="clc_height" name="clc_height" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['height']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['kovaniy']['height'][$i]['cost']; ?>" value="<?php echo $list_options['kovaniy']['height'][$i]['num']; ?>"><?php echo $list_options['kovaniy']['height'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Выбрать эскиз секции</button><br/><br/>
                                    <div class="collapse" id="collapseExample">
                                        <div class="panel panel-default wrap-sketch">
                                            <div class="panel-body">

                                                <?php for($i = 0, $max = count($list_options['kovaniy']['models']); $i < $max; $i++): ?>
                                                    <?php
                                                    $active = '';
                                                    $sel = 'Выбрать';
                                                    if($i == 0){
                                                        $active = 'active';
                                                        $sel = 'Выбрано';
                                                    }
    
                                                    $nal = '';
                                                    if($list_options['kovaniy']['models'][$i]['nal'])
                                                        $nal = '<span class="label label-success">Есть на складе</span>';
                                                    ?>
                                                
                                                    <div class="thumbnail <?php echo $active; ?>">
                                                        <?php echo $nal; ?>
                                                        <img src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['kovaniy']['models'][$i]['img']; ?>" alt="<?php echo $list_options['kovaniy']['models'][$i]['img']; ?>">
                                                        <div class="caption">
                                                            <p class="sketch-art"><?php echo $list_options['kovaniy']['models'][$i]['name']; ?></p>
                                                            <p class="sketch-price"><?php echo number_format($list_options['kovaniy']['models'][$i]['price'], 0, '.', ' '); ?> руб.</p>
                                                            <p class="sketch-unit">за м&sup2;</p>
                                                            <div class="clearfix"></div>
                                                            <a href="<?php echo $list_options['kovaniy']['models'][$i]['price']; ?>" class="btn btn-default btn-sm btn-block" role="button"><?php echo $sel; ?></a>
                                                        </div>
                                                    </div>
                                                    
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
                            
                            <div class="form-group">
                                <label for="clc_thick_prof" class="col-sm-4 control-label">Толщина профиля секции:</label>
                                <div class="col-sm-8">
                                    <select id="clc_thick_prof" name="clc_thick_prof" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['thick_prof']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> per="<?php echo $list_options['kovaniy']['thick_prof'][$i]['per']; ?>" value="<?php echo $list_options['kovaniy']['thick_prof'][$i]['name']; ?>"><?php echo $list_options['kovaniy']['thick_prof'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
                            
                            <div class="form-group">
                                <label for="clc_rasst_stolb" class="col-sm-4 control-label">Ширина секции:</label>
                                <div class="col-sm-8">
                                    <select id="clc_rasst_stolb" name="clc_rasst_stolb" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['rasst_stolb']); $i < $max; $i++): ?>
                                    	<option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['kovaniy']['rasst_stolb'][$i]['num']; ?>"><?php echo $list_options['kovaniy']['rasst_stolb'][$i]['name']; ?></option>
                                    <?php endfor; ?>
									</select>
								</div>
							</div>
                            
							<div class="form-group">
                                <label for="clc_coloration" class="col-sm-4 control-label">Окрашивание каркаса забора:</label>
                                <div class="col-sm-8">
                                    <select id="clc_coloration" name="clc_coloration" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['coloration']); $i < $max; $i++): ?>
                                        <?php if($i == 0) $coloration = $list_options['kovaniy']['coloration'][$i]['name']; ?>
                                        
                                        <option <?php if($i == 0) echo 'selected'; ?> cost="<?php echo $list_options['kovaniy']['coloration'][$i]['cost']; ?>" value="<?php echo $list_options['kovaniy']['coloration'][$i]['name']; ?>"><?php echo $list_options['kovaniy']['coloration'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_color" class="col-sm-4 control-label">Цвет:</label>
                                <div class="col-sm-8">
                                    <select id="clc_color" name="clc_color" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['color']); $i < $max; $i++): ?>
                                        <option <?php if($coloration != $list_options['kovaniy']['color'][$i]['coloration']) echo 'disabled'; ?> coloration="<?php echo $list_options['kovaniy']['color'][$i]['coloration']; ?>" value="<?php echo $list_options['kovaniy']['color'][$i]['name']; ?>"><?php echo $list_options['kovaniy']['color'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_count_gate" class="col-sm-4 control-label">Кол-во ворот:</label>
                                <div class="col-sm-8">
                                    <select id="clc_count_gate" name="clc_count_gate" class="form-control">
                                        <option selected value="0">Нет</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group" style="display: none;">
                                <label for="clc_width_gate" class="col-sm-4 control-label">Ширина ворот (м):</label>
                                <div class="col-sm-8">
                                    <select id="clc_width_gate" name="clc_width_gate" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['gate']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> value="<?php echo $list_options['kovaniy']['gate'][$i]['num']; ?>"><?php echo $list_options['kovaniy']['gate'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group" style="display: none;">
                                <label for="clc_type_gate" class="col-sm-4 control-label">Тип ворот (м):</label>
                                <div class="col-sm-8">
                                    <select id="clc_type_gate" name="clc_type_gate" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['type_gate']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> install="<?php echo $list_options['kovaniy']['type_gate'][$i]['install']; ?>" note="<?php echo $list_options['kovaniy']['type_gate'][$i]['note']; ?>" per="<?php echo $list_options['kovaniy']['type_gate'][$i]['per']; ?>" cost="<?php echo $list_options['kovaniy']['type_gate'][$i]['price']; ?>" value="<?php echo $list_options['kovaniy']['type_gate'][$i]['name']; ?>"><?php echo $list_options['kovaniy']['type_gate'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_wicket" class="col-sm-4 control-label">Кол-во калиток:</label>
                                <div class="col-sm-8">
                                    <select id="clc_wicket" name="clc_wicket" class="form-control">
                                        <option selected value="0">Нет</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_type_stolb" class="col-sm-4 control-label">Столбы:</label>
                                <div class="col-sm-8">
                                    <select id="clc_type_stolb" name="clc_type_stolb" class="form-control">
                                        <option value="Металлические">Металлические</option>
                                        <option value="Кирпичные">Кирпичные</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_metall_stolbi_type" class="col-sm-4 control-label">Параметры столба:</label>
                                <div class="col-sm-8">
                                    <select id="clc_metall_stolbi_type" name="clc_metall_stolbi_type" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['metall_stolbi']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> width="<?php echo $list_options['kovaniy']['metall_stolbi'][$i]['width']; ?>" cost="<?php echo $list_options['kovaniy']['metall_stolbi'][$i]['price']; ?>" value="<?php echo $list_options['kovaniy']['metall_stolbi'][$i]['name']; ?>"><?php echo $list_options['kovaniy']['metall_stolbi'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group kirpich_stolbi_type_block" style="display: none;">
                                <label for="clc_kirpich_stolbi_type" class="col-sm-4 control-label">Параметры столба:</label>
                                <div class="col-sm-8">
                                    <select id="clc_kirpich_stolbi_type" name="clc_kirpich_stolbi_type" class="form-control">
                                    <?php for($i = 0, $max = count($list_options['kovaniy']['kirpich_stolbi']); $i < $max; $i++): ?>
                                        <option <?php if($i == 0) echo 'selected'; ?> width="<?php echo $list_options['kovaniy']['kirpich_stolbi'][$i]['width']; ?>" cost="<?php echo $list_options['kovaniy']['kirpich_stolbi'][$i]['price']; ?>" value="<?php echo $list_options['kovaniy']['kirpich_stolbi'][$i]['name']; ?>"><?php echo $list_options['kovaniy']['kirpich_stolbi'][$i]['name']; ?></option>
                                    <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">Выбрать заглушки</button><br/><br/>
                                    <div class="collapse" id="collapseExample2">
                                        <div class="panel panel-default wrap-zagl-met">
                                            <div class="panel-body">
                                                <?php for($i = 0, $max = count($list_options['kovaniy']['metall_stolbi_zagl']); $i < $max; $i++): ?>
                                                    <?php
                                                    $active = '';
                                                    $sel = 'style="display: none;"';
                                                    if($i == 0){
                                                        $active = 'active';
                                                        $sel = '';
                                                    }
                                                    ?>
                                                    <div class="thumbnail <?php echo $active; ?>" cost="<?php echo $list_options['kovaniy']['metall_stolbi_zagl'][$i]['price']; ?>" naim="<?php echo $list_options['kovaniy']['metall_stolbi_zagl'][$i]['name']; ?>">
                                                        <span <?php echo $sel; ?> class="badge"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>
                                                        <img src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['kovaniy']['metall_stolbi_zagl'][$i]['img']; ?>" alt="<?php echo $list_options['kovaniy']['metall_stolbi_zagl'][$i]['img']; ?>">
                                                    </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default wrap-zagl-kir" style="display: none;">
                                            <div class="panel-body">
                                                <?php for($i = 0, $max = count($list_options['kovaniy']['kirpich_stolbi_zagl']); $i < $max; $i++): ?>
                                                    <?php
                                                    $active = '';
                                                    $sel = 'style="display: none;"';
                                                    if($i == 0){
                                                        $active = 'active';
                                                        $sel = '';
                                                    }
                                                    ?>
                                                    <div class="thumbnail <?php echo $active; ?>" cost="<?php echo $list_options['kovaniy']['kirpich_stolbi_zagl'][$i]['price']; ?>" naim="<?php echo $list_options['kovaniy']['kirpich_stolbi_zagl'][$i]['name']; ?>">
                                                        <span <?php echo $sel; ?> class="badge"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>
                                                        <img src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['kovaniy']['kirpich_stolbi_zagl'][$i]['img']; ?>" alt="<?php echo $list_options['kovaniy']['kirpich_stolbi_zagl'][$i]['img']; ?>">
                                                    </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
                            
                            <div class="form-group">
                                <label for="clc_install_met_stolb" class="col-sm-4 control-label">Установка:</label>
                                <div class="col-sm-8">
                                    <select id="clc_install_met_stolb" name="clc_install_met_stolb" class="form-control">
                                        <option cost="0" value="Без установки">Без установки</option>
                                        <option cost="<?php echo $list_options['kovaniy']['install']['met'][0]; ?>" value="Бетонирование столбов">Бетонирование столбов</option>
                                        <option cost="<?php echo $list_options['kovaniy']['install']['met'][1]; ?>" value="Ленточный фундамент 25х40">Ленточный фундамент 25х40</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group" style="display: none;">
                                <label for="clc_install_kir_stolb" class="col-sm-4 control-label">Установка:</label>
                                <div class="col-sm-8">
                                    <select id="clc_install_kir_stolb" name="clc_install_kir_stolb" class="form-control">
                                        <option cost2="<?php echo $list_options['kovaniy']['install']['kir'][0]; ?>" cost="<?php echo $list_options['kovaniy']['install']['kir'][1]; ?>" value="Ленточный фундамент 40х60">Ленточный фундамент 40х60</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_mkad" class="col-sm-4 control-label">Расстояние от МКАД:</label>
                                <div class="col-sm-8">
                                    <input cost="<?php echo $list_options['kovaniy']['dostavka']; ?>" name="clc_mkad" type="text" class="form-control" id="clc_mkad" placeholder="Расстояние от МКАД" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="clc_generator" class="col-sm-4 control-label">Вывоз генератора (если нет электричества):</label>
                                <div class="col-sm-8 checkbox">
                                    <label>
                                         <input name="clc_generator" type="checkbox" id="clc_generator" value="<?php echo $list_options['kovaniy']['generator']; ?>" />
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <input id="section_met_install" name="section_met_install" type="hidden" value="<?php echo $list_options['kovaniy']['install']['met'][2]; ?>" />
                        <input id="section_kir_install" name="section_kir_install" type="hidden" value="<?php echo $list_options['kovaniy']['install']['kir'][2]; ?>" />
                        
                        <input id="gate_install" name="gate_install" type="hidden" value="<?php echo $list_options['kovaniy']['gate_install']; ?>" />
                        <input id="wicket_install" name="wicket_install" type="hidden" value="<?php echo $list_options['kovaniy']['kalitka']['install']; ?>" />
                        <input id="clc_generator_length" name="clc_generator_length" type="hidden" value="<?php echo $list_options['kovaniy']['generator_length']; ?>" />
                        <input id="fence_job" name="fence_job" type="hidden" value="<?php echo $list_options['kovaniy']['fence_job']; ?>" />
                        <input id="fence_address" name="fence_address" type="hidden" value="<?php echo $list_options['kovaniy']['contacts']['address']; ?>" />
                        <input id="fence_phones" name="fence_phones" type="hidden" value="<?php echo $list_options['kovaniy']['contacts']['phones']; ?>" />
                        <input id="amortizacia" name="amortizacia" type="hidden" value="<?php echo $list_options['kovaniy']['amortizacia']; ?>" />
                        
                        <div class="col-md-4" style="padding-left: 30px;">
                            <div class="thumbnail"><img id="clc-img" src="<?php echo PATCH_CLCFENCE; ?>img/uploads/<?php echo $list_options['kovaniy']['models'][0]['img']; ?>" path="<?php echo PATCH_CLCFENCE; ?>img/uploads/" /></div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_mat" class="control-label">Итого за материалы:</label>
                                <input name="clc_itog_mat" type="text" class="form-control" id="clc_itog_mat" placeholder="Итого за материалы" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_itog_job" class="control-label">Стоимость работ:</label>
                                <input name="clc_itog_job" type="text" class="form-control" id="clc_itog_job" placeholder="Стоимость работ" readonly value="0" />
                            </div>
                            <div class="form-group" style="margin: 0;">
                                <label for="clc_total" class="control-label">Итого за материалы и работу:</label>
                                <input name="clc_total" type="text" class="form-control validate[required,min[1]]" id="clc_total" placeholder="Итого за материалы и работу" readonly value="0" />
                            </div>
                            <br/>
                            <button name="update" type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#order_modal">Оставить заявку</button><br/>
                            <button id="print_order" name="print_order" type="button" class="btn btn-default btn-lg btn-block" value="1">Распечатать смету</button>
                        </div>

                        <div class="col-md-12">
                            <br/>
                            <table id="table-list-data" class="table table-condensed table-hover table-bordered table-striped">
                                <tr>
                                    <th style="text-align:center;"><h4>Наименование</h4></th>
                                    <th style="text-align:center;"><h4>Количество</h4></th>
                                    <th style="text-align:center;"><h4>Ед. изм.</h4></th>
                                    <th style="text-align:center;"><h4>Цена</h4></th>
                                    <th style="text-align:center;"><h4>Сумма</h4></th>
                                </tr>
                            </table>
                        </div>

                        <input id="list-data" name="list-data" type="hidden" value="" />

                        <div id="order_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Отправить заказ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Ваше имя *</label>
                                            <input name="clc_name" type="text" class="form-control validate[required]" id="clc_name" placeholder="Ваше имя" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_phone" class="control-label">Ваш телефон *</label>
                                            <input name="clc_phone" type="text" class="form-control validate[required,custom[phone]]" id="clc_phone" placeholder="Ваш телефон" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_email" class="control-label">Ваш email *</label>
                                            <input name="clc_email" type="text" class="form-control validate[required,custom[email]]" id="clc_email" placeholder="Ваш email" />
                                        </div>
                                        <div class="form-group" style="margin: 0;">
                                            <label for="clc_name" class="control-label">Комментарий</label>
                                            <textarea name="clc_comment" id="clc_comment" placeholder="Комментарий" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="">
                                        <div class="g-recaptcha" data-callback="correctCaptcha"
                                             data-sitekey="<?php echo get_option('clcFence_options')['profnastil']['profnastil_captcha_key']; ?>"></div>
                                        <div>
                                        <input type="checkbox" class="validate[required,alertTextCheckboxe]"
                                                   id="agreement" name="agreement">
                                            <label for="agreement">Cогласен с <a href="/privacy">политикой конфиденцильности</a> и <a href="/terms">пользовательским соглашением</a>.</label>
                                        </div>
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                        <button type="submit" class="btn btn-primary" value="1" name="send-order">Отправить</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </form>
				</div>
			</div>
		</div>
	</div>
    <?php
}
?>