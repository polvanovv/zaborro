<?php
global $clcFence_options;
//Заборы из профнастила
$clcFence_options['profnastil'] = array(
	//Контакты
	'contacts' => array('emails' => get_bloginfo('admin_email'), 'address' => '', 'phones' => ''),
	'captcha' => array('key' => '', 'secret_key' => ''),
	//Высота забора
	'height' => array(
		array('name' => '1,5м', 'num' => 1.5, 'cost' => 720),
		array('name' => '1,8м', 'num' => 1.8, 'cost' => 820),
		array('name' => '2м', 'num' => 2, 'cost' => 920)
	),
	//Изготовление забора из профнастила
	'fence_job' => 280,
	//Вид покрытия
	'cover' => array(
		array('name' => 'Одностороннее', 'cost' => 0, 'per' => 0),
		array('name' => 'Двустороннее', 'cost' => 60, 'per' => 0),
		array('name' => 'Принтек (дерево, камень, кирпич)', 'cost' => 40, 'per' => 1)
	),
	//Кол-во лаг (горизонталей)
	'lagi' => array(
		array('name' => 'в 2 ряда', 'cost' => 0),
		array('name' => 'в 3 ряда', 'cost' => 120)
	),
	//Ворота
	'gate' => array(
		array('name' => '3', 'cost' => 5400),
		array('name' => '3,5', 'cost' => 6200),
		array('name' => '4', 'cost' => 7000)
	),
	//Калитка
	'wicket' => 3000,
	//Окрашивание каркаса забора
	'coloration' => array(
		array('name' => 'ГФ021', 'cost' => 30),
		array('name' => 'Антикоррозийное покрытие 3 в 1', 'cost' => 150)
	),
	//П-образный профиль сверху
	'profile' => array(
		array('name' => 'Нет', 'cost' => 0),
		array('name' => 'Да', 'cost' => 120)
	),
	//МКАД
	'mkad' => 40,
	//Вывоз генератора
	'generator' => 900,
	'generator_length' => 50,
	//Установка забора
	'fence_install' => 240,
	//Установка ворот
	'gate_install' => 1800,
	//Установка калитки
	'wicket_install' => 1200
);

//Металлический штакетник
$clcFence_options['metall'] = array(
	//Контакты
	'contacts' => array('emails' => get_bloginfo('admin_email'), 'address' => '', 'phones' => ''),
	//Высота забора
	'height' => array(
		array('name' => '1м', 'num' => 1),
		array('name' => '1,5м', 'num' => 1.5),
		array('name' => '1,8м', 'num' => 1.8),
		array('name' => '2м', 'num' => 2)
	),
	//Евроштакетник / Высота забора / 1,0м 1,5м 1,8м 2,0м
	'evroshtaketnik' => array(
		array('name' => 'Юникс Премиум односторонний', 'img' => 'YUniks_Premium_odnostoronnii.jpg', 'price' => '70,105,126,140', 'color' => 'Вишня,Шоколад,Мох'),
		array('name' => 'Юникс Премиум двухсторонний', 'img' => 'YUniks_Premium_dvustoronnii.jpg', 'price' => '76,114,137,152', 'color' => 'Вишня,Шоколад,Мох,На заказ по RAL'),
		array('name' => 'Юникс Премиум структурный, матовый (2 цвета)', 'img' => 'YUniks_Premium_strukturnyi__matovyi_2_cveta.jpg', 'price' => '100,150,180,200', 'color' => 'Красный,Зеленый'),
		array('name' => 'Юникс Премиум порошок «Глянец»', 'img' => 'YUniks_Premium_poroshok_Glyanec.jpg', 'price' => '110,165,198,220', 'color' => 'Вишня,Шоколад,Мох,На заказ по RAL'),
		array('name' => 'Юникс Премиум порошок «Матовый»', 'img' => 'YUniks_Premium_poroshok_Matovyi.jpg', 'price' => '120,180,216,240', 'color' => 'Вишня,Шоколад,Мох,На заказ по RAL'),
		array('name' => 'Юникс Принтек (под дерево, золотой и античный дуб)', 'img' => 'YUniks_Printek_pod_derevo__zolotoi_i_antichnyi_dub.jpg', 'price' => '130,195,234,260', 'color' => 'Золотой,Античный дуб'),
		array('name' => 'Юникс Премиум Антик (медь, золото, серебро)', 'img' => 'YUniks_Premium_Antik_med__zoloto__serebro.jpg', 'price' => '190,285,342,380', 'color' => 'Медь,Золото,Серебро')
	),
	//Расстояние между столбами
	'rasst_stolb' => array(
		array('name' => '2,5м', 'num' => 2.5),
		array('name' => '3м', 'num' => 3)
	),
	//Зазор между штакетником
	'zazor_shtak' => array(
		//Для одной стороны 
		array(
			array('name' => '3см', 'num' => 3),
			array('name' => '4см', 'num' => 4),
			array('name' => '5см', 'num' => 5),
			array('name' => '6см', 'num' => 6),
			array('name' => '7см', 'num' => 7)
		),
		//В шахматном порядке
		array(
			array('name' => '8см', 'num' => 8),
			array('name' => '9см', 'num' => 9),
			array('name' => '10см', 'num' => 10)
		)
	),
	//Металлические столбы
	'metall_stolbi' => 450,
	//Лаги
	'lagi' => 100,
	//Покрытие каркаса забора
	'pokritie_zabora' => array(
		array('name' => 'Грунт', 'price' => 30),
		array('name' => 'Антикоррозийная краска', 'price' => 150)
	),
	//Саморезы
	'samorezi' => array(
		array('name' => 'Оцинкованные', 'price' => 2.5),
		array('name' => 'В цвет забора', 'price' => 3.9)
	),
	//Пластиковые заглушки
	'zaglushki' => 35,
	//Ворота
	'vorota' => array(
		array('name' => '3м', 'price' => 5400),
		array('name' => '3,5м', 'price' => 6200),
		array('name' => '4м', 'price' => 7000)
	),
	//Калитка 
	'kalitka' => 3000,
	//Установка забора
	'ustanovka_zabora' => 350,
	//Установка столбов
	'ustanovka_stolbov' => array(
		array('name' => 'Вбивание', 'price' => 0),
		array('name' => 'Забутовка', 'price' => 250),
		array('name' => 'Бетонирование', 'price' => 600)
	),
	//Монтаж ворот
	'montaz_vorot' => 6000,
	//Монтаж калитки
	'montaz_kalitki' => 2500,
	//Покраска каркаса забора
	'pokraska_karkasa_zabora' => array(
		array('name' => 'Грунт', 'price' => 30),
		array('name' => 'Краска', 'price' => 150)
	),
	//Амортизация оборудования и расходники 
	'amortizacia' => 900,
	//Вывоз генератора
	'generator' => 900,
	'generator_length' => 50,
	//Доставка
	'dostavka' => 40
);

//Заборы сварной
$clcFence_options['svarnoi'] = array(
	//Контакты
	'contacts' => array('emails' => get_bloginfo('admin_email'), 'address' => '', 'phones' => ''),
	//Высота забора
	'height' => array(
		array('name' => '1,5м', 'num' => 1.5),
		array('name' => '1,8м', 'num' => 1.8),
		array('name' => '2м', 'num' => 2)
	),
	//Типы
    'models' => array(
        array('name' => '', 'img' => '', 'price' => 0, 'nal' => 1, 'install' => 0)
    ),
    //Толщина профиля
    'thick_prof' => array(
        array('name' => '15х15мм.', 'per' => 0),
        array('name' => '20х20мм.', 'per' => 20),
    ),
    //Ширина секции
	'rasst_stolb' => array(
		array('name' => '2,5м', 'num' => 2.5),
		array('name' => '3м', 'num' => 3)
	),
    //Окрашивание каркаса забора
	'coloration' => array(
		array('name' => 'ГФ021', 'cost' => 30),
		array('name' => 'Антикоррозийное покрытие 3 в 1', 'cost' => 150)
	),
    //Цвета
	'color' => array(
		array('coloration' => 'ГФ021', 'name' => 'Серый'),
		array('coloration' => 'ГФ021', 'name' => 'Коричневый'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Черный'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Коричневый'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Зеленый'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Синий')
	),
    //Калитка 
	'kalitka' => array(
        'install' => 3000
    ),
    //Ворота
	'gate' => array(
		array('name' => '3', 'num' => '3'),
		array('name' => '3,5', 'num' => '3.5'),
		array('name' => '4', 'num' => '4')
	),
    //Тип ворот
	'type_gate' => array(
		array('name' => 'Распашные', 'per' => 50, 'price' => 0, 'install' => 8000, 'note' => ''),
		array('name' => 'Автоматические откатные', 'per' => 70, 'price' => 40000, 'install' => 20000, 'note' => '(бетонирование основания)')
	),
    //Металлические столбы
    'metall_stolbi' => array(
        array('name' => '80x80x2мм', 'price' => 900, 'width' => 0.08),
        array('name' => '80x80x3мм', 'price' => 1100, 'width' => 0.08),
        array('name' => '100x100x3мм', 'price' => 1300, 'width' => 0.10),
        array('name' => '100x100x4мм', 'price' => 1500, 'width' => 0.10)
    ),
    //Кирпичные столбы
    'kirpich_stolbi' => array(
        array('name' => '38х38', 'price' => 8900, 'width' => 0.38)
    ),
    //Заглушки металлических столбов
    'metall_stolbi_zagl' => array(
        array('name' => 'Пластиковая', 'img' => '', 'price' => 5),
        array('name' => 'Заглушка с шаром', 'img' => '', 'price' => 150)
    ),
    //Заглушки кирпичных столбов
    'kirpich_stolbi_zagl' => array(
        array('name' => 'Колпак пирамида', 'img' => '', 'price' => 100)
    ),
    //Установка
    'install' => array(
        'met' => array(1000, 2900, 800),
        'kir' => array(1000, 3600, 800)
    ),
    //Амортизация оборудования и расходники 
	'amortizacia' => 900,
	//Вывоз генератора
	'generator' => 900,
	'generator_length' => 50,
	//Доставка
	'dostavka' => 45
);

//Заборы кованые
$clcFence_options['kovaniy'] = array(
	//Контакты
	'contacts' => array('emails' => get_bloginfo('admin_email'), 'address' => '', 'phones' => ''),
	//Высота забора
	'height' => array(
		array('name' => '1,5м', 'num' => 1.5),
		array('name' => '1,8м', 'num' => 1.8),
		array('name' => '2м', 'num' => 2)
	),
	//Типы
    'models' => array(
        array('name' => '', 'img' => '', 'price' => 0, 'nal' => 1, 'install' => 0)
    ),
    //Толщина профиля
    'thick_prof' => array(
        array('name' => '15х15мм.', 'per' => 0),
        array('name' => '20х20мм.', 'per' => 20),
    ),
    //Ширина секции
	'rasst_stolb' => array(
		array('name' => '2,5м', 'num' => 2.5),
		array('name' => '3м', 'num' => 3)
	),
    //Окрашивание каркаса забора
	'coloration' => array(
		array('name' => 'ГФ021', 'cost' => 30),
		array('name' => 'Антикоррозийное покрытие 3 в 1', 'cost' => 150)
	),
    //Цвета
	'color' => array(
		array('coloration' => 'ГФ021', 'name' => 'Серый'),
		array('coloration' => 'ГФ021', 'name' => 'Коричневый'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Черный'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Коричневый'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Зеленый'),
        array('coloration' => 'Антикоррозийное покрытие 3 в 1', 'name' => 'Синий')
	),
    //Калитка 
	'kalitka' => array(
        'install' => 3000
    ),
    //Ворота
	'gate' => array(
		array('name' => '3', 'num' => '3'),
		array('name' => '3,5', 'num' => '3.5'),
		array('name' => '4', 'num' => '4')
	),
    //Тип ворот
	'type_gate' => array(
		array('name' => 'Распашные', 'per' => 50, 'price' => 0, 'install' => 8000, 'note' => ''),
		array('name' => 'Автоматические откатные', 'per' => 70, 'price' => 40000, 'install' => 20000, 'note' => '(бетонирование основания)')
	),
    //Металлические столбы
    'metall_stolbi' => array(
        array('name' => '80x80x2мм', 'price' => 900, 'width' => 0.08),
        array('name' => '80x80x3мм', 'price' => 1100, 'width' => 0.08),
        array('name' => '100x100x3мм', 'price' => 1300, 'width' => 0.10),
        array('name' => '100x100x4мм', 'price' => 1500, 'width' => 0.10)
    ),
    //Кирпичные столбы
    'kirpich_stolbi' => array(
        array('name' => '38х38', 'price' => 8900, 'width' => 0.38)
    ),
    //Заглушки металлических столбов
    'metall_stolbi_zagl' => array(
        array('name' => 'Пластиковая', 'img' => '', 'price' => 5),
        array('name' => 'Заглушка с шаром', 'img' => '', 'price' => 150)
    ),
    //Заглушки кирпичных столбов
    'kirpich_stolbi_zagl' => array(
        array('name' => 'Колпак пирамида', 'img' => '', 'price' => 100)
    ),
    //Установка
    'install' => array(
        'met' => array(1000, 2900, 800),
        'kir' => array(1000, 3600, 800)
    ),
    //Амортизация оборудования и расходники 
	'amortizacia' => 900,
	//Вывоз генератора
	'generator' => 900,
	'generator_length' => 50,
	//Доставка
	'dostavka' => 45
);