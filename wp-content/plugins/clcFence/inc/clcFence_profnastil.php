<?php
/*
* Заборы из профнастила
*/

function clcFence_form(){
    ?>
        <div id="clcFence">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<?php 
							if(!empty($_POST['update'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_form');
								
								$update_options = get_option('clcFence_options');
								
								$update_options['profnastil']['contacts']['emails'] = trim($_POST['profnastil_email']);
								$update_options['profnastil']['contacts']['address'] = trim($_POST['profnastil_address']);
								$update_options['profnastil']['contacts']['phones'] = trim($_POST['profnastil_phone']);
														
								$update_options['profnastil']['height'] = array();
								for($i = 0, $max = count($_POST['profnastil_height_name']); $i < $max; $i++){
									$update_options['profnastil']['height'][] = array('name' => trim($_POST['profnastil_height_name'][$i]), 'num' => trim($_POST['profnastil_height_num'][$i]), 'cost' => trim($_POST['profnastil_height_cost'][$i]));
								}
								
								$update_options['profnastil']['fence_job'] = trim($_POST['fence_job']);
								
								$update_options['profnastil']['cover'] = array();
								for($i = 0, $max = count($_POST['profnastil_cover_name']); $i < $max; $i++){
									$update_options['profnastil']['cover'][] = array('name' => trim($_POST['profnastil_cover_name'][$i]), 'cost' => trim($_POST['profnastil_cover_cost'][$i]), 'per' => @$_POST['profnastil_cover_per'][$i] ? $_POST['profnastil_cover_per'][$i] : 0);
								}
								
								$update_options['profnastil']['lagi'] = array();
								for($i = 0, $max = count($_POST['profnastil_lagi_name']); $i < $max; $i++){
									$update_options['profnastil']['lagi'][] = array('name' => trim($_POST['profnastil_lagi_name'][$i]), 'cost' => trim($_POST['profnastil_lagi_cost'][$i]));
								}
								
								$update_options['profnastil']['gate'] = array();
								for($i = 0, $max = count($_POST['profnastil_gate_name']); $i < $max; $i++){
									$update_options['profnastil']['gate'][] = array('name' => trim($_POST['profnastil_gate_name'][$i]), 'cost' => trim($_POST['profnastil_gate_cost'][$i]));
								}
								
								$update_options['profnastil']['wicket'] = trim($_POST['profnastil_wicket']);
								
								$update_options['profnastil']['coloration'] = array();
								for($i = 0, $max = count($_POST['profnastil_coloration_name']); $i < $max; $i++){
									$update_options['profnastil']['coloration'][] = array('name' => trim($_POST['profnastil_coloration_name'][$i]), 'cost' => trim($_POST['profnastil_coloration_cost'][$i]));
								}
								
								$update_options['profnastil']['profile'] = array();
								for($i = 0, $max = count($_POST['profnastil_profile_name']); $i < $max; $i++){
									$update_options['profnastil']['profile'][] = array('name' => trim($_POST['profnastil_profile_name'][$i]), 'cost' => trim($_POST['profnastil_profile_cost'][$i]));
								}
								
								$update_options['profnastil']['mkad'] = trim($_POST['profnastil_mkad']);
								
								$update_options['profnastil']['generator'] = trim($_POST['profnastil_generator']);
								
								$update_options['profnastil']['generator_length'] = trim($_POST['profnastil_generator_length']);
								
								$update_options['profnastil']['fence_install'] = trim($_POST['fence_install']);
								
								$update_options['profnastil']['gate_install'] = trim($_POST['gate_install']);
								
								$update_options['profnastil']['wicket_install'] = trim($_POST['wicket_install']);

								$update_options['profnastil']['profnastil_captcha_key'] = trim($_POST['profnastil_captcha_key']);

								$update_options['profnastil']['profnastil_secret_key'] = trim($_POST['profnastil_secret_key']);

								update_option('clcFence_options', $update_options);
								
								?>
								<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Данные успешно обновлены.
								</div>
								<?php
							}
							if(!empty($_POST['reset'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_form');
								
								global $clcFence_options;
								$update_options = get_option('clcFence_options');
								$update_options['profnastil'] = $clcFence_options['profnastil'];
                                update_option('clcFence_options', $update_options);
								?>
								<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Данные успешно восстановлены.
								</div>
								<?php
							}
						?>
						<form name="clcFence_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=clcFence.php" >
							<?php 
							wp_nonce_field('clcFence_form'); 
							$list_options = get_option('clcFence_options');
							?>
							<h2>Заборы из профнастила</h2><hr/>
							
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  	<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading1_0">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse1_0" aria-expanded="true" aria-controls="collapse1_0">
									  			Контактные данные
											</a>
								  		</h4>
									</div>
									<div id="collapse1_0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1_0">
								  		<div class="panel-body">
											<div class="form-group" style="padding: 0 0 35px 0;">
												<div class="col-md-4" style="padding: 0 5px 0 0;">
													<input type="text" class="form-control" name="profnastil_email" placeholder="Email через запятую" value="<?php echo $list_options['profnastil']['contacts']['emails']; ?>" />
                                                    <p class="help-block">Контактные данные (email)</p>
												</div>
												<div class="col-md-4" style="padding: 0 5px 0 0;">
													<input type="text" class="form-control" name="profnastil_address" placeholder="Адрес" value="<?php echo $list_options['profnastil']['contacts']['address']; ?>" />
                                                    <p class="help-block">Контактные данные (адрес)</p>
												</div>
												<div class="col-md-4" style="padding: 0 5px 0 5px;">
													<input type="text" class="form-control" name="profnastil_phone" placeholder="Телефоны через запятую" value="<?php echo $list_options['profnastil']['contacts']['phones']; ?>" />
                                                    <p class="help-block">Контактные данные (телефоны через запятую)</p>
												</div>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading1">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
									  			Высота забора
											</a>
								  		</h4>
									</div>
									<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['profnastil']['height']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-4" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="profnastil_height_name[]" placeholder="Наименование" value="<?php echo $list_options['profnastil']['height'][$i]['name']; ?>" />
                                                        <p class="help-block">Высота забора (наименование)</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="profnastil_height_num[]" placeholder="Высота" value="<?php echo $list_options['profnastil']['height'][$i]['num']; ?>" />
                                                        <p class="help-block">Высота забора (высота)</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="profnastil_height_cost[]" placeholder="Цена" value="<?php echo $list_options['profnastil']['height'][$i]['cost']; ?>" />
                                                        <p class="help-block">Высота забора (цена 1мп)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading1_1">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1_1" aria-expanded="false" aria-controls="collapse1_1">
									  			Изготовление забора из профнастила
											</a>
								  		</h4>
									</div>
									<div id="collapse1_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_1">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="fence_job" placeholder="Цена" value="<?php echo $list_options['profnastil']['fence_job']; ?>" />
                                                <p class="help-block">Изготовление забора из профнастила (цена 1мп.)</p>
											</div>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading2">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
									  			Вид покрытия
											</a>
								  		</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['profnastil']['cover']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="profnastil_cover_name[]" placeholder="Наименование" value="<?php echo $list_options['profnastil']['cover'][$i]['name']; ?>" />
                                                        <p class="help-block">Вид покрытия (наименование)</p>
													</div>
													<div class="col-md-4" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="profnastil_cover_cost[]" placeholder="Цена" value="<?php echo $list_options['profnastil']['cover'][$i]['cost']; ?>" />
                                                        <p class="help-block">Вид покрытия (цена 1мп или %)</p>
													</div>
													<div class="col-md-2" style="padding: 0 5px 0 5px;">
														<div class="checkbox">
															<label>
																<input name="profnastil_cover_per[<?php echo $i; ?>]" type="checkbox" value="1" <?php if($list_options['profnastil']['cover'][$i]['per']) echo 'checked'; ?> /> Расчет в процентах
															</label>
														</div>
													</div>
													<div class="col-md-1" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
							  	<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading3">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
									  			Лаги
											</a>
								  		</h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['profnastil']['lagi']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="profnastil_lagi_name[]" placeholder="Наименование" value="<?php echo $list_options['profnastil']['lagi'][$i]['name']; ?>" />
                                                        <p class="help-block">Лаги (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="profnastil_lagi_cost[]" placeholder="Цена" value="<?php echo $list_options['profnastil']['lagi'][$i]['cost']; ?>" />
                                                        <p class="help-block">Лаги (цена 1мп)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading4">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
									  			Ворота
											</a>
								  		</h4>
									</div>
									<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['profnastil']['gate']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="profnastil_gate_name[]" placeholder="Наименование" value="<?php echo $list_options['profnastil']['gate'][$i]['name']; ?>" />
                                                        <p class="help-block">Ворота (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="profnastil_gate_cost[]" placeholder="Цена" value="<?php echo $list_options['profnastil']['gate'][$i]['cost']; ?>" />
                                                        <p class="help-block">Ворота (цена 1шт)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading5">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
									  			Калитка
											</a>
								  		</h4>
									</div>
									<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="profnastil_wicket" placeholder="Цена" value="<?php echo $list_options['profnastil']['wicket']; ?>" />
                                                <p class="help-block">Калитка (цена 1шт)</p>
											</div>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading6">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
									  			Окрашивание каркаса забора
											</a>
								  		</h4>
									</div>
									<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['profnastil']['coloration']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="profnastil_coloration_name[]" placeholder="Наименование" value="<?php echo $list_options['profnastil']['coloration'][$i]['name']; ?>" />
                                                        <p class="help-block">Окрашивание каркаса забора (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="profnastil_coloration_cost[]" placeholder="Цена" value="<?php echo $list_options['profnastil']['coloration'][$i]['cost']; ?>" />
                                                        <p class="help-block">Окрашивание каркаса забора (цена 1м&sup2)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading7">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
									  			П-образный профиль сверху
											</a>
								  		</h4>
									</div>
									<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['profnastil']['profile']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="profnastil_profile_name[]" placeholder="Наименование" value="<?php echo $list_options['profnastil']['profile'][$i]['name']; ?>" />
                                                        <p class="help-block">П-образный профиль сверху (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="profnastil_profile_cost[]" placeholder="Цена" value="<?php echo $list_options['profnastil']['profile'][$i]['cost']; ?>" />
                                                        <p class="help-block">П-образный профиль сверху (цена 1мп)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading8">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
									  			Расстояние от МКАД
											</a>
								  		</h4>
									</div>
									<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="profnastil_mkad" placeholder="Цена" value="<?php echo $list_options['profnastil']['mkad']; ?>" />
                                                <p class="help-block">Расстояние от МКАД (цена 1км)</p>
											</div>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading9">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
									  			Вывоз генератора
											</a>
								  		</h4>
									</div>
									<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="profnastil_generator" placeholder="Цена" value="<?php echo $list_options['profnastil']['generator']; ?>" />
                                                <p class="help-block">Вывоз генератора (цена)</p>
											</div>
                                            <div class="form-group">
												<input type="text" class="form-control" name="profnastil_generator_length" placeholder="Длина" value="<?php echo $list_options['profnastil']['generator_length']; ?>" />
                                                <p class="help-block">Вывоз генератора (двойной тариф мп.)</p>
											</div>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading10">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
									  			Установка
											</a>
								  		</h4>
									</div>
									<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="fence_install" placeholder="Цена" value="<?php echo $list_options['profnastil']['fence_install']; ?>" />
                                                <p class="help-block">Установка забора (цена 1мп)</p>
											</div>
                                            <div class="form-group">
												<input type="text" class="form-control" name="gate_install" placeholder="Цена" value="<?php echo $list_options['profnastil']['gate_install']; ?>" />
                                                <p class="help-block">Установка ворот (цена 1шт.)</p>
											</div>
                                            <div class="form-group">
												<input type="text" class="form-control" name="wicket_install" placeholder="Цена" value="<?php echo $list_options['profnastil']['wicket_install']; ?>" />
                                                <p class="help-block">Установка калитки (цена 1шт.)</p>
											</div>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading11">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse"
                                               data-parent="#accordion" href="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                                Google captcha
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse11" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading11">
                                        <div class="panel-body">
                                            <div class="form-group" style="padding: 0 0 35px 0;">
                                                <div class="col-md-4" style="padding: 0 5px 0 0;">
                                                    <input type="text"
                                                           class="form-control"
                                                           name="profnastil_captcha_key" placeholder="Ключ google-captcha" value="<?php echo $list_options['profnastil']['profnastil_captcha_key']; ?>" />
                                                    <p class="help-block">Captcha Key
                                                    </p>
                                                </div>
                                                <div class="col-md-4" style="padding: 0 5px 0 0;">
                                                    <input type="text"
                                                           class="form-control"
                                                           name="profnastil_secret_key" placeholder="Секретный ключ google-captcha" value="<?php echo $list_options['profnastil']['profnastil_secret_key']; ?>" />
                                                    <p class="help-block">Secret Key
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                	<button name="reset" type="submit" class="btn btn-default btn-lg btn-block" value="1">Сброс настроек</button>
                                </div>
                                <div class="btn-group" role="group">
                                	<button name="update" type="submit" class="btn btn-primary btn-lg btn-block" value="1">Сохранить</button>
                                </div>
                            </div>
                            <br/>
							<pre>Вывод калькулятора на странице: [clcFence_profnastil]</pre>
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="page_options" value="" />
						</form>
					</div>
				</div>
			</div>
		</div>
    <? 
}
?>