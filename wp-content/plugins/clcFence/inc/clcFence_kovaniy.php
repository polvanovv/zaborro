<?php
/*
* Кованые заборы
*/

function clcFence_kovaniy_form(){
    ?>
        <div id="clcFence">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<?php 
							if(!empty($_POST['update'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_kovaniy_form');
                                
                                $update_options = get_option('clcFence_options');
                                
                                $update_options['kovaniy']['contacts']['emails'] = trim($_POST['kovaniy_email']);
								$update_options['kovaniy']['contacts']['address'] = trim($_POST['kovaniy_address']);
								$update_options['kovaniy']['contacts']['phones'] = trim($_POST['kovaniy_phone']);
								
								$update_options['kovaniy']['height'] = array();
								for($i = 0, $max = count($_POST['kovaniy_height_name']); $i < $max; $i++){
									$update_options['kovaniy']['height'][] = array('name' => trim($_POST['kovaniy_height_name'][$i]), 'num' => trim($_POST['kovaniy_height_num'][$i]));
								}
								
                                $update_options['kovaniy']['models'] = array();
								for($i = 0, $max = count($_POST['kovaniy_models_name']); $i < $max; $i++){
									$update_options['kovaniy']['models'][] = array('name' => trim($_POST['kovaniy_models_name'][$i]), 
                                                                                   'img' => trim($_POST['kovaniy_models_img'][$i]), 
                                                                                   'price' => trim($_POST['kovaniy_models_price'][$i]), 
                                                                                   'nal' => trim($_POST['kovaniy_models_nal'][$i]));
								}
                                
                                $update_options['kovaniy']['thick_prof'] = array();
								for($i = 0, $max = count($_POST['kovaniy_thick_prof_name']); $i < $max; $i++){
									$update_options['kovaniy']['thick_prof'][] = array('name' => trim($_POST['kovaniy_thick_prof_name'][$i]), 
                                                                                   'per' => trim($_POST['kovaniy_thick_prof_per'][$i]));
								}
                                
                                $update_options['kovaniy']['rasst_stolb'] = array();
								for($i = 0, $max = count($_POST['kovaniy_rasst_stolb_name']); $i < $max; $i++){
									$update_options['kovaniy']['rasst_stolb'][] = array('name' => trim($_POST['kovaniy_rasst_stolb_name'][$i]), 
                                                                                   'num' => trim($_POST['kovaniy_rasst_stolb_num'][$i]));
								}
                                
                                $update_options['kovaniy']['coloration'] = array();
								for($i = 0, $max = count($_POST['kovaniy_coloration_name']); $i < $max; $i++){
									$update_options['kovaniy']['coloration'][] = array('name' => trim($_POST['kovaniy_coloration_name'][$i]), 
                                                                                   'cost' => trim($_POST['kovaniy_coloration_cost'][$i]));
								}
                                
                                $update_options['kovaniy']['color'] = array();
								for($i = 0, $max = count($_POST['kovaniy_color_coloration']); $i < $max; $i++){
									$update_options['kovaniy']['color'][] = array('coloration' => trim($_POST['kovaniy_color_coloration'][$i]), 
                                                                                   'name' => trim($_POST['kovaniy_color_name'][$i]));
								}
                                
                                $update_options['kovaniy']['kalitka'] = array(
                                    'install' => (int)$_POST['kovaniy_kalitka_install']
                                );
                                
                                $update_options['kovaniy']['gate'] = array();
								for($i = 0, $max = count($_POST['kovaniy_gate_name']); $i < $max; $i++){
									$update_options['kovaniy']['gate'][] = array('name' => trim($_POST['kovaniy_gate_name'][$i]), 
                                                                                 'num' => trim($_POST['kovaniy_gate_num'][$i]));
								}
                                
                                $update_options['kovaniy']['type_gate'] = array();
								for($i = 0, $max = count($_POST['kovaniy_type_gate_name']); $i < $max; $i++){
									$update_options['kovaniy']['type_gate'][] = array('name' => trim($_POST['kovaniy_type_gate_name'][$i]), 
                                                                                   'per' => trim($_POST['kovaniy_type_gate_per'][$i]), 
                                                                                   'price' => trim($_POST['kovaniy_type_gate_price'][$i]),
                                                                                    'install' => trim($_POST['kovaniy_type_gate_install'][$i]),
                                                                                     'note' => trim($_POST['kovaniy_type_gate_note'][$i]));
								}
                                
                                $update_options['kovaniy']['metall_stolbi'] = array();
								for($i = 0, $max = count($_POST['kovaniy_metall_stolbi_name']); $i < $max; $i++){
									$update_options['kovaniy']['metall_stolbi'][] = array('name' => trim($_POST['kovaniy_metall_stolbi_name'][$i]), 
                                                                                        'price' => trim($_POST['kovaniy_metall_stolbi_price'][$i]),
                                                                                         'width' => trim($_POST['kovaniy_metall_stolbi_width'][$i]));
								}
                                
                                $update_options['kovaniy']['kirpich_stolbi'] = array();
								for($i = 0, $max = count($_POST['kovaniy_kirpich_stolbi_name']); $i < $max; $i++){
									$update_options['kovaniy']['kirpich_stolbi'][] = array('name' => trim($_POST['kovaniy_kirpich_stolbi_name'][$i]), 
                                                                                            'price' => trim($_POST['kovaniy_kirpich_stolbi_price'][$i]),
                                                                                         'width' => trim($_POST['kovaniy_kirpich_stolbi_width'][$i]));
								}
                                
                                $update_options['kovaniy']['metall_stolbi_zagl'] = array();
								for($i = 0, $max = count($_POST['kovaniy_metall_stolbi_zagl_name']); $i < $max; $i++){
									$update_options['kovaniy']['metall_stolbi_zagl'][] = array('name' => trim($_POST['kovaniy_metall_stolbi_zagl_name'][$i]), 
                                                                                   'img' => trim($_POST['kovaniy_metall_stolbi_zagl_img'][$i]), 
                                                                                   'price' => trim($_POST['kovaniy_metall_stolbi_zagl_price'][$i]));
								}
                                
                                $update_options['kovaniy']['kirpich_stolbi_zagl'] = array();
								for($i = 0, $max = count($_POST['kovaniy_kirpich_stolbi_zagl_name']); $i < $max; $i++){
									$update_options['kovaniy']['kirpich_stolbi_zagl'][] = array('name' => trim($_POST['kovaniy_kirpich_stolbi_zagl_name'][$i]), 
                                                                                   'img' => trim($_POST['kovaniy_kirpich_stolbi_zagl_img'][$i]), 
                                                                                   'price' => trim($_POST['kovaniy_kirpich_stolbi_zagl_price'][$i]));
								}
                                
                                $update_options['kovaniy']['install'] = array(
                                    'met' => array($_POST['kovaniy_install_met'][0], 
                                                   $_POST['kovaniy_install_met'][1], 
                                                  $_POST['kovaniy_install_met'][2]),
                                    'kir' => array($_POST['kovaniy_install_kir'][0], 
                                                   $_POST['kovaniy_install_kir'][1],
                                                  $_POST['kovaniy_install_kir'][2])
                                );
                                
                                $update_options['kovaniy']['amortizacia'] = trim($_POST['kovaniy_amortizacia']);
                                $update_options['kovaniy']['generator'] = trim($_POST['kovaniy_generator']);
                                $update_options['kovaniy']['generator_length'] = trim($_POST['kovaniy_generator_length']);
                                $update_options['kovaniy']['dostavka'] = trim($_POST['kovaniy_mkad']);
                                
                                update_option('clcFence_options', $update_options);
                                ?>
								<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Данные успешно обновлены.
								</div>
								<?php
							}
    
                            if(!empty($_POST['reset'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_kovaniy_form');
								
								global $clcFence_options;
								$update_options = get_option('clcFence_options');
								$update_options['kovaniy'] = $clcFence_options['kovaniy'];
                                update_option('clcFence_options', $update_options);
                                
                                
								?>
								<div class="alert alert-success alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Данные успешно восстановлены.
								</div>
								<?php
							}
						?>
						<form name="clcFence_kovaniy_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=clcFence_kovaniy.php" >
							<?php 
							wp_nonce_field('clcFence_kovaniy_form'); 
							$list_options = get_option('clcFence_options');
							?>
							<h2>Сварной забор</h2><hr/>
                            
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  	<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading1_0">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse1_0" aria-expanded="true" aria-controls="collapse1_0">
									  			Контактные данные
											</a>
								  		</h4>
									</div>
									<div id="collapse1_0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1_0">
								  		<div class="panel-body">
											<div class="form-group" style="padding: 0 0 35px 0;">
												<div class="col-md-4" style="padding: 0 5px 0 0;">
													<input type="text" class="form-control" name="kovaniy_email" placeholder="Email через запятую" value="<?php echo $list_options['kovaniy']['contacts']['emails']; ?>" />
                                                    <p class="help-block">Контактные данные (email)</p>
												</div>
												<div class="col-md-4" style="padding: 0 5px 0 0;">
													<input type="text" class="form-control" name="kovaniy_address" placeholder="Адрес" value="<?php echo $list_options['kovaniy']['contacts']['address']; ?>" />
                                                    <p class="help-block">Контактные данные (адрес)</p>
												</div>
												<div class="col-md-4" style="padding: 0 5px 0 5px;">
													<input type="text" class="form-control" name="kovaniy_phone" placeholder="Телефоны через запятую" value="<?php echo $list_options['kovaniy']['contacts']['phones']; ?>" />
                                                    <p class="help-block">Контактные данные (телефоны через запятую)</p>
												</div>
											</div>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading1">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
									  			Высота забора
											</a>
								  		</h4>
									</div>
									<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['height']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_height_name[]" placeholder="Наименование" value="<?php echo $list_options['kovaniy']['height'][$i]['name']; ?>" />
                                                        <p class="help-block">Высота забора (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_height_num[]" placeholder="Высота" value="<?php echo $list_options['kovaniy']['height'][$i]['num']; ?>" />
                                                        <p class="help-block">Высота забора (высота)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading2">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
									  			Секции
											</a>
								  		</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['models']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_models_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['models'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_models_img[]" placeholder="Изображение" value="<?php echo $list_options['kovaniy']['models'][$i]['img']; ?>" />
                                                        <p class="help-block">Изображение (загрузка на странице файлов)</p>
													</div>
                                                    <div class="col-md-2" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_models_price[]" placeholder="Цены" value="<?php echo $list_options['kovaniy']['models'][$i]['price']; ?>" />
                                                        <p class="help-block">Цены 15x15 и 20х20, через запятую</p>
													</div>
                                                    <div class="col-md-2" style="padding: 0 5px 0 0;">
														<select class="form-control" name="kovaniy_models_nal[]">
                                                            <option <?php if($list_options['kovaniy']['models'][$i]['nal']) echo 'selected'; ?> value="1">В наличии</option>
                                                            <option <?php if(!$list_options['kovaniy']['models'][$i]['nal']) echo 'selected'; ?> value="0">Нет в наличии</option>
                                                        </select>
                                                        <p class="help-block">Наличие</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading2_1">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse2_1" aria-expanded="true" aria-controls="collapse2_1">
									  			Толщина профиля
											</a>
								  		</h4>
									</div>
									<div id="collapse2_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2_1">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['thick_prof']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_thick_prof_name[]" placeholder="Наименование" value="<?php echo $list_options['kovaniy']['thick_prof'][$i]['name']; ?>" />
                                                        <p class="help-block">Толщина профиля (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_thick_prof_per[]" placeholder="Процент" value="<?php echo $list_options['kovaniy']['thick_prof'][$i]['per']; ?>" />
                                                        <p class="help-block">Толщина профиля (процент)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading3">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
									  			Ширина секции
											</a>
								  		</h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['rasst_stolb']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_rasst_stolb_name[]" placeholder="Наименование" value="<?php echo $list_options['kovaniy']['rasst_stolb'][$i]['name']; ?>" />
                                                        <p class="help-block">Ширина секции (наименование)</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_rasst_stolb_num[]" placeholder="Значение" value="<?php echo $list_options['kovaniy']['rasst_stolb'][$i]['num']; ?>" />
                                                        <p class="help-block">Ширина секции (значение)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading4">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
									  			Окрашивание каркаса забора
											</a>
								  		</h4>
									</div>
									<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['coloration']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_coloration_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['coloration'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_coloration_cost[]" placeholder="Цена" value="<?php echo $list_options['kovaniy']['coloration'][$i]['cost']; ?>" />
                                                        <p class="help-block">Цена м2</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading5">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
									  			Цвет
											</a>
								  		</h4>
									</div>
									<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['color']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
                                                        <select class="form-control" name="kovaniy_color_coloration[]">
                                                            <?php for($j = 0, $max2 = count($list_options['kovaniy']['coloration']); $j < $max2; $j++): ?>
                                                                <option <?php if($list_options['kovaniy']['color'][$i]['coloration'] == $list_options['kovaniy']['coloration'][$j]['name']) echo 'selected'; ?> value="<?php echo $list_options['kovaniy']['coloration'][$j]['name']; ?>"><?php echo $list_options['kovaniy']['coloration'][$j]['name']; ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                        <p class="help-block">Тип покрытия</p>
													</div>
                                                    <div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_color_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['color'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading5_1">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5_1" aria-expanded="false" aria-controls="collapse5_1">
									  			Калитка
											</a>
								  		</h4>
									</div>
									<div id="collapse5_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5_1">
								  		<div class="panel-body">
											<div class="form-group" style="padding: 0 0 35px 0;">
												<div class="col-md-12" style="padding: 0 5px 0 0;">
                                                    <input type="text" class="form-control" name="kovaniy_kalitka_install" placeholder="Установка" value="<?php echo $list_options['kovaniy']['kalitka']['install']; ?>" />
                                                    <p class="help-block">Калитка (установка)</p>
											    </div>
                                            </div>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading6">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
									  			Ворота
											</a>
								  		</h4>
									</div>
									<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['gate']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_gate_name[]" placeholder="Наименование" value="<?php echo $list_options['kovaniy']['gate'][$i]['name']; ?>" />
                                                        <p class="help-block">Ворота (наименование)</p>
													</div>
                                                    <div class="col-md-5" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_gate_num[]" placeholder="Ширина" value="<?php echo $list_options['kovaniy']['gate'][$i]['num']; ?>" />
                                                        <p class="help-block">Ворота (ширина)</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading7">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
									  			Тип ворот
											</a>
								  		</h4>
									</div>
									<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['type_gate']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-2" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_type_gate_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['type_gate'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
													<div class="col-md-2" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_type_gate_per[]" placeholder="Наценка" value="<?php echo $list_options['kovaniy']['type_gate'][$i]['per']; ?>" />
                                                        <p class="help-block">Наценка %</p>
													</div>
                                                    <div class="col-md-2" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_type_gate_price[]" placeholder="Автоматика" value="<?php echo $list_options['kovaniy']['type_gate'][$i]['price']; ?>" />
                                                        <p class="help-block">Цена автоматики</p>
													</div>
                                                    <div class="col-md-2" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_type_gate_install[]" placeholder="Установка" value="<?php echo $list_options['kovaniy']['type_gate'][$i]['install']; ?>" />
                                                        <p class="help-block">Цена установки</p>
													</div>
                                                    <div class="col-md-2" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_type_gate_note[]" placeholder="Текст" value="<?php echo $list_options['kovaniy']['type_gate'][$i]['note']; ?>" />
                                                        <p class="help-block">Текст установки</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
                                                    <div class="clearfix"></div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading8">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
									  			Металлические столбы
											</a>
								  		</h4>
									</div>
									<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['metall_stolbi']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-4" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_metall_stolbi_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['metall_stolbi'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_metall_stolbi_price[]" placeholder="Цена" value="<?php echo $list_options['kovaniy']['metall_stolbi'][$i]['price']; ?>" />
                                                        <p class="help-block">Цена шт.</p>
													</div>
                                                    <div class="col-md-3" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_metall_stolbi_width[]" placeholder="Ширина" value="<?php echo $list_options['kovaniy']['metall_stolbi'][$i]['width']; ?>" />
                                                        <p class="help-block">Ширина м.</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
                                </div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading9">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
									  			Кирпичные столбы
											</a>
								  		</h4>
									</div>
									<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['kirpich_stolbi']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-4" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_kirpich_stolbi_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['kirpich_stolbi'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_kirpich_stolbi_price[]" placeholder="Цена" value="<?php echo $list_options['kovaniy']['kirpich_stolbi'][$i]['price']; ?>" />
                                                        <p class="help-block">Цена шт.</p>
													</div>
                                                    <div class="col-md-3" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_kirpich_stolbi_width[]" placeholder="Ширина" value="<?php echo $list_options['kovaniy']['kirpich_stolbi'][$i]['width']; ?>" />
                                                        <p class="help-block">Ширина м.</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading10">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
									  			Заглушки металлических столбов
											</a>
								  		</h4>
									</div>
									<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['metall_stolbi_zagl']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-4" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_metall_stolbi_zagl_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['metall_stolbi_zagl'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
                                                    <div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_metall_stolbi_zagl_img[]" placeholder="Изображение" value="<?php echo $list_options['kovaniy']['metall_stolbi_zagl'][$i]['img']; ?>" />
                                                        <p class="help-block">Изображение (загрузка на странице файлов)</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_metall_stolbi_zagl_price[]" placeholder="Цена" value="<?php echo $list_options['kovaniy']['metall_stolbi_zagl'][$i]['price']; ?>" />
                                                        <p class="help-block">Цена шт.</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading11">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
									  			Заглушки кирпичных столбов
											</a>
								  		</h4>
									</div>
									<div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
								  		<div class="panel-body">
											<?php for($i = 0, $max = count($list_options['kovaniy']['kirpich_stolbi_zagl']); $i < $max; $i++): ?>
												<div class="form-group" style="padding: 0 0 35px 0;">
													<div class="col-md-4" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_kirpich_stolbi_zagl_name[]" placeholder="Название" value="<?php echo $list_options['kovaniy']['kirpich_stolbi_zagl'][$i]['name']; ?>" />
                                                        <p class="help-block">Название</p>
													</div>
                                                    <div class="col-md-3" style="padding: 0 5px 0 0;">
														<input type="text" class="form-control" name="kovaniy_kirpich_stolbi_zagl_img[]" placeholder="Изображение" value="<?php echo $list_options['kovaniy']['kirpich_stolbi_zagl'][$i]['img']; ?>" />
                                                        <p class="help-block">Изображение (загрузка на странице файлов)</p>
													</div>
													<div class="col-md-3" style="padding: 0 5px 0 5px;">
														<input type="text" class="form-control" name="kovaniy_kirpich_stolbi_zagl_price[]" placeholder="Цена" value="<?php echo $list_options['kovaniy']['kirpich_stolbi_zagl'][$i]['price']; ?>" />
                                                        <p class="help-block">Цена шт.</p>
													</div>
													<div class="col-md-2" style="padding: 0 0 0 5px;">
														<button type="button" class="btn btn-danger dell-row"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
													</div>
												</div>
											<?php endfor; ?>
											<button type="button" class="btn btn-primary btn-block add-row">Добавить поле</button>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading12">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
									  			Установка
											</a>
								  		</h4>
									</div>
									<div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
								  		<div class="panel-body">
											<div class="form-group" style="padding: 0 0 35px 0;">
                                                <div class="col-md-4" style="padding: 0 5px 0 0;">
                                                    <input type="text" class="form-control" name="kovaniy_install_met[0]" placeholder="Бетонирование столбов" value="<?php echo $list_options['kovaniy']['install']['met'][0]; ?>" />
                                                    <p class="help-block">Бетонирование металлических столбов</p>
                                                </div>
                                                <div class="col-md-4" style="padding: 0 5px 0 0;">
                                                    <input type="text" class="form-control" name="kovaniy_install_met[1]" placeholder="Ленточный фундамент" value="<?php echo $list_options['kovaniy']['install']['met'][1]; ?>" />
                                                    <p class="help-block">Ленточный фундамент металлических столбов</p>
                                                </div>
                                                <div class="col-md-4" style="padding: 0 5px 0 0;">
                                                    <input type="text" class="form-control" name="kovaniy_install_met[2]" placeholder="Монтаж секций" value="<?php echo $list_options['kovaniy']['install']['met'][2]; ?>" />
                                                    <p class="help-block">Монтаж секций металлических столбов</p>
                                                </div>
                                                <div class="col-md-4" style="padding: 0 5px 0 0;">
                                                    <input type="text" class="form-control" name="kovaniy_install_kir[0]" placeholder="Бетонирование столбов" value="<?php echo $list_options['kovaniy']['install']['kir'][0]; ?>" />
                                                    <p class="help-block">Бетонирование кирпичных столбов</p>
                                                </div>
                                                <div class="col-md-4" style="padding: 0 5px 0 5px;">
                                                    <input type="text" class="form-control" name="kovaniy_install_kir[1]" placeholder="Ленточный фундамент" value="<?php echo $list_options['kovaniy']['install']['kir'][1]; ?>" />
                                                    <p class="help-block">Ленточный фундамент кирпичных столбов</p>
                                                </div>
                                                <div class="col-md-4" style="padding: 0 5px 0 5px;">
                                                    <input type="text" class="form-control" name="kovaniy_install_kir[2]" placeholder="Монтаж секций" value="<?php echo $list_options['kovaniy']['install']['kir'][2]; ?>" />
                                                    <p class="help-block">Монтаж секций кирпичных столбов</p>
                                                </div>
                                            </div>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading13">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
									  			Расстояние от МКАД
											</a>
								  		</h4>
									</div>
									<div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="kovaniy_mkad" placeholder="Цена" value="<?php echo $list_options['kovaniy']['dostavka']; ?>" />
                                                <p class="help-block">Расстояние от МКАД (цена 1км)</p>
											</div>
								  		</div>
									</div>
							  	</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading14">
								  		<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
									  			Вывоз генератора
											</a>
								  		</h4>
									</div>
									<div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="kovaniy_generator" placeholder="Цена" value="<?php echo $list_options['kovaniy']['generator']; ?>" />
                                                <p class="help-block">Вывоз генератора (цена)</p>
											</div>
                                            <div class="form-group">
												<input type="text" class="form-control" name="kovaniy_generator_length" placeholder="Длина" value="<?php echo $list_options['kovaniy']['generator_length']; ?>" />
                                                <p class="help-block">Вывоз генератора (двойной тариф мп.)</p>
											</div>
								  		</div>
									</div>
							  	</div>
                                <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading15">
								  		<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse15" aria-expanded="true" aria-controls="collapse15">
									  			Амортизация
											</a>
								  		</h4>
									</div>
									<div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
								  		<div class="panel-body">
											<div class="form-group">
												<input type="text" class="form-control" name="kovaniy_amortizacia" placeholder="Цена" value="<?php echo $list_options['kovaniy']['amortizacia']; ?>" />
                                                <p class="help-block">Амортизация оборудования и расходники (цена)</p>
											</div>
								  		</div>
									</div>
                                </div>
							</div>
							<div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                	<button name="reset" type="submit" class="btn btn-default btn-lg btn-block" value="1">Сброс настроек</button>
                                </div>
                                <div class="btn-group" role="group">
                                	<button name="update" type="submit" class="btn btn-primary btn-lg btn-block" value="1">Сохранить</button>
                                </div>
                            </div>
                            <br/>
							<pre>Вывод калькулятора на странице: [clcFence_kovaniy]</pre>
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="page_options" value="" />
						</form>
					</div>
				</div>
			</div>
		</div>
    <? 
}
?>