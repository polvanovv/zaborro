<?php
/*
* Поликарбонат
*/

function clcFence_policarbonat_form(){
    ?>
        <div id="clcFence">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<?php
							if(!empty($_POST['update'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_policarbonat_form');

							}
						?>
						<form name="clcFence_policarbonat_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=clcFence_policarbonat.php" >
							<?php
							wp_nonce_field('clcFence_policarbonat_form');
							$list_options = get_option('clcFence_options');
							?>
							<h2>Забор из поликарбоната</h2><hr/>
						</form>
					</div>
				</div>
			</div>
		</div>
    <?
}
?>