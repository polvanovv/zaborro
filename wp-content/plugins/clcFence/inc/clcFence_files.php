<?php
/*
* Загруженные файлы
*/

function clcFence_files_form(){
    ?>
        <div id="clcFence">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<?php 
							if(!empty($_POST['update'])){
								if(!current_user_can('level_8')) die();
								check_admin_referer('clcFence_files_form');
								
								if($_FILES['upload_file']['size'] > ((int)ini_get("upload_max_filesize") * (1024 * 1024))): ?>
                                	<div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Размер файла превышает <?php echo (int)ini_get("upload_max_filesize"); ?> mb.
                                    </div>
                                <?php elseif($_FILES['upload_file']['type'] != 'image/jpeg' 
								&& $_FILES['upload_file']['type'] != 'image/jpg' 
								&& $_FILES['upload_file']['type'] != 'image/png' 
								&& $_FILES['upload_file']['type'] != 'image/gif'): ?>
									<div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Тип файла только jpeg, jpg, png, gif.
                                    </div>
                                <?php elseif($_FILES['upload_file']['error']): ?>
									<div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Ошибка при загрузке файла.
                                    </div>
								<?php else: ?>
									<?php copy($_FILES['upload_file']['tmp_name'], '../wp-content/plugins/clcFence/img/uploads/'.transliterate($_FILES['upload_file']['name'])); ?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></strong> Файл успешно загружен.
                                    </div>
								<?php endif;
							}
							
							if($_GET['dell-file']):
							unlink('../wp-content/plugins/clcFence/img/uploads/'.$_GET['dell-file']); ?>
							<script type="text/javascript">
								document.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>?page=clcFence_files.php'
							</script>
                            <?php endif;
						?>
						<form name="clcFence_files_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=clcFence_files.php" enctype="multipart/form-data" >
							<?php 
							wp_nonce_field('clcFence_files_form'); 
							$list_options = get_option('clcFence_options');
							?>
							<h2>Загруженные файлы</h2><hr/>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">Список загруженных файлов</div>
                                <?php $files = scandir('../wp-content/plugins/clcFence/img/uploads/'); ?>
                                <?php if(count($files) > 2): ?>
                                	<ul class="list-group">
                                    <?php for($i = 0; $i < count($files); $i++): ?>
										<?php if($files[$i] != '.' && $files[$i] != '..'): ?>
                                            <li class="list-group-item">
												<div class="thumbnail">
                                                    <img style="max-height: 200px;" src="../wp-content/plugins/clcFence/img/uploads/<?php echo iconv('windows-1251', 'utf-8', $files[$i]); ?>" alt="<?php echo $files[$i]; ?>">
                                                    <div class="caption">
                                                    	<div class="form-group">
                                                        	<input readonly type="text" class="form-control name-upload-file" placeholder="<?php echo $files[$i]; ?>" value="<?php echo $files[$i]; ?>">
                                                        </div>
                                                        
                                                    	<p><a href="<?php echo $_SERVER['PHP_SELF']; ?>?page=clcFence_files.php&dell-file=<?php echo $files[$i]; ?>" class="btn btn-danger btn-block btn-sm dell-file" role="button">Удалить</a></p>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                    </ul>
                                <?php else: ?>
                                	<div class="panel-body">
                                        <div class="alert alert-warning alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Нет загруженных файлов.
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                            	<label for="upload_file">Загрузить файл</label>
                            	<input required name="upload_file" type="file" id="upload_file">
                            	<p class="help-block">Максимальный размер файла не более <?php echo (int)ini_get("upload_max_filesize"); ?> mb, тип файла jpeg, jpg, png, gif</p>
                            </div>
                            <button name="update" type="submit" class="btn btn-primary btn-lg btn-block" value="1">Сохранить</button>
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="page_options" value="" />
						</form>
					</div>
				</div>
			</div>
		</div>
    <? 
}
?>