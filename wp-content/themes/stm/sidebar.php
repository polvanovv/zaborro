<div class="col-md-3">

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="primary-sidebar" class="primary-sidebar widget-area sidebar" role="complementary">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div><!-- #primary-sidebar -->
	<?php endif; ?>

	<?php
		/* Get recent news*/
		$news = stm_query_news(array('posts_per_page'=>5));
		if($news->have_posts()):
	?>
		<h2><?php _e('News', STM_DOMAIN)?></h2>
		<ul class="media-list">
			<?php while($news->have_posts()): $news->the_post();?>
			<li class="media">
				<?php if(has_post_thumbnail()):?>
				<a class="pull-left" href="<?php the_permalink();?>">
					<?php the_post_thumbnail('thumbnail', array('class'=>'media-object'));?>
				</a>
				<?php endif;?>
				<div class="media-body">
					<h4 class="media-heading"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
					<?php the_excerpt();?>
				</div>
			</li>
			<?php endwhile;?>
		</ul>
		<a href="<?php echo get_post_type_archive_link(ST_News::POST_TYPE)?>"><?php _e('All news', STM_DOMAIN)?></a>

	<?php endif;?>


	<?php
		/* Get partners */
		$partners = stm_query_partners(array('posts_per_page'=>-1));
		if($partners->have_posts()):
	?>
		<h2><?php _e('Partners', STM_DOMAIN)?></h2>
		<ul class="partner-list">
			<?php while($partners->have_posts()): $partners->the_post();?>
			<li class="media">
				<?php if(has_post_thumbnail()):?>
				<a class="pull-left" href="<?php the_permalink();?>">
					<?php the_post_thumbnail('thumbnail', array('class'=>'media-object'));?>
				</a>
				<?php endif;?>
			</li>
			<?php endwhile;?>
		</ul>

	<?php endif;?>
</div>