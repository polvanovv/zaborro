<?php get_header() ?>
    <div class="container">
    <div class="row"><a href="javascript:void(0);" class="sidebar-show btn">Каталог продукции</a>
        <div class="col-xs-3">                <?php get_sidebar('left'); ?>            </div>
        <div class="col-md-9 col-xs-12 col-sm-12">
            <ul class="breadcrumbs list-unstyled clearfix">                    <?php if (function_exists('bcn_display_list')) {
                    bcn_display_list();
                } ?>                </ul> <?php if (have_posts()) : ?><?php while (have_posts()) : the_post(); ?>
                <h1>                            <?php the_title(); ?>                        </h1>
                <div class="single-articles-content">                            <?php the_content() ?>                        </div>                    <?php endwhile; ?><?php endif; ?>
        </div>
    </div>        <?php get_sidebar('articles'); ?>    </div><?php get_footer() ?>