<?php if(!empty($_GET['isAjax'])):	if (have_posts()) : ?>
    <ul class="list-unstyled archive-list testimonials-list testimonials-archivelist">            <?php while (have_posts()) : the_post(); ?>
            <li>
                <div class="media clearfix">
                    <div class="media-left">
                        <div class="testimonial-thumb">                                <?php if (has_post_thumbnail()) { ?><?php the_post_thumbnail('custom-additional') ?><?php } ?>                            </div>
                    </div>
                    <div class="media-body">
                        <div class="media-heading"><?php the_title(); ?></div>
                        <div class="media-content">                                <?php the_content(); ?>                            </div>
                        <div class="text-right">                                <?php echo get_post_meta($post->ID, 'tname', true); ?>,
                            <span class="tcity"><?php echo get_post_meta($post->ID, 'tcity', true); ?></span>
                        </div>
                    </div>
                </div>
            </li>            <?php endwhile; ?>
    </ul>        <?php more_pagination($query = stm_query_testimonials()->found_posts); endif;else:	?><?php get_header() ?>
<div class="container">
    <div class="row"><a href="javascript:void(0);" class="sidebar-show btn">Каталог продукции</a>
        <div class="col-xs-3">                <?php get_sidebar('left'); ?>            </div>
        <div class="col-md-9 col-xs-12 col-sm-12">
            <ul class="breadcrumbs list-unstyled clearfix">                    <?php if (function_exists('bcn_display_list')) {
                bcn_display_list();
                } ?>                </ul>
            <h1 class="title-inner-marg">Отзывы</h1> <?php if (have_posts()) : ?>
            <ul class="list-unstyled archive-list testimonials-list testimonials-archivelist">                        <?php while (have_posts()) : the_post(); ?>
                <li>
                    <div class="media clearfix">
                        <div class="media-left">
                            <div class="testimonial-thumb">                                                <?php if (has_post_thumbnail()) { ?>													<?php the_post_thumbnail('custom-additional') ?>												<?php } ?>                                            </div>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><?php the_title(); ?></div>
                            <div class="media-content">                                            <?php the_content(); ?>                                        </div>
                            <div class="text-right">                                            <?php echo get_post_meta($post->ID, 'tname', true); ?>,
                                <span class="tcity"><?php echo get_post_meta($post->ID, 'tcity', true); ?></span>
                            </div>
                        </div>
                    </div>
                </li> <?php endwhile; ?>
            </ul> <?php endif; ?>				<?php more_pagination($query = stm_query_testimonials()->found_posts); ?>
            <div class="testimonials-form-block">
                <div class="h1">Оставить отзыв</div>
                <form role="form" action='' method='POST' id='testimonial_form' enctype="multipart/form-data">
                    <div class="testimonials-form-wrap">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="form-group"><label>Ваше имя</label>
                                    <input class="form-control" name="testimonial-name" type="text"/>
                                </div>
                                <div class="form-group"><label>Электронная почта</label>
                                    <input class="form-control" name="testimonial-email" type="text"/>
                                </div>
                                <div class="form-group"><label>Название проекта</label>
                                    <input class="form-control" name="testimonial-project" type="text"/>
                                </div>
                                <div class="form-group"><label>Город</label>
                                    <input class="form-control" name="testimonial-city" type="text"/>
                                </div>
                                <div class="form-group"><label>Фотография объекта</label>
                                    <div class="form-control filetype-wrapper">
                                        <span class="file-value"></span>
                                        <a class="file-btn" href="#">Обзор</a></div>
                                    <div class="allowed-text">Допустимые типы файлов: png, jpg.</div>
                                    <input class="form-control type-file" name="testimonial-file" type="file"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10">
                                <div class="form-group"><label>Сообщение</label>
                                    <textarea class="form-control form-text" name="testimonial-text"></textarea>
                                </div>
                                <div class="form-group">
                                    <input value="Отправить" type="submit" class="submit-btn btn btn-lg"/>
                                    <input type="hidden" name="testimonials_action" value="1"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <script>        function loadMorePosts(url) {
        jQuery('.pagination:first').remove();
        var s = location.search.substring(1);
        if (s === '') {
          jQuery('#paginatedContent').
              html('<div class="loads">Loading...</div>').
              load(url + '?isAjax=1',
                  function() { jQuery('#paginatedContent:first').removeAttr('id'); });
        }
        else {
          jQuery('#paginatedContent').
              html('<div class="loads">Loading...</div>').
              load(url + '&isAjax=1',
                  function() { jQuery('#paginatedContent:first').removeAttr('id'); });
        }
      }    </script>    <?php get_footer();
endif; ?>