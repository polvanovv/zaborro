<?php get_header();?>

	<div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="section_title">
                    <h2><?php echo single_cat_title();?></h2>
                </div>
                <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post();
                        get_template_part( 'partials/loop', get_post_format() );
                    endwhile;
                    get_template_part( 'partials/paginate' );
                else :
                    get_template_part( 'content', 'none' );
                endif;
                ?>
            </div>
            <?php get_sidebar();?>
        </div>
	</div>

<?php get_footer();?>