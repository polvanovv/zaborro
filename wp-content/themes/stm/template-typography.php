<?php
/*
	Template name: Typography

*/
?>

<?php get_header();?>

<div class="bg1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php if(have_posts()): ?>
                    <ul class="list-unstyled">
                        <?php while(have_posts()): ?>
                            <?php the_post() ?>
                            <li>
                                <?php if(has_post_thumbnail()) { ?>
                                    <a class="js-fancybox" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID ), 'full' ) ?>">
                                        <?php the_post_thumbnail('custom-thumb') ?>
                                    </a>
                                <?php }?>
                            </li>
                        <?php endwhile ?>
                    </ul>
                <?php endif  ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>