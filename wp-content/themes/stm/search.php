<?php
/*
Template Name: Search Page
*/
?>

<?php get_header();?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div>
                <?php
                global $wp_query;
                $total_results = $wp_query->found_posts;
                ?>
                <h5>Результаты поиска по запросу <span class="res_found">«<?php echo get_search_query() ?>»</span></h5>
                <div class="results-count-found">Найдено <?php echo $total_results; ?> результатов</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="search_result_list list-unstyled">
                <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post();?>
                        <li class="clearfix">
                            <div class="media">
                                <div class="pull-left search-thumb-wrap">
                                    <?php if(has_post_thumbnail()):?>
                                        <?php the_post_thumbnail('search-thumb',array('class'=>'media-object img-responsive'));?>
                                    <?php endif;?>
                                </div>
                                <div class="media-body">
                                    <div class="search_result_link">
                                    </div>
                                    <?php echo excerpt(50); ?>
                                </div>
                            </div>
                        </li>
                    <?php endwhile;
                endif;
                ?>
            </ul>
            <div class="pagination_wrap">
                <?php get_template_part( 'partials/paginate' );?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>

