<!DOCTYPE html>


<!--[if lt IE 9]>


<html class="old-ie" <?php language_attributes(); ?>>


<![endif]-->


<!--[if gte IE 9]><!-->


<html <?php language_attributes(); ?>>


<!--<![endif]-->


<head>


    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="format-detection" content="telephone=no"/>


    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/fav.ico" type="image/x-icon">


    <link rel="icon" href="<?php echo get_stylesheet_directory_uri() ?>/fav.ico" type="image/x-icon">


    <title><?php wp_title('', true, 'right'); ?></title>


    <?php if (is_singular()) {


        wp_enqueue_script("comment-reply");


    } ?>


    <!--[if lt IE 9]>


    <script src="<?php echo get_stylesheet_directory_uri()?>/js/html5.js"></script>


    <script src="<?php echo get_stylesheet_directory_uri()?>/js/respond.min.js"></script>


    <![endif]-->


    <?php wp_head(); ?>


    <link rel="stylesheet" href="http://zaborro.ru/wp-content/themes/stm/assets/css/app.css">


</head>


<body <?php body_class() ?>>


<!-- Modal -->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


    <div class="modal-dialog modal-dialog-md">


        <div class="modal-content">


            <div class="modal-header">


                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>


                <!--<h1 class="modal-title">Заказ забора</h1>-->


            </div>


            <div class="modal-body">


                <?php echo do_shortcode('[contact-form-7 id="154" title="Popup"]'); ?>


            </div>


        </div>


    </div>


</div>


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


    <div class="modal-dialog modal-dialog-md">


        <div class="modal-content">


            <div class="modal-header">


                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>


                <div class="modal-title">Ваш заказ отправлен</div>


            </div>


            <div class="modal-body">


            </div>


        </div>


    </div>


</div>


<div>


</div>


<header id="header" class="stm-header">

    <div class="header-mobile">
        <div class="header-m">
            <div class="header-item">
                <a href="javascript:void(0);" class="burger">
                <span class="burger-box">
                    <span class="burger__line burger__line-1"></span>
                    <span class="burger__line burger__line-2"></span>
                    <span class="burger__line burger__line-3"></span>
                </span>
                    <span class="burger__text">Меню</span>
                </a>
            </div>
            <div class="header-item">
                <a href="/kontakty/" class="header__link header__tel"></a>
                <a href="/calc/" class="header__link header__calc"></a>
            </div>
        </div>
        <div class="logo-m">
            <a href="/">
                <img src="<?php echo get_theme_mod('logo') ?>" alt="">
            </a>
        </div>
        <div class="header-m-contacts">
            <a href="tel: + 7 (495) 740 95 13"><i class="spec-icon spec-icon-tel"></i>+ 7 (495) 740 95 13</a>
            <a href="mailto: zakaz@zaborro.ru"><i class="spec-icon spec-icon-mail"></i>zakaz@zaborro.ru</a>
        </div>
        <div class="location">
            <a href="/kontakty/" class="location__link">Схема проезда</a>
            <a href="/kontakty/" class="location__link">Режим работы</a>
        </div>
        <div class="location-line"></div>

    </div>
    <div class="menu-m">

        <?php


        wp_nav_menu(array(


                'menu' => 'primary',


                'theme_location' => 'primary',


                'depth' => 0,


                'container' => false,


                'menu_class' => 'top-nav list-unstyled pull-right'


            )


        );


        ?>

    </div>
    <div class="container">


        <div class="header-desktop">
            <div class="row">


                <div class="col-xs-3">


                    <a class="st-logo" href="/"><img class="img-responsive" src="<?php echo get_theme_mod('logo') ?> " alt=""/>
                    </a>


                </div>


                <div class="col-xs-9">


                    <div class="header-row row text-right">


                        <?php


                        $headerCustomizer = get_option('contacts');


                        ?>


                        <div class="col-xs-4">


                            <div class="header-block-title">Режим работы</div>


                            <?php if (!empty($headerCustomizer['work-days'])): ?>


                                <div class="header-workdays row_height">


                                    <?php echo $headerCustomizer['work-days']; ?>


                                </div>


                            <?php endif; ?>



                            <?php if (!empty($headerCustomizer['work-days2'])): ?>


                                <div class="ffm">


                                    <?php echo $headerCustomizer['work-days2']; ?>


                                </div>


                            <?php endif; ?>


                        </div>


                        <div class="col-xs-4">


                            <div class="header-block-title">Наш адрес</div>


                            <?php if (!empty($headerCustomizer['address'])): ?>


                                <div class="header-adress row_height">


                                    <?php echo $headerCustomizer['address']; ?>


                                </div>


                            <?php endif; ?>


                            <a class="header-contacts" href="<?php echo get_permalink(get_page_by_path('kontakty')


                            ); ?>">Схема проезда</a>


                        </div>


                        <div class="col-xs-4">


                            <div class="header-block-title">Наши контакты</div>


                            <?php if (!empty($headerCustomizer['phone'])): ?>


                                <div class="header-phone row_height">


                                    <?php echo $headerCustomizer['phone']; ?>


                                </div>


                            <?php endif; ?>



                            <?php if (!empty($headerCustomizer['email'])): ?>


                                <a class="header-email" href="mailto:<?php echo $headerCustomizer['email'];


                                ?>"><?php echo


                                    $headerCustomizer['email']; ?></a>


                            <?php endif; ?>


                        </div>


                    </div>


                </div>


            </div>


            <div class="row">


                <div class="col-xs-3">


                    <?php if (!is_home()) { ?>


                        <a href="/calc/" class="btn btn-lg header-calcbtn"><span class="calc-ico"></span>Калькулятор заборов</a>


                    <?php }; ?>


                </div>


                <div class="col-xs-9">


                    <nav>


                        <?php


                        wp_nav_menu(array(


                                'menu' => 'primary',


                                'theme_location' => 'primary',


                                'depth' => 0,


                                'container' => false,


                                'menu_class' => 'top-nav list-unstyled pull-right'


                            )


                        );


                        ?>


                    </nav>


                </div>


            </div>
        </div>

        <?php if (is_home()) { ?>


            <div class="clearfix">


                <div class="pull-left banner-img">


                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cat.png" alt=""/>


                </div>


                <div class="pull-right banner-content">


                    <div class="h1">


                        <h1>Строим и устанавливаем заборы под ключ высокого качества</h1>


                    </div>


                    <p class="banner-content-text">


                        Основная деятельность нашей компании это - заборы для дачи под ключ. Доверяя нам строительство забора, вы получаете полный спектр работ по изготовлению, доставке и монтажу забора. Купить забор в нашей компании, значит довериться настоящим профессионалам.


                    </p>


                    <div>


                        <a class="btn btn-lg" href="/calc/"><span class="calc-ico"></span>Калькулятор<span class="ar-ico"></span></a>


                    </div>


                </div>


            </div>


        <?php }; ?>


    </div>


</header>


<!--#header-->


<div id="wrapper" class="stm-wrapper">


    <?php // if(is_home()) putRevSlider("sliderpreview");  ?>



		



