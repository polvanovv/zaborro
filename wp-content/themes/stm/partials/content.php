<div class="section_title">
	<div class="big">prevalence blog</div>
	<div class="line"></div>
</div>
<div class="main left">
	<article class="article">

		<div class="blog_post">
			<div class="post_author">
				By <?php the_author();?>
			</div>
			<div class="post_title">
				<h1><?php the_title()?></h1>
			</div>
			<article class="article">
				<?php if(has_post_thumbnail()):?>
				<p><?php the_post_thumbnail();?></p>
				<?php endif;?>

				<?php the_content()?>
			</article>

			<?php get_template_part('partials/share');?>

			<div class="post_summary">
				<ul>
					<li class="post_date highlight">
						<i class="fa fa-calendar"></i>
						<a href="#"><?php the_date();?></a>
					</li>

					<li class="post_tags highlight">
						<?php the_tags('<i class="fa fa-tags"></i> ');?>
					</li>
				</ul>
			</div><!--post_summary-->

			<div class="about_author">
				<div class="author_avatar">
					<?php echo get_avatar( get_the_author_meta('email'), 120); ?>
				</div>
				<div class="author_info">
					<div class="author_name"><?php the_author();?></div>
					<div class="author_content"><?php echo get_the_author_meta('description')?></div>
				</div>
			</div>

		</div><!--blog_post-->


		<div class="widget widget_related_posts">

			<?php get_template_part('partials/related', 'posts');?>

		</div><!--widget-->

		<?php
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		?>

	</article>
</div>
