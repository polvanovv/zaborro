<?php

	$tags = wp_get_post_tags(get_the_ID(), array('fields'=>'ids'));

	if(!empty($tags)):

		$args = array(
			'tag__in' => $tags,
			'post__not_in' => array( get_the_ID() ),
			'posts_per_page'=>4, // Number of related posts to display.
		);

		$relatedPosts = new WP_Query( $args );
		if($relatedPosts->have_posts()):

	?>
			<div class="widget_title">
				<i class="highlight fa fa-paperclip fa-lg fa-fw"></i><?php _e('Related posts', STM_DOMAIN);?>
			</div>
			<div class="related_posts">
				<ul>
					<?php while($relatedPosts->have_posts()): $relatedPosts->the_post();?>
					<li>
						<?php if(has_post_thumbnail()):?>
						<div class="post_image">
							<a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a>
						</div>
						<?php endif;?>
						<div class="post_title">
							<a href="<?php the_permalink();?>"><?php the_title();?></a>
						</div>
						<div class="post_excerpt">
							<?php the_excerpt();?>
						</div>
						<div class="post_date">
							<?php echo get_the_date()?>
						</div>
					</li>
					<?php endwhile;?>

				</ul>
			</div>
		<?php endif;?>
		<?php wp_reset_query();?>
	<?php endif;?>
