<div class="blog_post" >
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if(has_post_thumbnail()):?>
		<div class="post_preview">
			<a href="<?php the_permalink();?>"><?php the_post_thumbnail('large');?></a>
		</div>
		<?php endif; ?>
		<div class="post_title">
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
				<span class="post_comments"><a href="<?php the_permalink();?>#comments"><i class="highlight fa fa-comments"></i> <?php echo get_comments_number()?></a></span>
			<?php endif;?>

		</div>
		<?php the_excerpt(); ?>
		<div class="post_summary">
			<ul>
				<li class="post_date highlight">
					<i class="fa fa-calendar"></i>
					<a href="#"><?php echo  get_the_date();?></a>
				</li>
				<li class="post_tags highlight">
					<?php the_tags('<i class="fa fa-tags"></i> ');?>
				</li>
			</ul>
		</div>
	</div>

</div><!--blog_post-->