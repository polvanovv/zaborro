	<?php
		/* grab gallery_code*/
		global $post;

		if(preg_match('~\[gallery ([^\]]+)\]~', $post->post_content, $matches)){
			$post->post_content = str_replace($matches[0], '', $post->post_content);
		}
	?>


	<div class="project_gallery">
	<?php if( !empty( $matches[0])) echo do_shortcode($matches[0]); ?>
	</div><!--project_gallery-->
	<div class="project_info">
		<div class="project_category highlight"><?php  the_category()?></div>
		<div class="project_title"><?php the_title()?></div>
		<div class="project_text">
			<?php echo apply_filters('the_content', $post->post_content);?>
		</div>
	</div>
