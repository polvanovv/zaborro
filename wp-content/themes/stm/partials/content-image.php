
<div class="project_gallery">
	<?php the_post_thumbnail('full');?>
</div><!--project_gallery-->
<div class="project_info">
	<div class="project_category highlight"><?php  the_category()?></div>
	<div class="project_title"><?php the_title()?></div>
	<div class="project_text">
		<?php echo apply_filters('the_content', $post->post_content);?>
	</div>
</div>
