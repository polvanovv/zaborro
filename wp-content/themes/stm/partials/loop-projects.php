
<?php
	global $post_elements;
	$filterClass  = '';
	$categories  = wp_get_post_terms(get_the_ID(), 'dslc_projects_cats', array('fields'=>'slugs'));
	if(!empty($categories)) $filterClass = implode(' ', $categories);

?>
<li class="<?php echo $filterClass;?>">
	<div class="portfolio_item">
		<a href="#!portfolio-item/<?php echo get_relative_permalink(get_the_permalink());?>" data-url="" class="portfolio_image">
			<?php the_post_thumbnail(array(290, 194));?>
			<span class="portfolio_overlay">
				<span class="thumb_info">
					<?php if(!empty($post_elements['icon'])):?>
					<span class="project_icon"><i class="fa <?php echo stm_get_format_icon(get_the_ID())?>"></i></span>
					<?php endif;?>

					<?php if(!empty($post_elements['title'])):?>
					<span class="project_title highlight"><?php the_title();?></span>
					<?php endif;?>

					<?php if(!empty($post_elements['excerpt'])):?>
					<span class="project_text"><?php the_excerpt();?></span>
					<?php endif;?>
				</span>
			</span>
		</a>
	</div>
</li>