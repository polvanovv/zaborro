<div class="section_title">
	<div class="big"><?php the_title(  );?></div>
	<div class="line"></div>
</div>


<div class="main">
	<article class="article">
		<?php the_post_thumbnail('large');?>
		<?php the_content();?>
		<div class="clear"></div>
		<?php wp_link_pages(); ?>
	</article>
</div>
