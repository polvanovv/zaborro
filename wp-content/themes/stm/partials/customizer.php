<?php
	
	$prevalence_generals = get_option('prevalence_generals');
	$prevalence_generals['color_scheme'] = empty($prevalence_generals['color_scheme']) ? 'blue' : $prevalence_generals['color_scheme'];
	$prevalence_generals['pattern'] = empty($prevalence_generals['pattern']) ? 20 : $prevalence_generals['pattern'];
	$prevalence_generals['layout'] = empty($prevalence_generals['layout']) ? 'fullwidth' : $prevalence_generals['layout'];
?>
<div id="style_picker" style="left: 0px;">
	<div class="handler"><i class="fa fa-cogs fa-2x"></i></div>
	<div class="title">
		Theme options
	</div>

	<div class="options_title">
		<span>Layout Style</span>
	</div>
	<div class="options_content">
		<div id="layout_style">
			<ul>
				<li>
					<label><input type="radio" value="layout_wide" name="layout_style" id="layout_wide" <?php if($prevalence_generals['layout'] == 'fullwidth'):?> checked="checked"<?php endif;?>> Wide</label>
				</li>
				<li>
					<label><input type="radio" value="boxed" name="layout_style" id="layout_boxed" <?php if($prevalence_generals['layout'] == 'boxed'):?> checked="checked"<?php endif;?>> Boxed</label>
				</li>
			</ul>
		</div>
	</div>

	<div class="options_title">
		<span>Colors</span>
	</div>
	<div class="options_content">
		<div id="theme_scheme">
			<ul>
				<?php
					$colors =array(
						'yellow'	=> 'Yellow',
						'orange'	=> 'Orange',
						'green'		=> 'Green',
						'blue'		=> 'Blue',
						'magenta'	=> 'Magenta',
						'violet'	=> 'Violet',
						'crimson'	=> 'Crimson',
					);
					foreach($colors as $colorCode=>$colorLabel) :?>
				<li <?php if($colorCode == $prevalence_generals['color_scheme']):?> class="active"<?php endif;?>>
					<a class="scheme_<?php echo $colorCode?>" href="#" title="<?php echo $colorLabel?>"></a>
				</li>
				<?php endforeach;?>

			</ul>
		</div><!--theme_scheme-->
	</div>

	<div class="options_title">
		<span>Patterns</span>
	</div>
	<div class="options_content">
		<div id="theme_patterns">
			<ul>
				<?php
					$patterns = array(
						'01', '02', '03', '04', '05',
						'06', '07', '08', '09', '10',
						'11', '12', '13', '14', '15',
						'16', '17', '18', '19', '20',
					);
					foreach($patterns as $patternCode) :?>
				<li <?php if($prevalence_generals['pattern'] == $patternCode):?> class="active" <?php endif;?>>
					<a class="pattern_<?php echo $patternCode?> " href="#"></a>
				</li>
				<?php endforeach;?>

			</ul>
		</div><!--theme_scheme-->
	</div>
</div>