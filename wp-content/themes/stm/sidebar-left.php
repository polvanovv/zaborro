<?php $args = stm_query_fence(array(
	'orderby'=>'date',
	'order'   => 'ASC'
)); ?>
<div class="sidebar-wrap">
    <div class="btn-close-sidebar">
        <span class="closer closer-1"></span>
        <span class="closer closer-2"></span>
    </div>
<?php if ( $args->have_posts() ) : ?>
	<div class="sidebar-wrapper">
	<div class="sidebar-type1">
	<div class="h3 sidebar-title"><a href="<?php echo get_post_type_archive_link(ST_fence::POST_TYPE); ?>">Заборы</a></div>
	<ul class="list-unstyled sidebar-list">
		<?php wp_reset_query(); ?>
		<?php while ( $args->have_posts() ) : $args->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</li>
		<?php endwhile; ?>
	</ul>
<?php endif; ?>
	</div>
</div>
	<div class="sidebar-calc-wrap">
		<a data-toggle="modal" data-target="#myModal" class="btn btn-lg btn-block" href="#"><span class="fence-ico"></span>Заказ забора<span class="ar-ico"></span></a>
	</div>
<div class="h3 add-title"><a href="<?php echo get_post_type_archive_link(ST_additional::POST_TYPE); ?>">Дополнительно</a></div>
<?php wp_reset_query(); ?>
<?php $args = stm_query_additional(); ?>
<?php if ( $args->have_posts() ) : ?>
	<ul class="list-unstyled ad-list">
		<?php while ( $args->have_posts() ) : $args->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</li>
		<?php endwhile; ?>
	</ul>
<?php endif; ?>
<?php wp_reset_query(); ?>
<div class="meteo-block">
<!-- Gismeteo informer START -->
<!--<link rel="stylesheet" type="text/css" href="http://www.gismeteo.kz/static/css/informer2/gs_informerClient.min.css">-->
<!--<div id="gsInformerID-h2FRw6eCdETyk1" class="gsInformer" style="width:270px;height:178px">-->
<!--  <div class="gsIContent">-->
<!--    <div id="cityLink">-->
<!--      <a rel="nofollow" href="http://www.gismeteo.ru/city/daily/4368/" target="_blank">Погода в Москве</a>-->
<!--    </div>-->
<!--    <div class="gsLinks">-->
<!--      <table>-->
<!--        <tr>-->
<!--          <td>-->
<!--            <div class="leftCol">-->
<!--              <a rel="nofollow" href="http://www.gismeteo.ru" target="_blank">-->
<!--                <img alt="Gismeteo" title="Gismeteo" src="http://www.gismeteo.kz/static/images/informer2/logo-mini2.png" align="absmiddle" border="0" />-->
<!--                <span>Gismeteo</span>-->
<!--              </a>-->
<!--            </div>-->
<!--            <div class="rightCol">-->
<!--              <a rel="nofollow" href="http://www.gismeteo.ru/city/weekly/4368/" target="_blank">Прогноз на 2 недели</a>-->
<!--            </div>-->
<!--            </td>-->
<!--        </tr>-->
<!--      </table>-->
<!--    </div>-->
<!--  </div>-->
<!--</div>-->
<!--<script src="http://www.gismeteo.kz/ajax/getInformer/?hash=h2FRw6eCdETyk1" type="text/javascript"></script>-->
<!-- Gismeteo informer END -->
</div>
</div>
<?php wp_reset_query(); ?>