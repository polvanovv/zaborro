<?php get_header() ?>
    <div class="container">
    <div class="row"><a href="javascript:void(0);" class="sidebar-show btn">Каталог продукции</a>
        <div class="col-xs-3">            <?php get_sidebar('left'); ?>        </div>
        <div class="col-md-9 col-xs-12 col-sm-12">
            <ul class="breadcrumbs list-unstyled clearfix">                <?php if (function_exists('bcn_display_list')) {
                    bcn_display_list();
                } ?>            </ul>
            <h1>Наши работы </h1> <?php if (have_posts()) : ?>
                <ul class="list-unstyled works-list row">                    <?php while (have_posts()) : the_post(); ?>
                        <li class="col-xs-4">
                            <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_post_thumbnail('medium'); ?></a>                            </a>
                            <div class="media-heading">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                        </li>                    <?php endwhile; ?>
                </ul>            <?php endif; ?>
            <div class="pagination_wrap">                <?php get_template_part('partials/paginate'); ?>            </div>
        </div>
    </div>    <?php get_sidebar('articles'); ?></div><?php get_footer() ?>