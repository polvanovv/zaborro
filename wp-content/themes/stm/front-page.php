<?php get_header() ?>
	<div class="container">
		<?php $args = stm_query_fence(array(
			'orderby'=>'date',
			'order'   => 'ASC'
		)); ?>
		<div class="st-block-header">
			<span class="h1 st-block-header-title">Виды заборов</span>
			<a class="link-to-archive"
			   href="<?php echo get_post_type_archive_link( ST_fence::POST_TYPE ); ?>">Все
				виды
				заборов</a>
		</div>
		<?php if ($args->have_posts()) : ?>
		<div class="carousel-relative-wrapper">
			<a class="fred-prev fred-prv2" href="#"></a>
			<a class="fred-next fred-nxt2" href="#"></a>
			<ul class="list-unstyled front-fence-list js-fence-list ">
				<?php while ( $args->have_posts() ) : $args->the_post(); ?>
					<li>
						<a class="fence-slider-img"
						   href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail(); ?>
						</a>
						<a class="fence-slider-title"
						   href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
		</div>
	</div>
	<div class="bg-type2 bg-block1">
		<div class="container">
			<div class="h1">Дополнительно делаем</div>
			<?php wp_reset_query(); ?>
			<?php $args = stm_query_additional(); ?>
			<div class="carousel-relative-wrapper">
				<a class="fred-prev fred-prv3" href="#"></a>
				<a class="fred-next fred-nxt3" href="#"></a>
				<?php if ( $args->have_posts() ) : ?>
					<ul class="list-unstyled js-additional carousel-additional-list clearfix">
						<?php while ( $args->have_posts() ) : $args->the_post(); ?>
							<li>
								<a class="carousel-additional-list-img"
								   href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail(); ?>
								</a>
									<div>
									<a class="carousel-additional-list-title"
									   href="<?php the_permalink(); ?>">
										<?php the_title(); ?>
									</a>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="text-center">
				<a data-toggle="modal" data-target="#myModal" href="#" class="btn btn-lg"><span class="fence-ico"></span>Заказ забора<span class="ar-ico"></span></a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="h1 h1marg"><?php $count_posts = wp_count_posts( ST_reasons::POST_TYPE ); ?>
			<?php echo $published_posts = $count_posts->publish;; ?> причин заказать забор в нашей компании
		</div>
		<?php wp_reset_query(); ?>
		<?php $args = stm_query_reasons(); ?>
		<?php if ( $args->have_posts() ) : ?>
			<ul class="list-unstyled reasons-list clearfix">
				<?php while ( $args->have_posts() ) : $args->the_post(); ?>
					<li class="col-xs-6">
						<div class="media">
							<?php if ( has_post_thumbnail() ) { ?>
								<div class="media-left">
									<div
										class="rounded-img">
										<?php the_post_thumbnail( '' ) ?>
									</div>
								</div>
							<?php } ?>
							<div class="media-body">
								<div
									class="media-heading"><?php the_title(); ?></div>
							</div>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		<div class="zigzag-line"></div>
	</div>
	<div class="container page-part">
		<div class="row">
			<?php
			$page = get_posts(
				array(
					'name'      => 'zabory-dlya-dachi',
					'post_type' => 'page'
				)
			);
			?>
			<div class="col-xs-12">
				<div class="h1"><?php echo $page[0]->post_title; ?></div>
			</div>
			<?php echo $page[0]->post_content; ?>
		</div>
	</div>
	<div class="bg-type3 bg-block2">
		<div class="container">
			<div class="st-block-header">
				<span class="h1 st-block-header-title">Примеры наших работ</span>
				<a class="link-to-archive" href="<?php echo get_post_type_archive_link( ST_works::POST_TYPE ); ?>">Все работы</a>
			</div>
			<?php wp_reset_query(); ?>
			<?php $args = stm_query_works(); ?>
			<div class="carousel-relative-wrapper">
				<a class="fred-prev fred-prv5" href="#"></a>
				<a class="fred-next fred-nxt5" href="#"></a>
				<?php if ( $args->have_posts() ) : ?>
					<ul class="list-unstyled js-work carousel-work-list clearfix">
						<?php while ( $args->have_posts() ) : $args->the_post(); ?>
							<li>
								<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="gallery">
									<?php the_post_thumbnail( 'custom-additional' ) ?>
									<span class="pic-overlay"></span>
								</a>
<div class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>

<script>
(function(){
	var elements = document.querySelectorAll('.carousel-relative-wrapper');

	for (var i = 0; i < elements.length; i++) {
		elements[i].style.visibility = 'hidden';
	}
})();
</script>

	<div class="container front-testimonials">
		<div class="st-block-header">
			<span class="h1 st-block-header-title">Отзывы о нас</span>
			<a class="link-to-archive" href="<?php echo get_post_type_archive_link( ST_testimonials::POST_TYPE ); ?>">Все отзывы</a>
		</div>
		<?php wp_reset_query(); ?>
		<?php $args = stm_query_testimonials(); ?>
		<?php if ($args->have_posts()) : ?>
		<div class="carousel-relative-wrapper">
			<a class="fred-prev fred-prv4" href="#"></a>
			<a class="fred-next fred-nxt4" href="#"></a>
			<ul class="list-unstyled js-testimonials testimonials-list carousel-testimonials-list clearfix">
				<?php while ( $args->have_posts() ) : $args->the_post(); ?>
					<li>
						<div class="media">
							<div class="media-left">
								<div class="testimonial-thumb">
									<?php the_post_thumbnail() ?>
								</div>
							</div>
							<div class="media-body">
								<div class="media-heading h4"><?php the_title(); ?></div>
								<div class="media-content">
									<?php echo excerpt( 45 ); ?>
								</div>
								<div class="text-right">
									<?php echo get_post_meta( $post->ID, 'tname', true ); ?>, <span class="tcity"><?php echo get_post_meta( $post->ID, 'tcity', true ); ?></span>
								</div>
							</div>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
		


		<div class="zigzag-line line-marg"></div>
	</div>
	<div class="container">
		<div class="h1">Как заказать забор?</div>
		<?php wp_reset_query(); ?>
		<?php $args = stm_query_howto(); ?>
		<?php if ($args->have_posts() ) : ?>
			<ul class="list-unstyled list-unstyled howto-list clearfix">
				<?php while ( $args->have_posts() ) : $args->the_post(); ?>
					<li>
						<div class="howto-img">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="howto-title">
							<?php the_title(); ?>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
</div>
		<div class="text-center">
			<a data-toggle="modal" data-target="#myModal" class="btn btn-lg" href="#"><span class="fence-ico"></span>Заказ забора<span class="ar-ico"></span></a>
		</div>
		<div class="zigzag-line line-marg"></div>
	</div>
	<div class="container">
		<?php get_sidebar( 'articles' ); ?>
	</div>
<?php get_footer() ?>