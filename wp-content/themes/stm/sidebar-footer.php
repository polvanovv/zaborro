<?php if ( is_active_sidebar( 'footer' ) ) : ?>
	
	<div id="footer-widgets" class="footer-widget widget-area row">
		<?php dynamic_sidebar( 'footer' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; ?>

