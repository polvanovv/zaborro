<?php
	
	$inc_path = get_template_directory() . '/inc';
	define('STM_DOMAIN', 'stm_domain');
	
	/* Initials*/
	require_once( $inc_path . '/post_types.class.php' );
	
	/* Admin only */
	require_once( $inc_path . '/sforms.class.php' );

	if( is_admin() ) {
		require_once( $inc_path . '/tgm.php' );
		require_once( $inc_path . '/customizer.php' );

		add_action( 'init', 'my_theme_add_editor_styles' );
	}

	/* Custom post types*/
	require_once( $inc_path . '/posts/articles.php' );
	require_once( $inc_path . '/posts/works.php' );
	require_once( $inc_path . '/posts/testimonials.php' );
	require_once( $inc_path . '/posts/fence.php' );
	require_once( $inc_path . '/posts/additional.php' );
	require_once( $inc_path . '/posts/reasons.php' );
	require_once( $inc_path . '/posts/howto.php' );

	/* Customize theme */
	require_once( $inc_path . '/setup.php' );
	require_once( $inc_path . '/shortcodes.php' );
	require_once( $inc_path . '/scripts_styles.php' );


	/* Functions && template tags */
	require_once( $inc_path . '/pagination.php' );
	require_once( $inc_path . '/feedback.php' );
	require_once( $inc_path . '/navigation.php' );

	
	/* Custom functions */
	require_once( $inc_path . '/custom.php' );
	require_once( $inc_path . '/gallery.php' );
	require_once( $inc_path . '/wp_bootstrap_navwalker.php' );
