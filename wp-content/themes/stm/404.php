<?php get_header('er');?>

	<div class="container">
		<div class="err-title text-center">
			Страница не найдена
		</div>
		<div class="row">
			<div class="col-xs-3 text-center">
				<img class="err-img" src="<?php echo get_template_directory_uri(); ?>/assets/img/fen.png" alt=""/>
			</div>
			<div class="col-xs-9">

				<div class="h4 err-t2">Ошибка 404. К сожалению, запрашиваемая вами страница не найдена.</div>

				<div class="h4 err-t3">Возможные причины</div>
				<ul class="list-unstyled list-custom-styled">
					<li>Вы набрали неправильный адрес</li>
					<li>Страница была удалена с сайта или никогда не существовала</li>
				</ul>
				<div class="h4 err-t3">Что делать</div>
				<ul class="list-unstyled list-custom-styled">
					<li>Убедитесь, что набираемый адрес правильный</li>
					<li>Начните пользоваться сайтом с <a href="/">главной страницы</a></li>
				</ul>
			</div>
		</div>
	</div>

<?php get_footer('er');?>