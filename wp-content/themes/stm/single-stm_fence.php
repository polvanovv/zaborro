<?php get_header() ?>
    <div class="container">
        <div class="row">
            <a href="javascript:void(0);" class="sidebar-show btn">Каталог продукции</a>
            <div class="col-xs-3">                <?php get_sidebar('left'); ?>            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <ul class="breadcrumbs list-unstyled clearfix">                    <?php if (function_exists('bcn_display_list')) {
                        bcn_display_list();
                    } ?>                </ul> <?php if (have_posts()) : ?><?php while (have_posts()) : the_post(); ?>
                    <h1>                            <?php the_title(); ?>                        </h1>
                    <div>
                        <div class="pull-left single-thumb">                                <?php the_post_thumbnail(); ?>                            </div> <?php the_content() ?>
                    </div>                    <?php endwhile; ?><?php endif; ?>            </div>
        </div>
        <div class="zigzag-line line-marg"></div>
    </div>
    <div class="container">
        <div class="h1">Как заказать забор?</div> <?php wp_reset_query(); ?>        <?php $args = stm_query_howto(); ?>        <?php if ($args->have_posts()) : ?>
            <ul class="list-unstyled list-unstyled howto-list clearfix">                <?php while ($args->have_posts()) : $args->the_post(); ?>
                    <li>
                        <div class="howto-img">                            <?php the_post_thumbnail(); ?>                        </div>
                        <div class="howto-title">                            <?php the_title(); ?>                        </div>
                    </li>                <?php endwhile; ?>            </ul>        <?php endif; ?>
    </div>
    <div class="text-center">
        <a data-toggle="modal" data-target="#myModal" class="btn btn-lg" href="#"><span class="fence-ico"></span>Заказ забора<span class="ar-ico"></span></a>
    </div>        </div><?php get_footer() ?>