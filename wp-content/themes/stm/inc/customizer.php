<?php

	
	function theme_register_customizer_options( $wp_customize ){
	
		require_once('customizer/TextArea.class.php');

		/* Logo */	
		$wp_customize->add_section( 'logo', array(
		    'title'          => __( 'Logo', STM_DOMAIN ),
		    'priority'       => 35,
		) );
		$wp_customize->add_setting( 'logo', array(
		    'default'        => '',
		    'capability'     => 'edit_theme_options',
		) );
		$wp_customize->add_control(
	       new WP_Customize_Image_Control(
	           $wp_customize,
	           'logo',
	           array(
	               'label'      => __( 'Upload a logo', 'theme_name' ),
	               'section'    => 'logo',
	               'settings'   => 'logo',
	               'context'    => '' 
	           )
	       )
	   );



		$wp_customize->add_section( 'social_buttons', array(
		    'title'          => __( 'Social buttons', STM_DOMAIN ),
		    'priority'       => 35,
		) );
		
		$socialLinks = array(
			'rss'		=> __('Rss', STM_DOMAIN),
			'linkedin'	=> __('Linkedin', STM_DOMAIN),
			'twitter'	=> __('Twitter', STM_DOMAIN),
			'youtube'	=> __('Youtube', STM_DOMAIN),
			'facebook'	=> __('Facebook', STM_DOMAIN),
			'pinterest'	=> __('Pinterest', STM_DOMAIN),
			'vk'		=> __('vKontakte', STM_DOMAIN),
			'ok'		=> __('Odnoklassniki', STM_DOMAIN),
			'instagram'	=> __('Instagram', STM_DOMAIN),
		);
		
		foreach($socialLinks as $setting=>$label){
			$wp_customize->add_setting( 'socials['.$setting.']', array(
			    'default'        => '',
			    'type'           => 'option',
			    'capability'     => 'edit_theme_options',
			) );
			$wp_customize->add_control( 'socials['.$setting.']', array(
			    'label'   => $label,
			    'section' => 'social_buttons',
			    'type'    => 'text',
			) );
		}
		
		
		
		/* Contacts Phone, address, email */
		
		$wp_customize->add_section( 'contacts', array(
		    'title'          => __( 'Contacts', STM_DOMAIN ),
		    'priority'       => 36,
		) );
		
		$wp_customize->add_setting( 'contacts[phone]', array(
		    'default'        => '',
		    'capability'     => 'edit_theme_options',
		    'type'           => 'option',
		) );
		$wp_customize->add_setting( 'contacts[phone2]', array(
			'default'        => '',
			'capability'     => 'edit_theme_options',
			'type'           => 'option',
		) );
		$wp_customize->add_control(
			'contacts_phone', 
			array(
				'label'    => __( 'Phone', STM_DOMAIN ),
				'section'  => 'contacts',
				'settings' => 'contacts[phone]',
				'type'     => 'text',
			)
		);
		$wp_customize->add_control(
			'contacts_phone2',
			array(
				'label'    => __( 'Phone2', STM_DOMAIN ),
				'section'  => 'contacts',
				'settings' => 'contacts[phone2]',
				'type'     => 'text',
			)
		);

		
		$wp_customize->add_setting( 'contacts[skype]', array(
		    'default'        => '',
		    'capability'     => 'edit_theme_options',
		    'type'           => 'option',
		) );
		$wp_customize->add_control(
			'contacts_skype', 
			array(
				'label'    => __( 'Skype', STM_DOMAIN ),
				'section'  => 'contacts',
				'settings' => 'contacts[skype]',
				'type'     => 'text',
			)
		);

			

		$wp_customize->add_setting( 'contacts[email]', array(
		    'default'        => '',
		    'capability'     => 'edit_theme_options',
		    'type'           => 'option',
		) );
		$wp_customize->add_control(
			'contacts_email', 
			array(
				'label'    => __( 'Email', STM_DOMAIN ),
				'section'  => 'contacts',
				'settings' => 'contacts[email]',
				'type'     => 'text',
			)
		);

		$wp_customize->add_setting( 'contacts[work-days]', array(
			'default'        => '',
			'capability'     => 'edit_theme_options',
			'type'           => 'option',
		) );
		$wp_customize->add_control(
			'contacts_workdays',
			array(
				'label'    => __( 'Время работы будни', STM_DOMAIN ),
				'section'  => 'contacts',
				'settings' => 'contacts[work-days]',
				'type'     => 'text',
			)

		);

		$wp_customize->add_setting( 'contacts[work-days2]', array(
			'default'        => '',
			'capability'     => 'edit_theme_options',
			'type'           => 'option',
		) );
		$wp_customize->add_control(
			'contacts_workdays2',
			array(
				'label'    => __( 'Время работы выходные', STM_DOMAIN ),
				'section'  => 'contacts',
				'settings' => 'contacts[work-days2]',
				'type'     => 'text',
			)

		);

		$wp_customize->add_setting( 'contacts[address]', array(
		    'default'        => '',
		    'capability'     => 'edit_theme_options',
		    'type'           => 'option',
		) );
		$wp_customize->add_control(new Stm_Customize_Textarea_Control($wp_customize, 'contacts[address]', array(
			'label' => __('Address', STM_DOMAIN),
			'section' => 'contacts',
			
		)));
		
		
		
		/* Footer area*/
		$wp_customize->add_section( 'footer', array(
		    'title'          => __( 'Footer', STM_DOMAIN ),
		    'priority'       => 36,
		) );
		
		$wp_customize->add_setting( 'footer[copyright]', array(
		    'default'        => '',
		    'type'           => 'option',
		    'capability'     => 'edit_theme_options',
		) );
		$wp_customize->add_control(new Stm_Customize_Textarea_Control($wp_customize, 'footer[copyright]', array(
			'label' => __('Copyright', STM_DOMAIN),
			'section' => 'footer',
			
		)));
		
		
		
				
	}
	add_action( 'customize_register', 'theme_register_customizer_options' );
	
	