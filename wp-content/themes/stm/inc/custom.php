<?php

	/*
		Define feedback form settings 
	*/
	if(function_exists('feedbackFormSetup')) {

		$allowedFields = array(
			'name'		=> array('required'=>true, 'label'=>__('Name', STM_DOMAIN)),
			'phone'		=> array('required'=>false, 'label'=>__('Phone', STM_DOMAIN)),
			'email'		=> array('required'=>true, 'label'=>__('Email', STM_DOMAIN), 'validate_rule'=>'email',),
			'message'	=> array('required'=>true, 'label'=>__('Message', STM_DOMAIN)),
		);

		
		feedbackFormSetup( 'feedback',  array(
			'allowedFields'		=> $allowedFields,
			'mailTitle'			=> __('Feedback', STM_DOMAIN),
			'recipients'		=> get_bloginfo('admin_email'),
		));
	}
	

	/*
		Set theme layout
	*/
	function prevalence_body_class($classes) 
	{
		/* Add body class here*/

		return $classes;
		
	}
	add_filter('body_class', 'prevalence_body_class');


	function stm_comment($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		extract($args, EXTR_SKIP);
		$add_below = 'div-comment';

		?>
		<li id="li-comment-<?php echo $comment->comment_ID; ?>" class="comment">
			<div id="comment-<?php echo $comment->comment_ID; ?>">
				<div class="comment-author">
					<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>

				</div><!-- .comment-author .vcard -->
				<div class="comment-meta">
					<?php printf( __( '<cite class="fn highlight">%s</cite>' ), get_comment_author_link() ); ?>
					<?php printf( __('%1$s at %2$s', STM_DOMAIN), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', STM_DOMAIN ), '  ', '' ); ?>
				</div><!-- .comment-meta .commentmetadata -->
				<div class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div><!-- .reply -->
				<div class="comment-body">
					<p><?php comment_text(); ?></p>
				</div>
			</div><!-- #comment-##  -->
		</li>


	<?php
	}

	function get_relative_permalink( $url ) {
		$url = get_permalink();
		return str_replace( home_url(), "", $url );
	}

	function my_theme_add_editor_styles() {

		add_editor_style( get_stylesheet_directory_uri(). '/assets/css/editor.css' );
	}

// Custom words trim function
function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/[.+]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function new_excerpt_more( $more ) {
    return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');


add_image_size( 'custom-additional', 235, 180, true );
add_image_size( 'custom-work', 270, 180, true );

function testimonials_function(){

	global $current_user;
	if(is_user_logged_in()) {
		get_currentuserinfo();
	}

	if(!empty($_POST['testimonials_action'])){
		//$_POST['testimonial-text'] = addslashes(trim($_POST['testimonial-text']));
		//$_POST['testimonial-name'] = addslashes(trim($_POST['testimonial-name']));
		//$_POST['testimonial-city'] = addslashes(trim($_POST['testimonial-city']));
		$json = array();

		if(isset($_POST['testimonial-text']) && $_POST['testimonial-text'] == ""){
			$json['error']['testimonial-text'] = "Пожалуйста, заполните поле";
		}
		if(isset($_POST['testimonial-name']) && $_POST['testimonial-name'] == ""){
			$json['error']['testimonial-name'] = "Пожалуйста, заполните поле";
		}
		if(isset($_POST['testimonial-city']) && $_POST['testimonial-city'] == ""){
			$json['error']['testimonial-city'] = "Пожалуйста, заполните поле";
		}
		if(isset($_POST['testimonial-email']) && $_POST['testimonial-email'] == ""){
			$json['error']['testimonial-email'] = "Пожалуйста, заполните поле";
		}
		if(isset($_POST['testimonial-project']) && $_POST['testimonial-project'] == ""){
			$json['error']['testimonial-project'] = "Пожалуйста, заполните поле";
		}

		if(!$json){
			$testimonial = array(
				'post_title' => $_POST['testimonial-project'],
				'post_content' => $_POST['testimonial-text'],
				'post_type' => 'stm_testimonials',
				'post_status' => 'pending'
			);

			$post_id = wp_insert_post( $testimonial );
			update_post_meta( $post_id, 'tcity', $_POST['testimonial-city'] );
			update_post_meta( $post_id, 'tname', $_POST['testimonial-name'] );
			update_post_meta( $post_id, 'tmail', $_POST['testimonial-email'] );

			if(is_file($_FILES['file']['tmp_name'])) {

				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );

				$attachment_id = media_handle_upload('file', $post_id );
				set_post_thumbnail( $post_id, $attachment_id );

				//update_post_meta($post_id,'_thumbnail_id',$attachment_id);
			}

			$json['success'] = '<div>Ваш отзыв добавлен.</div>';
		}
		echo json_encode($json);
		exit;
	}
}

add_action( 'init', 'testimonials_function');

function theme_name_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}

	global $page, $paged;

	// Add the blog name
	$title .= get_bloginfo( 'name', 'display' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
 }

	return $title;
}
add_filter( 'wp_title', 'theme_name_wp_title', 10, 2 );


?>