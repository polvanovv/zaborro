
(function() {
    tinymce.create('tinymce.plugins.Shortcodes', {
        init : function(ed, url) {

            ed.addButton('shortcodes', {
                text: 'Shortcodes',
                title : 'Short codes',
                type: 'menubutton',
                menu:[
                    {
                        text:'Custom list',
                        onclick: function(){
                            ed.execCommand('InsertUnorderedList', 0);
                            ed.dom.addClass(ed.dom.getParent(ed.selection.getNode(), 'ul'), 'custom_list');
                        }
                    },
                    {
                        text:'Custom quote',
                        onclick: function(){
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<blockquote class="quote">' + selected_text + '</blockquote>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        }
                    },
                    {
                        text:'Note',
                        onclick: function(){
                            var selected_text = ed.selection.getContent();
                            if(selected_text == '') selected_text = 'Note content';
                                var return_text = '';
                            return_text = '<div class="shortcode_note highlight">' + selected_text + '</div>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        }
                    },
                    {
                        text:'Messages',
                        menu:[
                            {text:'Notice', onclick:function(){ ed.execCommand('putMessage', 'notice'); }},
                            {text:'Warning', onclick:function(){ ed.execCommand('putMessage', 'warning'); }},
                            {text:'Success', onclick:function(){ ed.execCommand('putMessage', 'success'); }},
                            {text:'Error', onclick:function(){ ed.execCommand('putMessage', 'error'); }},
                            {text:'Info', onclick:function(){ ed.execCommand('putMessage', 'info'); }},
                            {text:'Callout', onclick:function(){
                                var return_text = '<div class="callout"><div class="callout_title">Callout title goes here </div><div class="callout_content">Callout content goes here </div></div>';
                                ed.execCommand('mceInsertContent', 0, return_text);
                            }},
                        ]
                    },
                    {
                        text:'Progress',
                        menu:[
                            {text:'Type1', onclick:function(){
                                var return_text = '[progress embedded=true]<br />' +
                                    '[bar title="Progress title" percentage="75"]<br />' +
                                    '[bar title="Progress title" percentage="55"]<br />' +
                                    '[bar title="Progress title" percentage="35"]<br />' +
                                    '[/progress]<br />';
                                ed.execCommand('mceInsertContent', 0, return_text);
                            }},
                            {text:'Type2', onclick:function(){
                                var return_text = '[progress]<br />' +
                                    '[bar title="Progress title" percentage="75"]<br />' +
                                    '[bar title="Progress title" percentage="55"]<br />' +
                                    '[bar title="Progress title" percentage="35"]<br />' +
                                    '[/progress]<br />';
                                ed.execCommand('mceInsertContent', 0, return_text);
                            }},

                        ]
                    },
                    {
                        text:'Others',
                        menu:[
                            {text:'Accordions', onclick:function(){

                                var return_text = '[accordion]<br />' +
                                    '[toggle title="Section One" open="yes"]Accordion content goes here[/toggle]<br />' +

                                    '[toggle title="Section Two" open="no"]Accordion content goes here[/toggle]<br />' +

                                    '[toggle title="Section Three" open="no"]Accordion content goes here[/toggle]<br />' +

                                    '[/accordion]<br />';
                                    ed.execCommand('mceInsertContent', 0, return_text);

                            }},
                            {text:'Toggles', onclick:function(){
                                var return_text = '[toggles]<br />' +
                                    '[toggle title="Section One" open="yes"]Accordion content goes here[/toggle]<br />' +

                                    '[toggle title="Section Two" open="no"]Accordion content goes here[/toggle]<br />' +

                                    '[toggle title="Section Three" open="no"]Accordion content goes here[/toggle]<br />' +

                                    '[/toggles]<br />';
                                ed.execCommand('mceInsertContent', 0, return_text);

                            }},
                            {text:'Tabs', onclick:function(){
                                var return_text = '[tabs]<br />' +
                                    '[tab title="Tab One" open="yes"]Tab content goes here[/tab]<br />' +

                                    '[tab title="Tab Two" open="no"]Tab content goes here[/tab]<br />' +

                                    '[tab title="Tab Three" open="no"]Tab content goes here[/tab]<br />' +

                                    '[/tabs]<br />';
                                ed.execCommand('mceInsertContent', 0, return_text);


                            }},
                            {text:'Tabs with icon', onclick:function(){
                                var return_text = '[tabs]<br />' +
                                    '[tab title="Tab One" icon="print" open="yes"]Tab content goes here[/tab]<br />' +

                                    '[tab title="Tab Two" icon="download" open="no"]Tab content goes here[/tab]<br />' +

                                    '[tab title="Tab Three" icon="users" open="no"]Tab content goes here[/tab]<br />' +

                                    '[/tabs]<br />';
                                ed.execCommand('mceInsertContent', 0, return_text);


                            }},
                        ]
                    },
                    {
                        text:'Columns',
                        menu:[
                            {text:'1/2', onclick:function(){ ed.execCommand('putColumns', 'one_half'); }},
                            {text:'1/3', onclick:function(){ ed.execCommand('putColumns', 'one_third'); }},
                            {text:'1/6', onclick:function(){ ed.execCommand('putColumns', 'one_six'); }},
                            {text:'2/3', onclick:function(){ ed.execCommand('putColumns', 'two_third'); }},
                        ]
                    }






                ]
            });



           

            ed.addCommand('putMessage', function(type){

                var selected_text = ed.selection.getContent();
                var return_text = '';
                return_text = '<div class="alert_message '+ type +'">' + selected_text + '</div>';
                ed.execCommand('mceInsertContent', 0, return_text);

            });


            ed.addCommand('putColumns', function(type){

                var return_text = '';
                switch (type){
                    case 'one_half':
                        return_text = '<div class="one_half">...</div><div class="one_half last">...</div><div class="clear"></div>';
                        break;
                    case 'one_third':
                        return_text = '<div class="one_third">...</div><div class="one_third">...</div><div class="one_third  last">...</div><div class="clear"></div>';
                        break;
                    case 'one_six':
                        return_text = '<div class="one_six">...</div><div class="one_six">...</div><div class="one_six">...</div><div class="one_six">...</div><div class="one_six">...</div><div class="one_six  last">...</div><div class="clear"></div>';
                        break;
                    case 'two_third':
                        return_text = '<div class="two_third">...</div><div class="one_third  last">...</div><div class="clear"></div>';
                        break;


                }

                ed.execCommand('mceInsertContent', 0, return_text);

            });

            ed.addCommand('dropcap', function(){


                ed.execCommand('InsertUnorderedList', 0);
                ed.dom.addClass(ed.dom.getParent(ed.selection.getNode(), 'ul'), 'custom_list');


            });


        },
        createControl : function(n, cm) {

            if(n=='revslider'){
                var mlb = cm.createListBox('revslider', {
                    title : 'revslider',
                    onselect : function(v) {
                        alert('asd');
                    }
                });


                return mlb;
            }
            return null;
        }



    });
    // Register plugin
    tinymce.PluginManager.add( 'stm', tinymce.plugins.Shortcodes );
})();