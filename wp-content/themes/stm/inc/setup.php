<?php

	if(!session_id()) session_start();
	if ( ! isset( $content_width ) ) {
		$content_width = 950;
	}
	
	
	add_action( 'after_setup_theme', 'local_theme_setup' );
	function local_theme_setup(){
		
		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'primary'   => __( 'Top primary menu', STM_DOMAIN ),
			'secondary' => __( 'Secondary menu in the footer', STM_DOMAIN ),
		) );
		
		add_theme_support( 'post-formats', array('video','gallery','image') );
		add_theme_support( 'custom-background');
		add_theme_support( 'automatic-feed-links' );

		register_sidebar( array(
			'name'          => __( 'Primary Sidebar', STM_DOMAIN ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Main sidebar that appears on the right.', STM_DOMAIN ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<div class="widget_title">',
			'after_title'   => '</div>',
		) );
		
		register_sidebar( array(
			'name'          => __( 'Footer area', STM_DOMAIN ),
			'id'            => 'footer',
			'description'   => __( 'Footer widget area that appears at the bottom.', STM_DOMAIN ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<div class="widget_title">',
			'after_title'   => '</div>',
		) );
		
	}
