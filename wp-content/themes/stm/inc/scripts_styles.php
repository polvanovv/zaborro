<?php
	
	/*
		Scripts and Styles (SS)
	*/

	if( ! is_admin() )
		add_action( 'wp_enqueue_scripts', 'stm_load_theme_ss' );
	
	function stm_load_theme_ss(){
		
		/* Styles */
//		wp_enqueue_style( 'theme_style', get_stylesheet_directory_uri() . '/assets/style.css', NULL, NULL, 'all' ); 
/* 		wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/assets/css/font-awesome.css', NULL, NULL, 'all' );  */
		
		
		wp_enqueue_style( 'boostrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css', NULL, NULL, 'all' ); 
		wp_enqueue_style( 'fonts', get_stylesheet_directory_uri() . '/assets/fonts/fonts.css', NULL, NULL, 'all' );
        wp_enqueue_style( 'fancyboxcss', get_stylesheet_directory_uri() . '/assets/css/jquery.fancybox.css', NULL, NULL, 'all' );
		wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/assets/css/style.css', NULL, NULL, 'all' );
		wp_enqueue_style( 'select2css', get_stylesheet_directory_uri() . '/assets/css/select2.css', NULL, NULL, 'all' );
		wp_enqueue_style( 'scss', get_stylesheet_directory_uri() . '/assets/css/mystyle.css', NULL, NULL, 'all' );
		wp_enqueue_style( 'theme-style-responsive', get_stylesheet_directory_uri() . '/assets/css/responsive.css', NULL, NULL, 'all' );

		/* Register own jquery */		
		wp_deregister_script('jquery');
		
		/* Scripts */ 
		wp_enqueue_script( 'jquery',   get_template_directory_uri() . '/assets/js/jquery-1.11.1.min.js', '', NULL, false );
		wp_enqueue_script( 'jquerymigrate',   get_template_directory_uri() . '/assets/js/jquery-migrate-1.2.1.min.js',     'jquery', NULL, TRUE );
        wp_enqueue_script( 'bootstrap',   get_template_directory_uri() . '/assets/js/bootstrap.min.js', 'jquery', NULL, TRUE );
        wp_enqueue_script( 'fancyboxjs',   get_template_directory_uri() . '/assets/js/jquery.fancybox.js', 'jquery', NULL, TRUE );
        wp_enqueue_script( 'fredseljs',   get_template_directory_uri() . '/assets/js/jquery.carouFredSel-6.2.1.js', 'jquery', NULL, TRUE );
        wp_enqueue_script( 'select2',   get_template_directory_uri() . '/assets/js/select2.min.js', 'jquery', NULL, TRUE );
        wp_enqueue_script( 'custom',   get_template_directory_uri() . '/assets/js/custom.js', 'jquery', NULL, TRUE );
	}
	
	
	
	
	