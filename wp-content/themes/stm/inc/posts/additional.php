<?php
/**
 * Additional custom post types
 */

ST_Additional::init();

class ST_additional extends ST_PostType {

	const POST_TYPE = 'stm_additional';
	const CATEGORY_TAXONOMY = 'Category';
	const CATEGORY_TAXONOMY_SLUG = 'additional_category';


	public static function init() {

		/* Register type */

		self::registerPostType( self::POST_TYPE, __( 'Additional', STM_DOMAIN ), array(
			'pluralTitle'       => __( 'Дополнительно делаем' ),
			'public'            => true,
			'rewrite'           => array( 'slug' => 'additional' ),
			'supports'          => array( 'title', 'thumbnail', 'editor' ),
			'show_in_nav_menus' => true
		) );

		/* Register taxonomy */
		self::addTaxonomy( self::CATEGORY_TAXONOMY, self::CATEGORY_TAXONOMY_SLUG, self::POST_TYPE );

		/* Register custom fields */
		/*
		self::addMetaBox('params','Params', self::POST_TYPE, '', '', '',array(
			'fields'=>array(
				'price'				=> array('label'=>__('Price (Eur.)'), 	'required'=>false),
				'priceWithTax'		=> array('label'=>__('Price with tax (Eur.)'), 	'required'=>false),
				'paymentType'		=> array('label'=>__('Payment type'), 	'type'=>'selectbox', 'options'=>servicePaymentTypes()),
				'duration'			=> array('label'=>__('Duration (Min.)'), 	'required'=>false),
				'order'				=> array('label'=>__('Sort'), 	'required'=>false),
				'timeStep'			=> array('label'=>__('Time step (Min.)'), 	'required'=>false),
				'canOrder'			=> array('label'=>__('Can order'), 	'required'=>false, 'type'=>'selectbox', 'options'=>array('yes'=>_('Yes'), ''=>_('No'))),
			)
		) );
		*/


	}
}


function stm_query_additional( $args = '' ) {

	$defaults = array(
		'post_type'      => ST_Additional::POST_TYPE,
		'posts_per_page' => - 1,
	);
	$args     = wp_parse_args( $args, $defaults );

	return new WP_Query( $args );
}

