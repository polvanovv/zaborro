var $ = jQuery;

$(window).load(function() {
  $('.js-additional').carouFredSel({
    scroll: {
      items: 1,
    },
    auto: {
      play: false,
    },
    items: 4,
    prev: '.fred-prv3',
    next: '.fred-nxt3',
  });

  var additionalBlock = $('.carousel-relative-wrapper');
  additionalBlock.hide();
  additionalBlock.css({visibility: 'visible'});
  additionalBlock.fadeIn(200);
});

function footerToBottom() {

  $('body').css('margin-bottom', $('.stm-footer').outerHeight() + 100);

}

$(document).ready(footerToBottom);

$(window).on('resize', footerToBottom);

$(document).ready(function() {

  //FredSel Carousel

  $('.js-fence-list').carouFredSel({

    height: 'auto',

    scroll: {

      items: 1,

    },

    auto: {

      play: false,

    },

    items: 5,

    prev: '.fred-prv2',

    next: '.fred-nxt2',

  });

  $('.js-testimonials').carouFredSel({

    scroll: {

      items: 1,

    },

    auto: {

      play: false,

    },

    items: 1,

    prev: '.fred-prv4',

    next: '.fred-nxt4',

  });

  $('.js-work').carouFredSel({

    scroll: {

      items: 1,

    },

    auto: {

      play: false,

    },

    items: 4,

    prev: '.fred-prv5',

    next: '.fred-nxt5',

  });

  $('.scrollToTop').click(function(event) {

    event.preventDefault();

    $('html, body').animate({scrollTop: 0}, 800);

    return false;

  });

  $('.js-trigger').click(function() {

    $('.st-btn-submit').click();

  });

  /*$('select').not('#clcFence select').select2({

      width:'100%',

      minimumResultsForSearch: -1

  });*/

  // custom input type file

  $('.file-btn').click(function(e) {

    e.preventDefault();

    $('.type-file').trigger('click');

  });

  $('.type-file').change(function() {

    var uploadVal = $('.type-file').val();

    $('.file-value').text(uploadVal);

  });

//FancyBox

  $('.fancybox').fancybox();

  $('.gal').fancybox({

    type: 'ajax',

    padding: 50,

    autoResize: false,

    autoHeight: false,

    fitToView: false,

  });

  $('#testimonial_form').submit(function(e) {

    e.preventDefault();

    submitModalForm('testimonial_form');

    console.log('123');

    return false;

    var m_method = $(this).attr('method');

    var m_action = $(this).attr('action');

    var m_action = $(this).attr('action');

    var m_data = $(this).serialize();

    $.ajax({

      type: m_method,

      url: m_action,

      data: m_data,

      dataType: 'json',

      context: $(this),

      success: function(json) {

        if (json['error']) {

          for (key in json['error']) {

            $(this).
                find('textarea[name=\'' + key + '\']').
                addClass('has-error').
                after('<span class="wpcf7-not-valid-tip">' +
                    json['error']['testimonial-text'] + '</span>');

            $(this).
                find('input[name=\'' + key + '\']').
                addClass('has-error').
                after('<span class="wpcf7-not-valid-tip">' +
                    json['error']['testimonial-text'] + '</span>');

          }

        }

        if (json['success']) {

          $('.testimonials-form-wrap input').removeClass('has-error');

          $('.testimonials-form-wrap textarea').removeClass('has-error');

          $('.testimonials-form-wrap .wpcf7-not-valid-tip').remove();

          $('.testimonials-form-wrap').
              after(
                  '<div class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" style="display: block;" role="alert">Спасибо за отзыв, ваше сообщение появится после проверки модератора.</div>');

        }

      },

    });

  });

  jQuery('input[type="file"]').on('change', function(e) {

    Files.push(e.target.files[0]);

  });

});

var Files = [];

function submitModalForm(formId) {

  var data = $('#' + formId).serializeArray();

  var formUrl = $('#' + formId).attr('action');

  /* Clear previous log*/

  $('#' + formId + ' .has-error').removeClass('has-error');

  var formData = new FormData();

  for (file in Files) {

    formData.append('file', Files[file]);

  }

  for (i in data) {

    formData.append(data[i]['name'], data[i]['value']);

  }

  var xhr = new XMLHttpRequest();

  xhr.open('POST', formUrl);

  xhr.send(formData);

  xhr.onreadystatechange = function() {

    if (xhr.readyState == 4) {

      var json = jQuery.parseJSON(xhr.responseText);

      console.log('result', json);

      if (json['error']) {

        for (key in json['error']) {

          $('#' + formId).
              find('textarea[name=\'' + key + '\']').
              addClass('has-error').
              after('<span class="wpcf7-not-valid-tip">' +
                  json['error']['testimonial-text'] + '</span>');

          $('#' + formId).
              find('input[name=\'' + key + '\']').
              addClass('has-error').
              after('<span class="wpcf7-not-valid-tip">' +
                  json['error']['testimonial-text'] + '</span>');

        }

      }

      if (json['success']) {

        $('.testimonials-form-wrap input').removeClass('has-error');

        $('.testimonials-form-wrap textarea').removeClass('has-error');

        $('.testimonials-form-wrap .wpcf7-not-valid-tip').remove();

        $('.testimonials-form-wrap').
            after(
                '<div class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" style="display: block;" role="alert">Спасибо за отзыв, ваше сообщение появится после проверки модератора.</div>');

      }

    }

  };

}

$('.sidebar-show').click(function() {
  $('.sidebar-wrap').addClass('active');
  $('.sidebar-wrap').closest('.col-md-3').removeClass('hidden-sm hidden-xs');
  $('body').addClass('body-hidden');
});
$('body').click(function(e) {
  if ($(e.target).closest('.sidebar-show').length === 0) {
    $('.sidebar-wrap').removeClass('active');
    $('.sidebar-wrap').closest('.col-md-3').addClass('hidden-sm hidden-xs');
    $('body').removeClass('body-hidden');
  }
});
$('.burger').click(function() {
  $(this).toggleClass('active');
  $('.menu-m').toggleClass('active');
});
$('.btn-close-sidebar').click(function() {
  $('.sidebar-wrap').removeClass('active');
  $('.sidebar-wrap').closest('.col-md-3').addClass('hidden-sm hidden-xs');
  $('body').removeClass('body-hidden');
});
// $('.page-template-template-prices-php table').closest('div').addClass('scroll-container');