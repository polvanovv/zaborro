<div class="st-block-header">
	<span class="h1 st-block-header-title">Другие наши работы</span>
	<a class="link-to-archive" href="<?php echo get_post_type_archive_link(ST_works::POST_TYPE); ?>">Все работы</a>
</div>
<?php $args = stm_query_works(array(
	'posts_per_page' => 4
)) ; ?>
<?php if ( $args->have_posts() ) : ?>
	<ul class="list-unstyled carousel-article-list clearfix">
		<?php while ( $args->have_posts() ) : $args->the_post(); ?>
			<li>
				<a class="carousel-article-list-img" href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(array(270,180)); ?>
				</a>
				<div class="carousel-article-list-title">
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</div>
				<div class="carousel-article-list-content">
					<?php echo excerpt(20); ?>
				</div>
			</li>
		<?php endwhile; ?>
	</ul>
<?php endif; ?>